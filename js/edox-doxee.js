(function ($) {
    "use strict"; // Start of use strict


    if ($.fn.style) {
        return;
    }

    // The style function
    $.fn.style = function(styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };

    ExtractUserData();
    var userData = GetUserData();

    // Store some information in the global object
    window.edoxData = {};
    window.edoxData.userName = userData.firstName;
})(jQuery); // End of use strict


/**
 * Retrieve User data from the JSON data and save it to a global variable
 * @returns {*}
 */
function ExtractUserData() {
    var jsonData = GetJSonData();

    if (jsonData.length <= 0)
        return false;

    try {
        if (!("eDox" in window))
            window.eDox = {};
        window.eDox.userData = jsonData.FILE.DOCUMENT[0].TEMPLATE[0].DATA[0];
        return true;
    }
    catch (err) {
        return false;
    }
}


/**
 * Retrieve JSON data from the JSON script
 */
function GetJSonData() {
    var $jsonData = $('#doc-data');
    return JSON.parse($jsonData.text());
}


/**
 * Retrieve user data from the global eDox variable
 *
 * @returns {*}
 */
function GetUserData() {
    if (!'eDox' in window)
        return null;

    if (!'userData' in window.eDox)
        return null;

    return window.eDox.userData;
}


function UpdateUserInformation() {
    var $name = $('#name');
    var $birthDate = $('#date-of-birth');
    var $address1 = $('#address-1');
    var $email = $('#email');
    var $startDate = $('#start-date');
    var $peopleCount = $('#people');
    var $occupant = $('#occupant');
    var $business = $('#business');
    var $firstNameAlt = $('#firstnamealt');

    var userData = GetUserData();

    $name.text(userData.firstName + ' ' + userData.lastName);
    $address1.text(userData.street + ', ' + userData.city + ' - ' + userData.country);
    $email.text(userData.eMail);
    $firstNameAlt.text(userData.firstName);
}

/**
 * "Enrol" form handler
 */
function HandleEnrolForm() {
    var $progressBarContainer = $('#enrol-progress-container').find('#counter-line');
    var $progressBar = $progressBarContainer.find('.rect-fg');
    var $sparksAnimation = $('#sparks-animation');
    var $currentScore = $('#enrol-progress-container').find('#point');


    var $form = $('#enrol-form');
    var $inputs = $form.find('input[type=text]');
    var userData = GetUserData();
    var totalPoints = 0;
    var currentScore = 0;

    console.log('HandleEnrolForm()');

    // Hide sparks animation by default
    $sparksAnimation.hide();

    // Init input scores
    $inputs.each(function () {
        var id = $(this).attr('id');

        // Id not found in userData, that's not right
        if (!id in userData)
            return true;

        var score = parseInt(userData[id]);
        var inputName = $(this).attr('name');
        var $p = $(this).siblings('p');
        var $tickImg = $(this).siblings('img');

        // Score is not a number, that's not right
        if (isNaN(score))
            return true;

        // Init "score" attribute
        $(this).attr('score', score);
        if (inputName in userData) {
            $(this).val(userData[inputName]);
            $(this).attr('value', userData[inputName]);
            $p.removeClass('text-secondary');
            $p.addClass('text-white');
            $tickImg.removeClass('hidden-xs-up');
        }
        else {
            $p.removeClass('text-white');
            $p.addClass('text-secondary');
            $tickImg.addClass('hidden-xs-up');
        }

        // Init score text
        $(this).siblings('p').text(score + 'pt');

        totalPoints += score;
    });

    $currentScore.text('0pt');
    $progressBar.attr('width', '0%');


    $inputs.change(function () {
        // Show sparks for n-seconds
        $sparksAnimation.show();
        setTimeout(function () {
            submit
            $sparksAnimation.hide();
        }, 5000);

        var $p = $(this).siblings('p');
        var $tickImg = $(this).siblings('img');

        var inputVal = $(this).val();
        if (inputVal.length <= 0) {
            $p.removeClass('text-white');
            $p.addClass('text-secondary');
            $tickImg.addClass('hidden-xs-up');
        }
        else {
            $p.removeClass('text-secondary');
            $p.addClass('text-white');
            $tickImg.removeClass('hidden-xs-up');
        }

        UpdateScore();
    });

    UpdateScore();


    function UpdateScore() {
        currentScore = 0;
        $inputs.each(function () {
            var inputScore = parseInt($(this).attr('score'));

            // That's not right
            if (isNaN(inputScore))
                return true;

            var inputVal = $(this).val();
            if (inputVal.length > 0)
                currentScore += inputScore;
        });

        $currentScore.text(currentScore);

        var barPercentage = (currentScore / totalPoints) * 100;
        $progressBar.attr('width', barPercentage + '%');
    }
}


/**
 * "Monthly fee" form handler
 */
function HandleMonthlyFeeForm() {
    var $monthlyFeeSection = $('#monthly-fee');
    var $monthlyFee = $('#monthly-fee-price');
    var $currency = $('#monthly-fee-currency');
    var $inputs = $monthlyFeeSection.find('input[type=checkbox]');

    var userData = GetUserData();

    var currentFee = userData.initialMonthlyFee;

    console.log('HandleMonthlyFeeForm()');
    console.log($inputs);

    // Init inputs fees
    $inputs.each(function () {
        var id = $(this).attr('id');

        // Id not found in userData, that's not right
        if (!id in userData)
            return true;

        var fee = parseFloat(userData[id]);

        // Score is not a number, that's not right
        if (isNaN(fee))
            return true;

        // Init "fee" attribute
        $(this).attr('fee', fee);
        $(this).prop('checked', false);
    });

    $inputs.change(function () {
        console.log('checkbox changed:');
        console.log($(this));

        UpdateMonthlyFee();
    });


    $monthlyFee.text(parseFloat(currentFee).toFixed(2));
    $currency.text(userData.currency);


    function UpdateMonthlyFee() {
        var newFee = userData.initialMonthlyFee;
        $inputs.each(function () {
            var inputFee = parseFloat($(this).attr('fee'));

            // That's not right
            if (isNaN(inputFee))
                return true;

            if ($(this).prop('checked'))
                newFee += inputFee;
        });

        console.log('UPDATE MONTHLY FEE');
        console.log(newFee);

        // $monthlyFee.text(feeTotal);
        animateCount(currentFee, newFee, 1.73);

        currentFee = newFee;
    }


    function animateCount(start, stop, step) {
        console.log('CountUP():');
        console.log('Start: ' + start);
        console.log('Stop: ' + stop);
        console.log('Step: ' + step);

        var current = parseFloat(start);
        $monthlyFee.text(current.toFixed(2));

        if (start < stop)
            _countUp();
        else if (start > stop)
            _countDown();


        function _countUp() {
            current = parseFloat(current + step);

            if (current > stop) {
                $monthlyFee.text(parseFloat(stop).toFixed(2));
            }
            else {
                $monthlyFee.text(current.toFixed(2));
                setTimeout(_countUp, 10);
            }
        }

        function _countDown() {
            current = parseFloat(current - step);

            if (current < stop) {
                $monthlyFee.text(parseFloat(stop).toFixed(2));
            }
            else {
                $monthlyFee.text(current.toFixed(2));
                setTimeout(_countDown, 10);
            }
        }
    }
}


/**
 * Handle eDox data submission
 */
function InitEDoxDataSubmission() {
    $('#submit-edox-data').click(function () {
        var url = 'https://eu2.cloud.doxee.com/ondemand-ws/test/Doxee/MIPU/Vita01/envVita01';

        // Bom XML
        var bomXml = '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<bom xmlns="http://www.doxee.com/ps/bom"\n' +
            '    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n' +
            '    xsi:schemaLocation="http://www.doxee.com/ps/bom http://www.doxee.com/ps/bom/bom.xsd"\n' +
            '    creationDate="20170918" label="IFF sample job" name="IFF_00001" inceptionId="000" requestId="000">\n' +
            '  <material>\n' +
            '    <workPackage>\n' +
            '    <workUnit creationDate="20170918" size="443" name="input_data_file2.xml" />\n' +
            '    </workPackage>\n' +
            '  </material>\n' +
            '</bom>\n';

        var userData = GetUserData();
        var eDoxData = {};


        eDoxData['firstName'] = $('input[name=firstName]').val();
        eDoxData['lastName'] = $('input[name=lastName]').val();
        eDoxData['eMail'] = $('input[name=eMail]').val();
        eDoxData['street'] = userData.street;
        eDoxData['city'] = userData.city;
        eDoxData['country'] = userData.country;
        eDoxData['p1'] = $('#option1Fee').is(':checked') ? '1' : '0';
        eDoxData['p2'] = $('#option2Fee').is(':checked') ? '1' : '0';
        eDoxData['p3'] = $('#option3Fee').is(':checked') ? '1' : '0';
        eDoxData['p4'] = $('#option4Fee').is(':checked') ? '1' : '0';
        eDoxData['p5'] = userData.p5;
        eDoxData['initialMonthlyFee'] = $('#monthly-fee-price').text();
        eDoxData['option1Fee'] = userData.option1Fee;
        eDoxData['option2Fee'] = userData.option2Fee;
        eDoxData['option3Fee'] = userData.option3Fee;
        eDoxData['option4Fee'] = userData.option4Fee;
        eDoxData['currency'] = userData.currency;
        eDoxData['webLink'] = userData.webLink;


        var inputDataXml = sprintf('<?xml version="1.0" encoding="utf-8"?>\n' +
            '<FILE>\n' +
            '  <DOCUMENT>\n' +
            '    <TEMPLATE>\n' +
            '        <DATA IDTEMPLATE="IC" CodCli="3020595" firstName="%(firstName)s" lastName="%(lastName)s" eMail="%(eMail)s" street="%(street)s" city="%(city)s" country="%(country)s" p1="%(p1)s" p2="%(p2)s" p3="%(p3)s" p4="%(p4)s" p5="%(p5)s" initialMonthlyFee="%(initialMonthlyFee)s" option1Fee="%(option1Fee)s" option2Fee="%(option2Fee)s" option3Fee="%(option3Fee)s" option4Fee="%(option4Fee)s" currency="%(currency)s" webLink="%(webLink)s">\n' +
            '\t\t</DATA>\n' +
            '    </TEMPLATE>\n' +
            '  </DOCUMENT>\n' +
            ' </FILE>', eDoxData);


        console.log('eDox Data');
        console.log(eDoxData);
        console.log(inputDataXml);


        // Data to send
        var params = {
            'bomXml': btoa(bomXml),
            'dataXml': btoa(inputDataXml)
        };

        
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            dataType: "json",
            success: function (data, textStatus, jqXHR ) {
                console.log('Success: ' + textStatus);
                console.log(data);
            },

            complete: function (jqXHR, textStatus ) {
                console.log('Complete: ' + textStatus);
            },

            error: function (jqXHR, textStatus, errorThrown ) {
                console.log('Error: ' + textStatus);
                console.log(errorThrown);
            }
        });

        return false;
    });
}


$(document).ready(function () {

    $('.menu-button').on('click', function () {
        $('#main-menu').toggleClass('menu-hide');
    });

    // Smooth scrolling using jQuery easing
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 48)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Activate scrollspy to add active class to navbar items on scroll


    // Closes responsive menu when a link is clicked
    $('.navbar-collapse>ul>li>a').click(function () {
        $('.navbar-collapse').collapse('hide');
    });


    InitIFrameAnimationHandler(jQuery);
    InitTextAnimationHandler(jQuery);


    UpdateUserInformation();

    // Init form handlers
    HandleEnrolForm();
    HandleMonthlyFeeForm();

    // Init eDox data submission
    InitEDoxDataSubmission();


    $('.btn').click(function() {
        var $that = $(this);

        setTimeout(
            function () {
                $that.val('DONE');
                $that.style('background-color', '#64bf99', 'important');
            }, 500
        );
    });
});
