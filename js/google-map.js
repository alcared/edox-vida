function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: -34.397, lng: 150.644},
        mapTypeId: 'roadmap',
        scrollwheel: false,
        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": "-20"
                    },
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "50"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-20"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "saturation": "15"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "10"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-40"
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-20"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-25"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-35"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-35"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-35"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a28bb5"
                    },
                    {
                        "lightness": "-10"
                    },
                    {
                        "saturation": "20"
                    }
                ]
            }
        ]
    });
    var geocoder = new google.maps.Geocoder();


    // document.getElementById('submit').addEventListener('click', function() {
    //     geocodeAddress(geocoder, map);
    // });


    function SetMapLocation(address) {
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }


    function WaitForUserData() {
        // If GetUserData is not a function it means user data has not been extracted yet
        if (typeof GetUserData !== 'function') {
            setTimeout(WaitForUserData, 5);
        }
        else {
            var userData = GetUserData();
            var address = userData.street + ', ' + userData.city + ' - ' + userData.country;

            SetMapLocation(address);
        }
    }

    // Wait for user data to be extracted
    // then initialize map location
    WaitForUserData();
}

//
// function geocodeAddress(geocoder, resultsMap) {
//     var address = document.getElementById('address').value;
//     geocoder.geocode({'address': address}, function(results, status) {
//         if (status === 'OK') {
//             resultsMap.setCenter(results[0].geometry.location);
//             var marker = new google.maps.Marker({
//                 map: resultsMap,
//                 position: results[0].geometry.location
//             });
//         } else {
//             alert('Geocode was not successful for the following reason: ' + status);
//         }
//     });
// }
//
//
//
//
