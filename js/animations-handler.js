/*******************************************
 * Handle IFrame animations
 *******************************************/
function InitIFrameAnimationHandler($) {
    var $animationContainers = $('.animation-container');

    $animationContainers.each(function() {
        var $this = $(this);

        var animationOffset = "60%";    // default animation offset

        // data-animation-offset option
        var optAnimationOffset = $this.attr('data-animation-offset');
        if (typeof optAnimationOffset !== 'undefined')
            animationOffset = optAnimationOffset;

        var waypoint = new Waypoint({
            element: $this[0],
            handler: function() {
                $this.trigger('enterViewPort');
            },
            offset: animationOffset
        });

        $this.data('waypoint', waypoint);


        // Get animation url
        var animationUrl = $(this).attr('data-animation-file');

        // Check url\file
        if (typeof animationUrl === 'undefined' || animationUrl.length <= 0)
            return;

        // Create the iframe
        var $iframe = $('<iframe></iframe>');
        $iframe.attr('id', $(this).attr('id') + '-iframe');
        $iframe.addClass('animation-iframe');
        $iframe.attr('src', animationUrl);
        $iframe.attr('frameBorder', 0);
        $iframe.attr('height', '100%');
        $iframe.attr('width', '100%');
        $iframe.attr('seamless', 'seamless');

        // $iframe.hide();
        $iframe.appendTo($(this));

        $iframe.on('load', function() {
            // On load stop the animation
            CheckAndStopAnimation($iframe);
        });

    });

    function CheckAndStopAnimation($iframe) {
        if (typeof $iframe === 'undefined') {
            return;
        }

        var $animationContainer = $iframe.closest('.animation-container');
        var iframe = $iframe.get(0);
        var iframeWindow = (iframe.contentWindow || iframe.contentDocument);

        // Wait for the functions to be available
        if (typeof iframeWindow.StopAnimation !== 'undefined' &&
            typeof iframeWindow.StartAnimation !== 'undefined') {

            // iframeWindow.ResetAnimation();
            iframeWindow.StopAnimation();

            // If element is visible the animation should start immediately
            if (isElementPartiallyInViewport($iframe)) {
                StartAnimation($animationContainer);
            }
            else {
                BindAnimationStart($iframe);
            }
        }
        else {
            setTimeout(function() {
                CheckAndStopAnimation($iframe);
            }, 50);
        }
    }

    function BindAnimationStart($iframe) {
        var $animationContainer = $iframe.closest('.animation-container');

        $animationContainer.on('enterViewPort', function(){
            StartAnimation($animationContainer);
        });
    }

    function StartAnimation ($animationContainer) {
        var $iframe = $animationContainer.find('iframe');
        var animationUrl = $iframe.attr('src');

        // First check if it has been already initialized
        var animationInitialized = $animationContainer.data('animation-initialized');
        if (typeof animationInitialized !== 'undefined') {
            return;
        }

        // Mark as initialized
        $animationContainer.data('animation-initialized', true);

        // Placeholder img
        var $placeholderImg = $animationContainer.find('.img-placeholder');

        // On load stop the animation
        var iframe = $iframe.get(0);
        var iframeWindow = (iframe.contentWindow || iframe.contentDocument);

        // $iframe.show();
        // Delay the start
        setTimeout(function () {
            iframeWindow.StartAnimation();
            $placeholderImg.hide();
        }, 50);
        // $placeholderImg.remove();
    }
}
/*******************************************
 * EOF IFrame animation Handler
 *******************************************/


/*******************************************
 * Handle Text in viewport animations
 *******************************************/
function InitTextAnimationHandler($) {
    var $elementToAnimate = $('.animate-fadein');
    $elementToAnimate.acrWaypointAnimate({
        waypointOffsetPercentage: 40,
        classToAdd: 'animated fadeIn'
    });


    var $elementToAnimate_2 = $('.animate-test');
    $elementToAnimate_2.acrWaypointAnimate({
        waypointOffsetPercentage: 60,
        classToAdd: 'animated slideInUp'
    });


    var $elementToAnimate_3 = $('.selector-3');
    $elementToAnimate_3.acrWaypointAnimate({
        waypointOffsetPercentage: 10,
        classToAdd: 'classe31 classe32 classe33'
    });
}
/*******************************************
 * EOF Handle Text in viewport animations
 *******************************************/


/**
 * Check if an element is totally visible in the viewport
 * @param el
 * @returns {boolean}
 */
function isElementInViewport (el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}


/**
 * Check if an element is partially in the viewport on the Y axis.
 *
 * @param el
 * @returns {boolean}
 */
function isElementPartiallyInViewport(el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();
    var wiewportHeight = (window.innerHeight || document.documentElement.clientHeight);

    // Element above the viewport nothing is showing
    if (rect.top < 0 && rect.top < (rect.height * -1)) {
        return false;
    }

    // Element below viewport nothing is showing
    if (rect.top > 0 && rect.top > wiewportHeight) {
        return false;
    }


    return true;
}