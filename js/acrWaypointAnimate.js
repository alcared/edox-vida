/**
 * Alcared Waypoint Animations v.1.0
 *
 *
 * Requirements:
 *      - jQuery
 *      - (plugin) Waypoint
 *
 *
 * jQuery element options:
 *      - acr-waypoint-class-to-add : (string) space separated list of classes to add when the waypoint is reached
 *      - acr-waypoint-offset-percentage : (int) waypoint trigger offset
 *
 *      NOT USED:
 *      - acr-animation-duration : (int) animation duration
 *      - acr-animation-delay : (int) animation delay
 *      - acr-animation-iterations : (int) animation iterations, -1 stands for infinite
 *
 *
 */
(function ($) {
    console.log('%c Initializing acrWaypointAnimate', 'color: #0000FF');
    if (typeof Waypoint !== 'function') {
        console.log('%c Waypoint plugin needed!', 'color: #FF0000');
        return false;
    }


    $.fn.extend(
        {
            acrWaypointAnimate: function (options) {
                // Default options
                var acrDefaults  = {
                    // duration: 0,
                    // delay: 0,
                    // iterations: 1,  // Infinite
                    waypointOffsetPercentage: 0,
                    classToAdd: ""
                };




                // Var names
                var var_AcrWaypoint = 'acr-waypoint';
                var var_AcrWaypointTriggered = 'acr-waypoint-animated';

                // Attr names
                var attr_AcrClassToAdd  = 'acr-waypoint-class-to-add';    // List of classes to add when the waypoint is reached
                var attr_AcrWaypointOffsetPercentage = 'acr-waypoint-offset-percentage';


                return this.each(function () {
                    var $this = $(this);

                    var _options = GetOptions($this);

                    var waypoint = new Waypoint({
                        element: $this[0],
                        handler: function() {
                            // Animate one time only
                            if ($this.data(var_AcrWaypointTriggered) === true)
                                return;

                            // Mark as animated
                            $this.data(var_AcrWaypointTriggered, true);


                            // Get classes to add
                            var txtClassesToAdd = _options.classToAdd;
                            var aClasses = txtClassesToAdd.split(" ");
                            for(var i = 0; i < aClasses.length; i++) {
                                $this.addClass(aClasses[i]);
                            }
                        },
                        offset: _options.waypointOffsetPercentage + '%'
                    });

                    $this.data(var_AcrWaypoint, waypoint);
                });




                function GetOptions($element) {
                    // // Duration
                    // var duration = parseInt($this.attr('acr-animation-duration'));
                    // if( isNaN(duration))
                    //     duration = acrDefaults.duration;
                    //
                    // // Delay
                    // var delay = parseInt($this.attr('acr-animation-delay'));
                    // if( isNaN(delay))
                    //     delay = acrDefaults.delay;
                    //
                    // // Iterations count
                    // var iterations = parseInt($this.attr('acr-animation-iterations'));
                    // if( isNaN(iterations))
                    //     iterations = acrDefaults.iterations;
                    var _options = {};


                    // Option: waypointOffsetPercentage
                    if ('waypointOffsetPercentage' in options)
                        _options.waypointOffsetPercentage = options.waypointOffsetPercentage;
                    else {
                        _options.waypointOffsetPercentage = parseInt($element.attr(attr_AcrWaypointOffsetPercentage));
                        if( isNaN(_options.waypointOffsetPercentage))
                            _options.waypointOffsetPercentage = acrDefaults.waypointOffsetPercentage;
                    }


                    // Option: classToAdd
                    if ('classToAdd' in options)
                        _options.classToAdd = options.classToAdd;
                    else {
                        _options.classToAdd = $element.attr(attr_AcrClassToAdd);
                        if (typeof _options.classToAdd === 'undefined') {
                            _options.classToAdd = acrDefaults.classToAdd;
                        }
                    }


                    return _options;
                }
            }
        }
    );

})(jQuery);


