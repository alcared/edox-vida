(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Singolobianco = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Singolobianco, new cjs.Rectangle(0,0,26,26), null);


(lib.piurosa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape.setTransform(19.1,19.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_1.setTransform(19.2,19.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.piurosa, new cjs.Rectangle(0,0,38.3,38.3), null);


(lib.piubianco = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgYC/IAAimIimAAIAAgxICmAAIAAimIAxAAIAACmICmAAIAAAxIimAAIAACmg");
	this.shape.setTransform(19.1,19.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.piubianco, new cjs.Rectangle(0,0,38.3,38.3), null);


(lib.Coppiarosa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("AhaBcQgngmABg2QgBg1AngmQAmgmA0AAQA2AAAlAmQAnAmAAA1QAAA2gnAmQglAmg2AAQg0AAgmgmg");
	this.shape.setTransform(62.6,13);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AgnApQgRgRAAgYQAAgXARgQQAQgRAXAAQAYAAARARQAQAQAAAXQAAAYgQARQgRAQgYAAQgXAAgQgQg");
	this.shape_1.setTransform(5.7,306.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FC3065").s().p("AhkBkQgqgpABg7QgBg6AqgqQAqgqA6AAQA7AAAqAqQApAqAAA6QAAA7gpApQgqArg7gBQg6ABgqgrg");
	this.shape_2.setTransform(239.2,283.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Coppiarosa, new cjs.Rectangle(0,0,253.5,311.9), null);


// stage content:
(lib._4sparksconteggio = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Coppia rosa
	this.instance = new lib.Coppiarosa();
	this.instance.parent = this;
	this.instance.setTransform(391.9,276,0.621,0.621,75,0,0,126.8,155.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).wait(1).to({regX:126.7,regY:155.9,scaleX:0.79,scaleY:0.79,rotation:41.1,x:391.8,alpha:0.452},0).wait(1).to({scaleX:0.85,scaleY:0.85,rotation:29.7,alpha:0.603},0).wait(1).to({scaleX:0.89,scaleY:0.89,rotation:22.2,alpha:0.705},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:16.5,alpha:0.779},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:12.2,alpha:0.837},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:8.8,alpha:0.882},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:6.2,x:391.9,alpha:0.918},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:4.1,x:391.8,alpha:0.945},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:2.5,alpha:0.966},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:1.4,x:391.9,alpha:0.982},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.6,x:391.8,alpha:0.992},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.1,alpha:0.998},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:391.9,alpha:1},0).to({regX:126.8,rotation:-6.2,x:392,alpha:0},16).wait(14));

	// piu bianco copy
	this.instance_1 = new lib.piubianco();
	this.instance_1.parent = this;
	this.instance_1.setTransform(201,152,0.525,0.525,144.5,0,0,19.2,19.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).wait(1).to({regX:19.1,scaleX:0.81,scaleY:0.81,rotation:190.9,x:182,y:138.6},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:202,x:177.4,y:135.4},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:208.3,x:174.9,y:133.6},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:212.4,x:173.1,y:132.5},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:215.2,x:172,y:131.6},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:217.1,x:171.2,y:131.1},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:218.5,x:170.6,y:130.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:219.6,x:170.2,y:130.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:220.3,x:169.9,y:130.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:220.7,x:169.7,y:130.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:221,x:169.6,y:130},0).wait(1).to({scaleX:1,scaleY:1,rotation:221.2,x:169.5},0).wait(1).to({x:169.4,y:129.9},0).to({alpha:0},10).wait(22));

	// piu bianco
	this.instance_2 = new lib.piubianco();
	this.instance_2.parent = this;
	this.instance_2.setTransform(517.7,253.8,0.525,0.525,-76.7,0,0,19.1,19.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).wait(1).to({scaleX:0.81,scaleY:0.81,rotation:-30.3,x:553.9,y:247.8},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-19.2,x:562.7,y:246.3},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:-12.9,x:567.6,y:245.5},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-8.8,x:570.8,y:245},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-6.1,x:572.9,y:244.6},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-4.1,x:574.4,y:244.4},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-2.7,x:575.5,y:244.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.7,x:576.4,y:244.1},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1,x:576.9,y:244},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.5,x:577.3,y:243.9},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,x:577.5},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:577.6},0).wait(1).to({x:577.7},0).to({alpha:0},10).wait(22));

	// piu rosa copy
	this.instance_3 = new lib.piurosa();
	this.instance_3.parent = this;
	this.instance_3.setTransform(58.5,252.5,0.513,0.513,-67.7,0,0,19,19.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(13).to({_off:false},0).wait(1).to({regX:19.1,regY:19.1,scaleX:0.77,scaleY:0.77,rotation:-31.6,x:48.6,y:231.2},0).wait(1).to({scaleX:0.85,scaleY:0.85,rotation:-20.9,x:45.7,y:224.9},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-14.2,x:43.8,y:221},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-9.7,x:42.6,y:218.3},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-6.4,x:41.8,y:216.4},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-4.1,x:41.1,y:215.1},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-2.4,x:40.7,y:214},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.3,x:40.3,y:213.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.5,x:40.1,y:213},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1,y:212.7},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:39.9},0).to({alpha:0},7).wait(20));

	// piu rosa
	this.instance_4 = new lib.piurosa();
	this.instance_4.parent = this;
	this.instance_4.setTransform(451.4,194.5,0.513,0.513,-67.7,0,0,19,19.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(13).to({_off:false},0).wait(1).to({regX:19.1,regY:19.1,scaleX:0.77,scaleY:0.77,rotation:-31.6,x:467.4,y:183.7},0).wait(1).to({scaleX:0.85,scaleY:0.85,rotation:-20.9,x:472.1,y:180.5},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-14.2,x:475.1,y:178.6},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-9.7,x:477.1,y:177.2},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-6.4,x:478.6,y:176.3},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-4.1,x:479.6,y:175.6},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-2.4,x:480.3,y:175.1},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.3,x:480.8,y:174.8},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.5,x:481.2,y:174.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1,x:481.4,y:174.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:481.3,y:174.5},0).to({alpha:0},7).wait(20));

	// Singolo bianco
	this.instance_5 = new lib.Singolobianco();
	this.instance_5.parent = this;
	this.instance_5.setTransform(99.5,173,0.308,0.308,0,0,0,13,13);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off:false},0).wait(1).to({scaleX:0.84,scaleY:0.84,x:68.7,y:142.2,alpha:0.769},0).wait(1).to({scaleX:0.93,scaleY:0.93,x:63.6,y:137.1,alpha:0.897},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:61.4,y:134.9,alpha:0.953},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:60.3,y:133.8,alpha:0.979},0).wait(1).to({scaleX:1,scaleY:1,x:59.8,y:133.3,alpha:0.992},0).wait(1).to({scaleX:1,scaleY:1,x:59.6,y:133.1,alpha:0.998},0).wait(1).to({scaleX:1,scaleY:1,x:59.5,y:133,alpha:1},0).wait(1).to({alpha:0},9).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	id: '28E07DA412F043DCBD4E381D9CF6CC6B',
	width: 750,
	height: 517,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['28E07DA412F043DCBD4E381D9CF6CC6B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;