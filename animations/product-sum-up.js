(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Volo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AhZi2ICzFt");
	this.shape.setTransform(164.8,99.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AA2BBIhriB");
	this.shape_1.setTransform(179,123.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("Ah+B6IAUjuIDuAV");
	this.shape_2.setTransform(184.2,128.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3E3E").s().p("AhHBKQgegfAAgrQAAgqAegfQAegeApAAQAqAAAeAeQAeAfAAAqQAAArgeAfQgeAegqAAQgpAAgegeg");
	this.shape_3.setTransform(69,48.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AFVAAQAACRhkBmQhkBniNAAQiMAAhkhnQhkhmAAiRQAAiPBkhnQBkhmCMAAQCNAABkBmQBkBmAACQg");
	this.shape_4.setTransform(69,48.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AjwD3QhkhmAAiRQAAiPBkhnQBkhmCMgBQCNABBkBmQBkBmAACQQAACRhkBmQhkBniNAAQiMAAhkhng");
	this.shape_5.setTransform(69,48.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FC3065").s().p("AslHoQjKAAiPiPQiPiPAAjKQAAjJCPiPQCPiPDKAAMAgzAAAQAADGhMC2QhLCuiHCHQiHCHivBLQi1BMjHAAg");
	this.shape_6.setTransform(148.3,48.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("ACojGIlgDwIF2Cfg");
	this.shape_7.setTransform(18.5,59.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ai7ApIFgjwIAXGPg");
	this.shape_8.setTransform(18.8,59.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AhZi2ICzFt");
	this.shape_9.setTransform(215,99.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AA2BBIhriB");
	this.shape_10.setTransform(229.3,123.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("Ah+B6IAVjuIDtAV");
	this.shape_11.setTransform(234.4,128.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Volo, new cjs.Rectangle(-1,0,278.7,142.1), null);


(lib.Viola = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("Ag4hzIBxDn");
	this.shape.setTransform(104.4,63);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AAiApIhDhR");
	this.shape_1.setTransform(113.4,78.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AhPBOIAOiWICWAN");
	this.shape_2.setTransform(116.6,81.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3E3E").s().p("AgtAvQgSgUAAgbQAAgaASgUQATgTAaAAQAaAAATATQATAUAAAaQAAAbgTAUQgTATgaAAQgaAAgTgTg");
	this.shape_3.setTransform(43.7,30.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("ADYAAQAABcg/BBQhABBhZAAQhZAAg/hBQg/hBAAhcQAAhbA/hBQBAhBBYAAQBZAABABBQA/BBAABbg");
	this.shape_4.setTransform(43.7,30.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AiXCdQhAhBAAhcQAAhaBAhCQA/hBBYABQBZgBBABBQA/BCAABaQAABcg/BBQhABBhZgBQhZABg+hBg");
	this.shape_5.setTransform(43.7,30.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5F2CCC").s().p("An+E1QiAAAhahaQhbhbAAiAQAAh/BbhbQBahaCAAAIUyAAQAAEAi1C0Qi2C1kAAAg");
	this.shape_6.setTransform(93.9,30.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("ABrh9IjeCXIDsBlg");
	this.shape_7.setTransform(11.6,37.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ah2AaIDfiYIAOD9g");
	this.shape_8.setTransform(11.9,37.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("Ag4hzIBxDn");
	this.shape_9.setTransform(136.2,63);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AAiApIhEhR");
	this.shape_10.setTransform(145.2,78.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("AhPBOIANiWICWAN");
	this.shape_11.setTransform(148.4,81.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Viola, new cjs.Rectangle(-1,0,176.9,90.4), null);


(lib.umbrella = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1A1A").ss(1.2).p("EAmmAN3MhNLAAAQAAlpDClJQC7k+FWj1QFWj2G7iHQHLiLH2AAQH3AAHKCLQG8CHFWD2QFVD1C8E+QDCFJAAFpg");
	this.shape.setTransform(247,88.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AoPN3QgBreCcoHQBIj2BfiHQBjiLBqAAQBrAABjCLQBfCHBJD2QCbIGgBLfg");
	this.shape_1.setTransform(247,88.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("A5QN3QAAlpB/lJQB7k+Dfj1QDhj2EhiHQEtiLFIAAQFJAAEsCLQEjCHDfD2QDgD1B7E+QB/FJAAFpg");
	this.shape_2.setTransform(247,88.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FC3065").s().p("EgmlAN3QAAlpDClJQC8k+FVj1QFXj2G6iHQHMiLH1AAQH2AAHLCLQG8CHFVD2QFXD1C7E+QDCFJAAFpg");
	this.shape_3.setTransform(247,88.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.umbrella, new cjs.Rectangle(-1,-1,496,179.5), null);


(lib.Pioggia = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E6E6E6").s().p("A0FFIQAAiiBzhyQByhyCiAAQAtAAAsAKQAjAJAigKQAigJAZgaQBMhOBlgwQCDg9CRAFQCLAGB6BDQA0AcA8gEQA8gEAxgiQBRg4BdgdQBggfBmAAQEQAADADAQDAC/AAEQg");
	this.shape.setTransform(128.6,294.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B0B0AF").s().p("AseDLQAAhkBHhHQBIhGBkAAQAeABAaAFQAVAFAVgFQAWgGAPgQQAwgxA+gdQBRgmBbADQBWAEBMApQAgASAlgDQAlgCAegWQAzgiA6gTQA7gSBAgBQCoAAB4B4QB2B2ABCog");
	this.shape_1.setTransform(119,-26.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("AyGEoQAAiSBmhnQBohmCSAAQAqAAAmAJQAfAHAggIQAegJAXgXQBFhHBagqQB3g4CCAFQB+AFBuA9QAvAZA2gEQA1gDAsgfQBKgyBUgbQBWgcBdAAQD0AACtCuQCtCsAAD1g");
	this.shape_2.setTransform(569.4,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Pioggia, new cjs.Rectangle(0,-47.1,685.3,374.4), null);


(lib.Goccia = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1A1A").ss(2,1,1).p("ACeCWQAABCguAuQgvAuhBAAQhAAAgvguQguguAAhCQAAg2BPjOQAnhoAnhdQAnBdAoBoQBPDOAAA2g");
	this.shape.setTransform(15.8,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Goccia, new cjs.Rectangle(-1,-1,33.6,63.6), null);


(lib.Path_3_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C4C4C4").s().p("AgmCyIAAljIBNAAIAAFjg");
	this.shape.setTransform(3.9,17.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3_2, new cjs.Rectangle(0,0,7.8,35.6), null);


(lib.Path_3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F2F2F2").s().p("Ag3DAIAAl/IBvAAIAAF/g");
	this.shape.setTransform(5.6,19.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3_1, new cjs.Rectangle(0,0,11.2,38.4), null);


(lib.Path_3_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F2F2F2").s().p("Ag3DAIAAl/IBvAAIAAF/g");
	this.shape.setTransform(5.6,19.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3_0, new cjs.Rectangle(0,0,11.2,38.4), null);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F2F2F2").s().p("Ag3DAIAAl/IBvAAIAAF/g");
	this.shape.setTransform(5.6,19.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(0,0,11.2,38.4), null);


(lib.sole = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Pioggia();
	this.instance.parent = this;
	this.instance.setTransform(-482,126.1,1,1,0,0,0,342.7,163.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("EAiiAEkQhlAAhhAfQhdAdhRA4QgxAig7AEQg8AEg0gcQh7hDiMgFQiQgGiEA+QhkAvhNBPQgYAZgjAJQgiAKgjgIQgrgLguAAQiiAAhyBzQhzByAACiMAoMAAAQAAkQjAjAQhwhwiMgvQg6gTg+gIQgtgGgvAAgAu1uzQhmAAhhAeQhdAehRA4QgxAig7AEQg8AEg0gcQh7hDiMgGQiQgFiEA9QhkAwhNBOQgYAZgjAKQgiAJgjgIQgqgKgvAAQiiAAhyByQhyBzAACiMAoMAAAQAAkQjAjAQjBjAkPAAg");
	this.shape.setTransform(286.6,94.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AmSGTQininAAjsQAAjrCninQCninDrAAQDsAACnCnQCnCnAADrQAADsinCnQinCnjsAAQjrAAining");
	this.shape_1.setTransform(518.1,110.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.sole, new cjs.Rectangle(-824.7,-84.7,1399.8,374.4), null);


(lib.pioggiagocce = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 copy 2
	this.instance = new lib.Goccia();
	this.instance.parent = this;
	this.instance.setTransform(-19.9,273.5,0.545,0.545,0,0,0,15.8,30.7);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(17).to({_off:false},0).to({regX:15.6,regY:30.8,scaleX:0.95,scaleY:0.95,x:-20,y:313.6,alpha:1},5).to({regX:15.7,scaleX:0.9,scaleY:0.9,y:432.6,alpha:0},11).wait(1));

	// Layer 2 copy
	this.instance_1 = new lib.Goccia();
	this.instance_1.parent = this;
	this.instance_1.setTransform(54.1,-94.5,0.545,0.545,0,0,0,15.8,30.7);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:15.7,regY:30.8,scaleX:0.59,scaleY:0.59,x:54,y:-54.4,alpha:1},12).to({scaleX:0.53,scaleY:0.53,y:-10.4,alpha:0},8).to({_off:true},6).wait(4));

	// Layer 2
	this.instance_2 = new lib.Goccia();
	this.instance_2.parent = this;
	this.instance_2.setTransform(574.1,-14.5,0.545,0.545,0,0,0,15.8,30.7);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regY:30.8,scaleX:1,scaleY:1,y:105.6,alpha:1},5).to({y:445.6,alpha:0},24).to({_off:true},1).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-98.6,596.9,484.8);


// stage content:
(lib._3productsumup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

    window.StopAnimation = function () {
        // console.log('executing StopAnimation()');
        this.stop();
    }.bind(this);
    window.StartAnimation = function () {
        // console.log('executing StartAnimation()');
        this.play();
    }.bind(this);

    // timeline functions:
    this.frame_143 = function() {
        this.gotoAndPlay(89
        );
    }

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(143).call(this.frame_143).wait(1));

	// gocce
	this.instance = new lib.pioggiagocce();
	this.instance.parent = this;
	this.instance.setTransform(76,225.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(40).to({_off:false},0).wait(104));

	// Layer 12
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("AgNEBQhLgDgzgWQhDgcgag5QgNgdgEg4QgEhNAQg4QAVhIA0gmQANgJAXgNIAlgUIAcgTQARgLAOgCQAIgCANABIAUAAQAPABAegCQAuABApAaQAoAaAUApIASApIAMAZIAKAYQAKAbAAAkQAAAWgGApQgEAlgFAUQgHAfgMAWQgfA3hGAYQgtAQg9AAIgXgBg");
	this.shape.setTransform(322.1,145.8);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(16).to({_off:false},0).wait(3).to({x:317.6,y:144.8},0).to({_off:true},2).wait(123));

	// viola
	this.instance_1 = new lib.Viola();
	this.instance_1.parent = this;
	this.instance_1.setTransform(863.5,149.3,1,1,0,0,0,87.5,44.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:87.4,regY:44.7,x:609.2,y:154.2},0).wait(1).to({x:535.3,y:155.7},0).wait(1).to({x:489.5,y:156.6},0).wait(1).to({x:457.7,y:157.3},0).wait(1).to({x:434.3,y:157.7},0).wait(1).to({x:416.7,y:158.1},0).wait(1).to({x:403.2,y:158.4},0).wait(1).to({x:392.9,y:158.6},0).wait(1).to({x:384.9,y:158.7},0).wait(1).to({x:378.9,y:158.8},0).wait(1).to({x:374.4,y:158.9},0).wait(1).to({x:371.1,y:159},0).wait(1).to({x:368.8},0).wait(1).to({x:367.3,y:159.1},0).wait(1).to({x:366.5},0).wait(1).to({regX:87.5,regY:44.9,x:366.4,y:159.3},0).wait(1).to({regX:87.4,regY:44.7,x:366.2,y:159.1},0).wait(1).to({x:365.8},0).wait(1).to({x:365.2},0).wait(1).to({x:364.2},0).wait(1).to({x:363,y:159.2},0).wait(1).to({x:361.3},0).wait(1).to({x:359.4},0).wait(1).to({x:357,y:159.3},0).wait(1).to({x:354.1},0).wait(1).to({x:350.7,y:159.4},0).wait(1).to({x:346.8,y:159.5},0).wait(1).to({x:342.3,y:159.6},0).wait(1).to({x:337,y:159.7},0).wait(1).to({x:330.9,y:159.8},0).wait(1).to({x:324,y:160},0).wait(1).to({x:316,y:160.1},0).wait(1).to({x:306.8,y:160.3},0).wait(1).to({x:296.2,y:160.5},0).wait(1).to({x:283.8,y:160.8},0).wait(1).to({x:269.4,y:161},0).wait(1).to({x:252.3,y:161.4},0).wait(1).to({x:231.9,y:161.8},0).wait(1).to({x:206.9,y:162.3},0).wait(1).to({x:175.2,y:162.9},0).wait(1).to({x:132.9,y:163.8},0).wait(1).to({x:69.3,y:165.1},0).wait(1).to({regX:87.5,regY:44.9,x:-130.7,y:169.3},0).wait(101));

	// cloud
	this.instance_2 = new lib.sole();
	this.instance_2.parent = this;
	this.instance_2.setTransform(380.7,261.6,1,1,0,0,0,287.3,94.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(20).to({regX:-124.9,regY:102.5,x:-25.4,y:269.3},0).wait(1).to({x:-4.7,y:269.2},0).wait(1).to({x:34.8,y:269.1},0).wait(1).to({x:97.6,y:269},0).wait(1).to({x:184.7,y:268.7},0).wait(1).to({x:287.9,y:268.5},0).wait(1).to({x:390.6,y:268.2},0).wait(1).to({x:479.9,y:267.9},0).wait(1).to({x:551.6,y:267.8},0).wait(1).to({x:607,y:267.6},0).wait(1).to({x:648.7,y:267.5},0).wait(1).to({x:679.2,y:267.4},0).wait(1).to({x:700.7},0).wait(1).to({x:714.6,y:267.3},0).wait(1).to({x:722.3},0).wait(1).to({regX:287.3,regY:94.8,x:1136.9,y:259.6},0).wait(109));

	// Volo
	this.instance_3 = new lib.Volo();
	this.instance_3.parent = this;
	this.instance_3.setTransform(852.5,459.6,0.662,0.662,0,0,0,138.3,70.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(51).to({_off:false},0).wait(1).to({regY:70.5,x:557.7,y:459.4},0).wait(1).to({x:513.3},0).wait(1).to({x:490},0).wait(1).to({x:476.2},0).wait(1).to({x:467.8},0).wait(1).to({x:462.8},0).wait(1).to({x:460.2},0).wait(1).to({regY:70.8,x:459.5,y:459.6},0).wait(1).to({regY:70.5,y:460.9},0).wait(1).to({y:465.7},0).wait(1).to({y:473.7},0).wait(1).to({y:484.9},0).wait(1).to({regY:70.8,y:499.6},0).to({_off:true},1).wait(79));

	// fuxia
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AAihiIhDDF");
	this.shape_1.setTransform(436.1,531.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AgDArIAHhV");
	this.shape_2.setTransform(432.4,545.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#222221").p("AhWAiIBdhOIBPBd");
	this.shape_3.setTransform(432.1,545.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3F3E3E").s().p("AgrAsQgSgSAAgaQAAgZASgSQASgSAZAAQAaAAASASQASASAAAZQAAAagSASQgSASgaAAQgZAAgSgSg");
	this.shape_4.setTransform(448.2,444.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#222221").p("ADQAAQAABWg9A9Qg9A9hWAAQhVAAg8g9Qg+g9AAhWQAAhVA+g9QA8g9BVAAQBWAAA9A9QA9A9AABVg");
	this.shape_5.setTransform(448.2,445.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AiSCTQg8g9gBhWQABhVA8g9QA9g9BVAAQBWAAA9A9QA8A9ABBVQgBBWg8A9Qg9A8hWAAQhVAAg9g8g");
	this.shape_6.setTransform(448.2,445.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FC3065").s().p("AlIJMQhRAAhDgtQhBgsgehIQgUgwAAg1IAAgNIABgEIgBAAIAAosQAAiOBmhkQBlhkCOACQCMACBhBmQBhBlAACMIAAExIH4AAQAADaiaCaQiZCZjaAAg");
	this.shape_7.setTransform(473.4,472.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#222221").p("ABrh9IjfCYIDtBkg");
	this.shape_8.setTransform(412.6,451.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ah2AaIDfiYIAOD9g");
	this.shape_9.setTransform(412.8,451.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AAihiIhDDF");
	this.shape_10.setTransform(465.8,533.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("AgDArIAHhW");
	this.shape_11.setTransform(462.1,547.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#222221").p("AhWAiIBdhOIBPBd");
	this.shape_12.setTransform(461.8,547.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]},65).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]},47).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]},3).wait(29));

	// Layer 20 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_13 = new cjs.Graphics().p("AiqDqQgPgGAAgPQAAgHAEgGQAEgGAHgDQAEgCAJAAQAKgBAEgBIAGgEQAMgFAPAFQALAEALALQAHAJABAHQADAKgHAGQgFAGgPAAIgyAAIgFAAQgGAAgFgCgAg9gnQgQgIgIgDIgPgEQgJgDgDgFQgEgFAAgLQAAgQAGgHQAGgFAKgDIAXgHQAIABAFAGQAFAHgBAIQgDAGABACQABADAIAEQANAGAEALQAFANgKAJQgFAEgHAAQgGAAgIgDgACbiYQgFgFgCgHQgBgFAAgIIAAgOQgBgUADgJQABgGAEgEQAFgEAFAAQAFAAgDgBQAMABAFANQADAHAAAQIAAARQAAAOgDAHQgFANgLAAIgBAAQgGAAgFgFg");
	var mask_graphics_14 = new cjs.Graphics().p("AjHELQgEgCgJgKQgGgKAAgGQADgIAAgEIgDgLQgBgKALgFQAGgCAMgBQA5gBAVAEQAJACAHADQAJgBALADQAKAEALAMQAIAJABAGQACAKgGAHQgGAFgPAAIguAAIgGAFQgFAEgOAAIgjAAQgPAAgHgDgAgcgNIgBgBQgPgIgIgCIgPgEQgIgDgEgFQgEgGAAgLIABgIIgGgEIgMgKQgQgNAAgNQgBgIAGgHQAGgHAIgBQAIABAIAIIAOAOIATAOIAKgDQAIAAAFAHQAEAEAAAFIAEABQAFACADADQAFAFAAAJIAAAGQAEAFACAGQAGAOgKAJQgGAEgIAAIgCAAQgFAAgFgDgADEhxQgKgBgDgMQgCgFAAgMIgBgJIABgOQgBgNABgIIAAg2QAAgVAKgGQAGgDAIAEQAGADAEAGQAEAIAAATIAABSQAAAPgDAKQgDAFgFAEQgFADgEAAIgDgBg");
	var mask_graphics_15 = new cjs.Graphics().p("AitExIgHgFQgOAAgSgEQgKgBgRgEQgMgCgGgGQgHgIAGgLQADgFAHgEIAMgGIAOgKQANgLAUgBQAMAAAGAGIADADIAEAAQA5gBAVAEQAJACAHAEQAJgCALAEQAKAEALALQAIAJABAHQACAKgGAGQgGAGgPAAIguAAIgGAFQgFADgOAAIgjABQgPAAgHgEgAgCAYIgBgBQgPgIgIgCIgPgEQgIgDgEgFQgCgCgBgFQgIgCgIgKQgMgNgFgRQgLgFgRgRIgGgGQgFACgHAAQgPgBgGgKQgDgFABgJIAAgPIAAgMQAAgGACgEQAEgFAIgDQAPgEAIADQAIACAJAJIAPAPIAPAQQACADAEAJIAIAKIAFAEIAOAOIATAOIAKgDQAIABAEAGQAEAFAAAFIAEABQAFACADADQAFAEAAAJIAAAHQAFAEACAGQAGANgLAJQgGAFgIgBIgCAAQgEAAgFgDgADehKQgKgCgDgLQgCgGAAgMIgBgIIABgOIgFACQgIAAgGgHQgGgGgBgIQgBgIAGgZQAFgTgBgdIgBgwQABgcAOgEQARgEAMAZQAQAgAEAmQABAUADAHIAFATQgBAGgEAFQgFAGgHABIAAAqQAAAPgDAJQgDAGgFADQgFADgEAAIgDAAg");
	var mask_graphics_16 = new cjs.Graphics().p("AiCFPIgHgGQgNAAgPgDQgHADgKAAIgzAAQgrABgSgOQgJgIAAgGQgBgLAPgHQAHgCAQgCIARgGIAZgCQAQAAASgGQAFABADADIACgCQANgLAUgBQAMAAAGAGIAEAEIAEgBQA4gBAVAFQAKACAHADQAJgBAKADQAKAEALALQAHAKABAGQADAKgHAHQgFAFgOAAIguAAIgGAFQgGAEgNgBIgjABQgPAAgHgDgAAoA2IgBgBQgPgIgIgCIgPgFQgIgCgDgGQgDgDAAgFQgIgBgJgLQgMgMgEgQQgLgGgSgRIgFgGQgFACgHABQgPgCgGgJQgDgGAAgJIABgLIgCgBIgWgJIgMgFQgLgBgDgCQgIgDgDgMIAAgUQAAgPABgEQAEgMAKAAQAFAAAAgDQASAFAPANIAPAOIARAKQAGAEADAFIADABQAJACAJAJIAPAQIAOAPQADADADAKIAJAJIAEAEIANAOIATAPIALgEQAIABAFAGQADAFABAFIADABQAGACADADQAEAEAAAIIAAAHQAGAFACAGQAFAOgKAJQgGAEgJgBIgBABQgFAAgFgDgAEJgtQgKgBgEgMQgCgGAAgLIAAgJIAAgNIgFABQgIABgGgIQgGgFgBgJQAAgHAFgaQAFgTAAgcIgCgxQABgJABgGIgBgOIAAgiQAAgPAFgHQAFgGAJAAQAKAAAHAJQAHAHADALQACAIgBAMIgBAVIAAARQAOAeAEAjQABAUACAHIAFATQAAAGgFAFQgFAGgGABIAAAqQAAAPgEAJQgDAGgFADQgEADgEAAIgDAAg");
	var mask_graphics_17 = new cjs.Graphics().p("AkXGRQgNgBgRgFQgQgFgFgIQgHgMAHgMQAFgKANgEQgIgIgDgOQgCgJAAgRQABgUALgDQAJgBAAgEQAMAKAJANIAIAOQAEAHAFAFIAIAFQAIgCAOgCIARgGIAZgCQAQAAASgFQAFAAADADIACgCQANgKAUgBQAMAAAGAFIAEAEIAEAAQA4gBAVAEQAKACAGAEQAJgCAKADQALAEALAMQAHAJABAGQADAKgHAHQgFAFgPAAIgtAAIgGAFQgGAEgNAAIgjAAQgPAAgHgDIgHgGQgNAAgPgCQgHACgKAAIgrABIgBABQgDANgOAIQgUAMgdAAIgRgBgABGBcIgBgBQgPgIgIgCIgPgEQgJgDgDgFQgCgDgBgGQgIgBgIgKQgMgOgEgQQgLgFgSgRIgFgFQgFACgHAAQgPgBgGgKQgDgGAAgJIABgLIgCgBIgWgJIgMgEQgLgBgDgDQgIgDgDgMIAAgUIAAgDQgMgHgQgLIgjgYIgMgGQgGgEgEgEQgLgLAFgMQADgHAJgCQAHgDAJACIAOAGIAPAGQgEgPAMgJQALgKAOAFQAHAEANAMIAPAKQAJAHACAHQACAJgHAHQgHAIgJgCIAPARIAFAIIAJAHIAPANIARAKQAGAEADAGIADAAQAJACAJAJIAPAQIAOAPQACAEADAJIAJAKIAEAEIAOANIATAOIALgDQAIAAAFAHQAEAEAAAGIADAAQAGACADADQAEAFAAAJIAAAGQAGAFACAGQAFAOgKAJQgGAEgJAAIgBAAQgFAAgFgDgAEngHQgKgBgEgMQgBgFgBgMIAAgJIAAgNIgFABQgIABgGgHQgGgGgBgJQAAgHAFgZQAFgUAAgcIgCgxQABgIABgHIgBgOIAAghQAAgOAEgHIgBgCQgDgJgBgXQAAgWACgLQACgTAHgMQAGgKAHgBIAFgBQAKgBAFALQADAHAAAMIABBkIAAAEQABAHgBAJIgBAVIAAASQAOAeAEAiQABAUACAHIAFATQAAAHgFAFQgFAFgGABIAAAqQAAAPgEAKQgDAFgFAEQgEADgEAAIgDgBg");
	var mask_graphics_18 = new cjs.Graphics().p("AkXGRQgNgBgRgFQgQgFgFgIQgHgMAHgMQAFgKANgEQgIgIgDgOQgCgJAAgRQABgUALgDQAJgBAAgEQAMAKAJANIAIAOQAEAHAFAFIAIAFQAIgCAOgCIARgGIAZgCQAQAAASgFQAFAAADADIACgCQANgKAUgBQAMAAAGAFIAEAEIAEAAIAyAAIAAA7IABANIgDAAIgjAAQgPAAgHgDIgHgGQgNAAgPgCQgHACgKAAIgrABIgBABQgDANgOAIQgUAMgdAAIgRgBgAATBFQgCgDgBgGQgIgBgIgKQgMgOgEgQQgLgFgSgRIgFgFQgFACgHAAQgPgBgGgKQgDgGAAgJIABgLIgCgBIgWgJIgMgEQgLgBgDgDQgIgDgDgMIAAgUIAAgDQgMgHgQgLIgjgYIgMgGQgGgEgEgEQgLgLAFgMQADgHAJgCQAHgDAJACIAOAGIAPAGQgEgPAMgJQALgKAOAFQAHAEANAMIAPAKQAJAHACAHQACAJgHAHQgHAIgJgCIAPARIAFAIIAJAHIAPANIARAKQAGAEADAGIADAAQAJACAJAJIAPAQIAOAPQACAEADAJIAJAKIAEAEIAOANIATAOIALgDIADAAQgXAZgUAfIgBgCgAD9hPQAAgHAFgZQAFgUAAgcIgCgxQABgIABgHIgBgOIAAghQAAgOAEgHIgBgCQgDgJgBgXQAAgWACgLQACgTAHgMQAGgKAHgBIAFgBQAKgBAFALQADAHAAAMIABBkIAAAEQABAHgBAJIgBAVIAAASQAOAeAEAiQABAUACAHIAFATQAAAHgFAFQgFAFgGABIAAABIgHABQgOACgbAHIgOADIgDgIg");
	var mask_graphics_19 = new cjs.Graphics().p("AkRGRQgNgBgSgFQgQgFgFgIQgGgMAGgMQAFgKANgEQgIgIgCgOQgCgJAAgRQABgUAKgDQAJgBAAgEQAMAKAJANIAIAOQAEAHAFAFIAIAFQAIgCAOgCIARgGIAZgCQAQAAATgFQAEAAAEADIABgCQAJgHAMgCIgMBFIgLABIgrABIAAABQgEANgOAIQgUAMgcAAIgRgBgAgngDIgGgFQgFACgHAAQgPgBgGgKQgDgGAAgJIABgLIgCgBIgVgJIgNgEQgLgBgDgDQgHgDgEgMIAAgUIAAgDQgMgHgQgLIgjgYIgMgGQgGgEgEgEQgLgLAFgMQAEgHAIgCQAIgDAIACIAOAGIAPAGQgDgPALgJQALgKAOAFQAHAEAOAMIAOAKQAKAHABAHQADAJgIAHQgHAIgJgCIAPARIAFAIIAJAHIAPANIARAKQAGAEADAGIADAAQAJACAJAJIAPAQIANAPIAGAKQgSAQgPAUIgOgNgAEMifIgBgxQAAgIABgHIAAgOIAAghQAAgOADgHIgBgCQgCgJgBgXQgBgWACgLQACgTAIgMQAFgKAHgBIAFgBQAKgBAGALQACAHAAAMIABBkIAAAEQABAHgBAJIgBAVIAAASQAMAYAEAcQgLABgNAFQgMAFgRAJIgIAEIAAgWg");
	var mask_graphics_20 = new cjs.Graphics().p("AkKGRQgNgBgSgFQgQgFgFgIQgGgMAGgMQAFgKAOgEQgJgIgCgOQgCgJAAgRQABgUAKgDQAKgBgBgEQANAKAIANIAIAOQAFAHAEAFIAJAFQAHgCAOgCIASgGIAYgCIAGAAIgEAfIgEAfQgEALgNAHQgUAMgcAAIgRgBgAgggDIgGgFQgFACgHAAQgPgBgGgKQgDgGABgJIABgLIgDgBIgVgJIgNgEQgLgBgDgDQgHgDgDgMIAAgUIAAgDQgMgHgRgLIgjgYIgLgGQgHgEgDgEQgMgLAGgMQADgHAIgCQAIgDAIACIAPAGIAOAGQgDgPALgJQALgKAOAFQAIAEANAMIAPAKQAJAHACAHQACAJgIAHQgHAIgJgCIAPARIAGAIIAIAHIAPANIARAKQAHAEADAGIADAAQAIACAJAJIAPAQIAOAPIABACQgXAQgTARIgBgCgAESjQQAAgIACgHIgBgOIAAghQAAgOAEgHIgBgCQgDgJgBgXQAAgWABgLQACgTAIgMQAFgKAHgBIAFgBQAKgBAGALQADAHAAAMIAABkIAAAEQABAHAAAJIgBAVIAAALIgvAfIAAgTg");
	var mask_graphics_21 = new cjs.Graphics().p("AjmF4IgBAHQgCAEgDAFIAGgQgAiPgqIgNgEQgLgBgDgDQgHgDgDgMIAAgUIAAgDQgNgHgQgLIgjgYIgLgGQgHgEgDgEQgMgLAGgMQADgHAIgCQAIgDAIACIAPAGIAOAGQgDgPALgJQALgKAOAFQAIAEANAMIAPAKQAJAHACAHQACAJgIAHQgHAIgJgCIAPARIAFAIIAJAHIAPANIAGADQgRAUgQAXgADkkDQAAgOADgHIAAgCQgDgJgBgXQAAgWABgLQACgTAIgMQAFgKAHgBIAFgBQAKgBAGALQADAHAAAMIAABMIguAag");
	var mask_graphics_22 = new cjs.Graphics().p("AAVEMIgBAHQgCAFgDAEIAGgQgAgTkSQADgHAHgCIgLAKIABgBg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_graphics_13,x:139,y:716.6}).wait(1).to({graphics:mask_graphics_14,x:135.6,y:714}).wait(1).to({graphics:mask_graphics_15,x:133,y:710.2}).wait(1).to({graphics:mask_graphics_16,x:128.8,y:707.3}).wait(1).to({graphics:mask_graphics_17,x:125.8,y:703.4}).wait(1).to({graphics:mask_graphics_18,x:125.8,y:703.4}).wait(1).to({graphics:mask_graphics_19,x:125.3,y:703.4}).wait(1).to({graphics:mask_graphics_20,x:124.5,y:703.4}).wait(1).to({graphics:mask_graphics_21,x:129.2,y:702.3}).wait(1).to({graphics:mask_graphics_22,x:104,y:713.1}).wait(1).to({graphics:null,x:0,y:0}).wait(121));

	// sparks
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(1,1,1).p("AAACiIAAlD");
	this.shape_13.setTransform(111.5,736.8,1,1,-90);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(1,1,1).p("AihAAIFDAA");
	this.shape_14.setTransform(154.7,682.5,1,1,-90);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(1,1,1).p("Ah1CEIDskH");
	this.shape_15.setTransform(119.5,698.2,1,1,-90);

	var maskedShapeInstanceList = [this.shape_13,this.shape_14,this.shape_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13}]},13).to({state:[]},10).wait(121));

	// Grigio
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#222221").p("AAihiIhDDF");
	this.shape_16.setTransform(166.7,854.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#222221").p("AgDArIAHhW");
	this.shape_17.setTransform(163,868.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#222221").p("AhWAiIBdhOIBPBd");
	this.shape_18.setTransform(162.7,868.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#3F3E3E").s().p("AgrAsQgSgSAAgaQAAgZASgSQASgSAZAAQAaAAASASQASASAAAZQAAAagSASQgSASgaAAQgZAAgSgSg");
	this.shape_19.setTransform(178.8,766.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#222221").p("ADQAAQAABWg9A9Qg9A9hWAAQhVAAg9g9Qg9g9AAhWQAAhVA9g9QA9g9BVAAQBWAAA9A9QA9A9AABVg");
	this.shape_20.setTransform(178.8,768.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AiSCTQg9g9AAhWQAAhVA9g9QA9g9BVAAQBWAAA9A9QA9A9AABVQAABWg9A9Qg9A9hWAAQhVAAg9g9g");
	this.shape_21.setTransform(178.8,768.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#222221").p("AFSA/IAAABIj6AAIAAkyQAAiMhhhlQhhhmiMgCQiOgChmBkQhlBjAACPIAAIsIAAAAIAAAEIAAANQAAA1AUAwQAeBIBBAsQBCAuBRAAIGLAAQDaAACaiaQCaiaAAjag");
	this.shape_22.setTransform(204.1,795.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E6E6E6").s().p("AlJJNQhRAAhCguQhBgsgehIQgUgwAAg1IAAgNIAAgEIAAAAIAAosQAAiPBlhjQBmhkCOACQCMACBiBmQBgBlAACMIAAEyID6AAIAAgBID+AAQAADaiaCaQiaCajaAAg");
	this.shape_23.setTransform(204.1,795.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#222221").p("ABrh9IjeCXIDsBlg");
	this.shape_24.setTransform(143.2,774.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ah2AaIDfiYIAOD9g");
	this.shape_25.setTransform(143.4,774.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#222221").p("AAihiIhDDF");
	this.shape_26.setTransform(196.4,856.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#222221").p("AgDArIAHhV");
	this.shape_27.setTransform(192.7,870.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#222221").p("AhWAiIBdhOIBPBd");
	this.shape_28.setTransform(192.4,870.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#3F3E3E").s().p("AgrAsQgSgSAAgaQAAgZASgRQASgTAZAAQAaAAASATQASARAAAZQAAAagSASQgSASgaAAQgZAAgSgSg");
	this.shape_29.setTransform(178.8,759.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_29},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},12).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},76).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},17).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},3).wait(36));

	// Layer 7
	this.instance_4 = new lib.umbrella();
	this.instance_4.parent = this;
	this.instance_4.setTransform(359.9,280.6,0.699,1,0,0,0,247.1,88.7);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(75).to({_off:false},0).wait(1).to({regX:247,scaleX:0.91,x:359.8},0).wait(1).to({scaleX:0.95},0).wait(1).to({scaleX:0.97,x:359.7},0).wait(1).to({scaleX:0.98,x:359.8},0).wait(1).to({scaleX:0.99},0).wait(1).to({scaleX:1,x:359.7},0).wait(1).to({scaleX:1,x:359.8},0).wait(1).to({scaleX:1,x:359.7},0).wait(1).to({scaleX:1,x:359.8},0).wait(60));

	// Layer 16 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_65 = new cjs.Graphics().p("AgWBAQgagJgMgWQgIgPAAgRQAAgQAHgPQAIgPAOgKQAOgKAQgCQANgCANADQARAFANANQANAMAGARQAHAYgMAYQgLAZgYAKQgMAFgMAAQgLAAgNgFg");
	var mask_1_graphics_66 = new cjs.Graphics().p("AiSBMIhLgDQgYgBgMgDQgUgFgLgLQgRgRgCgiQgDgpATgWQAMgPAUgFQATgFATAFIAaAJQAJADATABQCoAHCZABQAQABALgCIAQgEQALgHANgCIAEAAQAagEAWAMQAbAPAHAeQAEASgGARQgGASgMAOQgXAZgpAIQgXAFgnAAIgKAAQhdAAjMgIg");
	var mask_1_graphics_67 = new cjs.Graphics().p("AiyClQhJgFgxgYQgdgOgIgDQgKgCgPgBIgZgCQg8gFg1grQgrgjgkg7QgbguAFgfQACgRALgOQAKgPAQgHQAPgIASABQARAAAPAJQARAJAPAUQAHAJAQAcQAcAzAgAKQAKADARAAIAcACQAVACAgAQQAlAQAPAEQAXAHAvAAIBSAAQAdAAARgFQAHgCARgIIAKgEQAKgIAOgDQATgGATAGIAaAJQAJABATABQCpAHCZACQAQABALgDIAQgEQALgFANgCIAEAAQAagFAWAMQAbAOAHAfQAEARgGATQgGARgMAOQgXAZgpAJQgXAFgnAAQhbAAjZgIIhLgEIgYgBIgEABQgcAOglAFQgYADguAAQhBAAgWgBg");
	var mask_1_graphics_68 = new cjs.Graphics().p("AhrGGQhJgFgxgYQgdgOgIgDQgKgCgPgBIgZgCQg8gFg1grQgrgjgkg8QgLgTgFgQIgHgEQgWgQgXgsIgfg7QgWgqgGgQQgEgKgHgZQgKghgEgVQgFgaABgmIABhAQABgYgEgSIgGgcQgEgaAQgYQAPgXAZgIQAZgHAaAMQAaALALAYQAHARACAgIAABaQgBA3AHAjQAGAbAOAhQAIATATAmQANAZAJAMIANARQAIAKAEAHQAQAJANATQAHAJAQAcQAcAzAgAKQAKADARAAIAcACQAVACAgAQQAlARAPAEQAXAHAvAAIBRAAQAdAAARgFQAHgCARgIIALgEQAKgJAOgDQATgGATAGIAaAJQAJACATABQCpAHCZACQAQABALgDIAQgEQALgGANgCIAEAAQAagFAWANQAbAOAHAfQAEARgGATQgGARgMAOQgXAZgpAJQgXAFgnAAQhbAAjZgIIhLgEIgYgBIgEABQgcAOgmAFQgYADgtAAQhBAAgWgBg");
	var mask_1_graphics_69 = new cjs.Graphics().p("AhrJnQhJgEgxgYQgdgPgIgCQgKgDgPgBIgZgBQg8gGg1grQgrgjgkg8QgLgTgFgQIgHgEQgWgPgXgtIgfg7QgWgqgGgRQgEgJgHgaQgKgggEgWQgFgaABglIABg/QABgZgEgSIgGgcQgEgaAQgXIAEgGQAAgRACgVQAQiDBVhnIAxg2IAugvQAfgfAVgRQBDg3A/gCQAagBAVAJQAYAKALATQAMASgEAYQgDAYgQAPQgNANgaAJQgdAIgOAGQgrARg1A3QgfAhgXAdQgfAlgQAcQgWAngFAkQgCATAAAnQgCAbgIATIAAAKIAABZQgBA2AHAjQAGAcAOAgQAIAUATAlQANAaAJAMIANARQAIAKAEAIQAQAJANASQAHAKAQAcQAcAzAgAJQAKADARABIAcABQAVADAgAPQAlASAPAEQAXAGAvAAIBRAAQAdAAARgEQAHgCARgIIALgFQAKgIAOgEQATgFATAFIAaAJQAJADATABQCpAHCZABQAQABALgCIAQgFQALgGANgCIAEAAQAagEAWAMQAbAPAHAeQAEASgGASQgGASgMAOQgXAZgpAIQgXAFgnAAQhbABjZgJIhLgDIgYgCIgEACQgcANgmAFQgYADgtABQhBAAgWgCg");
	var mask_1_graphics_70 = new cjs.Graphics().p("AhrKGQhJgFgxgYQgdgOgIgDQgKgCgPgBIgZgCQg8gFg1grQgrgjgkg8QgLgTgFgQIgHgEQgWgQgXgsIgfg7QgWgrgGgQQgEgKgHgZQgKghgEgVQgFgaABgmIABhAQABgXgEgSIgGgcQgEgaAQgYIAEgGQAAgRACgUQAQiDBVhoIAxg1IAugwQAfgfAVgRQBDg2A/gDIALAAQAFgKAIgHQAVgRAxgFQC6gXBlABQAaABAPADQAXAEAPAKQAPALAJARQAJAQgBASQgBATgJAQQgKAQgQAJQgUAMgggBIg4gFQgsgEhMALQhWAMgjAAIgYgCQgEAJgIAHQgNANgaAJQgdAJgOAFQgrASg1A3QgfAggXAdQgfAlgQAdQgWAmgFAkQgCATAAAnQgCAcgIATIAAAJIAABZQgBA3AHAjQAGAbAOAhQAIATATAmQANAaAJAMIANARQAIAJAEAIQAQAJANATQAHAJAQAcQAcAzAgAKQAKADARAAIAcACQAVACAgAQQAlARAPAEQAXAHAvAAIBRAAQAdAAARgFQAHgCARgIIALgFQAKgIAOgDQATgGATAGIAaAJQAJACATABQCpAHCZACQAQABALgDIAQgEQALgHANgBIAEgBQAagEAWANQAbAOAHAfQAEARgGATQgGARgMAOQgXAZgpAJQgXAFgnAAQhbAAjZgIIhLgEIgYgCIgEACQgcAOgmAFQgYADgtAAQhBAAgWgBg");
	var mask_1_graphics_71 = new cjs.Graphics().p("Ah1KcQhJgEgxgYQgdgPgIgCQgKgDgPgBIgZgBQg8gGg1grQgrgjgkg8QgLgSgFgRIgHgEQgWgPgXgtIgfg7QgWgqgGgRQgEgJgHgaQgKgggEgWQgFgaABglIABhAQABgZgEgRIgGgcQgEgaAQgXIAEgGQgBgRADgVQAQiDBVhnIAxg2IAugvQAfgfAVgRQBDg3A/gCIALAAQAFgKAIgHQAVgSAxgFQCmgUBhgBQAEgKAGgJQAOgUAVgFQAPgDAYACQAoACB2AEQBjACA7AGIA3AHQAgADAWABIAiACQATACANAHQARAIAKARQALARAAATQAAASgKARQgKARgRAJQgUAKglAAQgmABgzgEIhagIQhmgJhsABIgFAKQgKARgQAJQgUAMgggBIg4gFQgsgEhMALQhWALgjAAIgYgBQgEAJgIAHQgNANgaAJQgdAIgOAGQgrARg1A3QgfAhgXAdQgfAlgQAcQgWAngFAkQgCATAAAnQgCAcgIASIAAAKIAABZQgBA2AHAjQAGAcAOAgQAIAUATAlQANAaAJAMIANARQAIAKAEAIQAQAJANASQAHAKAQAcQAcAzAgAJQAKADARABIAcABQAVADAgAPQAlASAPAEQAXAGAvAAIBSAAQAcAAARgEQAHgCARgIIALgFQAKgIAOgEQATgFATAFIAaAJQAJADATABQCpAHCZABQAQABALgCIAQgEQALgHANgCIAEAAQAagEAWAMQAbAPAHAeQAEASgGASQgGASgMAOQgXAZgpAIQgXAFgnAAQhbABjZgJIhLgDIgYgCIgEACQgcANgmAFQgYADgtABQhBAAgWgCg");
	var mask_1_graphics_72 = new cjs.Graphics().p("AlNKnQhJgFgxgYQgdgOgIgDQgKgCgPgBIgZgCQg8gFg1grQgrgjgkg8QgLgTgFgQIgHgEQgWgQgXgsIgfg7QgWgrgGgQQgEgKgHgZQgKghgEgVQgFgaABgmIABhAQABgYgEgSIgGgbQgEgaAQgYIAEgFQAAgRACgVQAQiDBVhoIAxg1IAugwQAfgfAVgRQBDg2A/gDIALAAQAFgJAIgIQAVgRAxgFQCmgUBigCQAEgJAGgKQAOgTAVgFQAPgEAYACQAoADB1ADQBjADA7AGIA3AGQAgAEAWABIAdACQAOgOARgEQAKgCAhADQATACAlAAICyAAQAoAAATgHQAHgDAWgMQATgJAMgDQATgDATAJQATAKAKARQALARABAVQACAUgHATQgIAXgPALQgKAHgRAEIgdAHIglAMQgNADgRABIgfABIjrAAQgcAAgQgDIgHAEQgUAKglAAQgmAAgzgEIhagIQhmgIhrAAIgFALQgKAQgQAJQgUAMgggBIg4gFQgsgEhNALQhWAMgjAAIgYgBQgEAJgIAGQgNANgaAJQgdAJgOAFQgrASg1A3QgfAggXAdQgfAlgQAdQgWAmgFAkQgCATAAAnQgCAcgIATIAAAJIAABZQgBA3AHAjQAGAbAOAhQAIATATAmQANAaAJAMIANARQAIAKAEAHQAQAKANASQAHAJAQAcQAcAzAgAKQAKADARAAIAcACQAVACAgAQQAlARAPAEQAXAHAvAAIBSAAQAdAAARgFQAHgCARgIIALgEQAKgJAOgDQATgGATAGIAaAJQAJACATABQCoAHCZACQAQABALgDIAQgEQALgGANgCIAEAAQAagEAWAMQAbAOAHAfQAEARgGATQgGARgMAOQgXAZgpAJQgXAFgnAAQhbAAjYgIIhLgEIgYgBIgEABQgcAOgmAFQgYADguAAQhBAAgWgBg");
	var mask_1_graphics_73 = new cjs.Graphics().p("AlNPxQhJgEgxgYQgdgPgIgCQgKgDgPgBIgZgBQg8gGg1grQgrgjgkg8QgLgTgFgQIgHgEQgWgPgXgtIgfg7QgWgqgGgRQgEgJgHgaQgKgggEgWQgFgaABglIABhAQABgZgEgSIgGgcQgEgaAQgXIAEgGQAAgRACgVQAQiDBVhmIAxg2IAugvQAfgfAVgRQBDg3A/gCIALAAQAFgKAIgHQAVgSAxgFQCmgUBigBQAEgKAGgJQAOgUAVgFQAPgDAYACQAoACB1AEQBjACA7AGIA3AHQAgADAWABIAdACQAOgOARgDQAKgCAhACQATACAlAAICyAAQAiAAATgFQgEgWAAgkIAAoSQAAgXACgNQACgUAIgNQAJgOAOgIQAOgJAQgBQAQgBAPAGQAPAHAKAMQAMAQAFAZQACAPAAAeIAAG8QgBA6AEAjIAEAqQAAALgCAKQAEAKAAALQACAUgHAUQgIAWgPALQgKAHgRAFIgdAGIglAMQgNAEgRABIgfAAIjrAAQgcAAgQgDIgHAEQgUAKglAAQgmABgzgEIhagIQhmgJhrABIgFAKQgKARgQAJQgUAMgggBIg4gFQgsgEhNALQhWALgjAAIgYgBQgEAJgIAHQgNANgaAJQgdAIgOAGQgrARg1A2QgfAhgXAdQgfAlgQAcQgWAngFAkQgCATAAAnQgCAbgIATIAAAKIAABaQgBA2AHAjQAGAcAOAgQAIAUATAlQANAaAJAMIANARQAIAKAEAIQAQAJANASQAHAKAQAcQAcAzAgAJQAKADARABIAcABQAVADAgAPQAlASAPAEQAXAGAvAAIBSAAQAdAAARgEQAHgCARgIIALgFQAKgIAOgEQATgFATAFIAaAJQAJADATABQCoAHCZABQAQABALgCIAQgFQALgGANgCIAEAAQAagEAWAMQAbAPAHAeQAEASgGASQgGASgMAOQgXAZgpAIQgXAFgnAAQhbABjYgJIhLgDIgYgCIgEACQgcANgmAFQgYADguABQhBAAgWgCg");
	var mask_1_graphics_74 = new cjs.Graphics().p("AlTVUQhIgEgxgYQgdgPgIgCQgKgDgPgBIgagBQg7gGg2grQgrgjgjg8QgLgSgGgRIgHgEQgVgPgXgtIgfg7QgWgqgGgRQgEgJgIgaQgKgggEgWQgEgaAAglIAChAQABgZgEgSIgHgcQgDgaAPgXIAEgGQAAgRACgVQAQiDBVhnIAxg2IAvgvQAfgfAVgRQBDg3A/gCIAKAAQAFgKAJgHQAVgSAxgFQCmgUBigBQADgKAHgJQAOgUAVgFQAOgDAYACQApACB0AEQBjACA7AGIA3AHQAgADAXABIAdACQANgNASgEQAKgCAgACQATACAmAAICyAAQAhAAATgFQgEgWAAgjIAAoSQAAgXACgNQADgUAIgNIACgEIgDgPQgDgPAAgeIAChxQAAgsALgTQAJgQAQgIIAEgCIgIgGQgMgMgFgQQgDgJgDgbIgGgjQgDgVAEgOQAHgWAXgOIgHgGQgNgQgDgXQgDgVAIgUQADgIAFgHIgCgDQgKgQgCgYIgBgrQAAgiABgIQACgXAJgQQAIgOAOgIQAOgJAQgCQAQgBAPAHQAQAGAKAMQAOARAEAdQADAQgBAjQgBAegCAMQgDAVgKAPQAOASAAAaQABAagPAUQgGAJgIAGIAHAGQATAUADAtQAFBHggAcQgHAGgIAEQANAJAIANQANAYgBAyIgBBoQAAAagCAPQgEAVgJAPIADAPQADAPAAAeIgBG8QgBA6AEAjIAEApQABAMgCAKQADAJABALQABAUgHAUQgHAWgQALQgKAHgRAFIgcAGIgmAMQgNAEgRABIgeAAIjsAAQgcAAgQgDIgHAEQgUAKgkAAQgmABg0gEIhZgIQhngJhqABIgGAKQgJARgQAJQgVAMgggBIg3gFQgsgEhNALQhXALgjAAIgXgBQgFAJgHAHQgOANgZAJQgeAIgOAGQgrARg0A3QgfAhgYAdQgeAlgRAcQgVAngFAkQgCATgBAnQgBAcgJASIABAKIgBBaQgBA2AIAjQAFAcAOAgQAJAUATAlQANAaAIAMIAOARQAIAKAEAIQAPAJANASQAHAKAQAcQAcAzAgAJQALADARABIAcABQAVADAgAPQAlASAOAEQAXAGAwAAIBSAAQAdAAAQgEQAHgCASgIIALgFQAKgIAOgEQATgFATAFIAaAJQAJADASABQCoAHCZABQARABALgCIAQgEQALgHAMgCIAFAAQAZgEAWAMQAcAPAHAeQADASgFASQgGASgNAOQgWAZgpAIQgXAFgnAAQhbABjZgJIhLgDIgXgCIgEACQgcANgnAFQgYADguABQhBAAgWgCg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(65).to({graphics:mask_1_graphics_65,x:302.8,y:597.4}).wait(1).to({graphics:mask_1_graphics_66,x:279.7,y:598}).wait(1).to({graphics:mask_1_graphics_67,x:250.3,y:590.9}).wait(1).to({graphics:mask_1_graphics_68,x:243.2,y:568.4}).wait(1).to({graphics:mask_1_graphics_69,x:243.2,y:545.8}).wait(1).to({graphics:mask_1_graphics_70,x:243.2,y:542.8}).wait(1).to({graphics:mask_1_graphics_71,x:244.2,y:540.5}).wait(1).to({graphics:mask_1_graphics_72,x:265.8,y:539.5}).wait(1).to({graphics:mask_1_graphics_73,x:265.8,y:506.4}).wait(1).to({graphics:mask_1_graphics_74,x:266.4,y:470.9}).wait(70));

	// filo
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#1A1A1A").ss(2.5,1,1).p("AGBT/Ip0AAQjwAAiqiqQiqiqAAjxQAAjxCqiqQCqiqDwAAIQrAAIAA1z");
	this.shape_30.setTransform(265.2,470.5);
	this.shape_30._off = true;

	var maskedShapeInstanceList = [this.shape_30];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_30).wait(65).to({_off:false},0).wait(79));

	// casa
	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#1A1A1A").ss(1.2).p("AjukPIFZIcICJAA");
	this.shape_31.setTransform(417.3,643.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#1A1A1A").ss(1.2).p("ADxAAInhAA");
	this.shape_32.setTransform(417.6,616.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#5F2CCC").s().p("AgZAZQgKgKAAgPQAAgOAKgLQALgKAOAAQAPAAAKAKQALALAAAOQAAAPgLAKQgKALgPAAQgOAAgLgLg");
	this.shape_33.setTransform(241.8,772.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("Aj6AAIH1AA");
	this.shape_34.setTransform(254.2,742.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#5F2CCC").s().p("AgfAgQgOgNAAgTQAAgSAOgNQANgOASAAQATAAAOAOQANANAAASQAAATgNANQgOAOgTAAQgSAAgNgOg");
	this.shape_35.setTransform(530,766.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AiCAAIEFAA");
	this.shape_36.setTransform(378.6,527.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AiCAAIEFAA");
	this.shape_37.setTransform(378.6,518.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAHNIAAuZ");
	this.shape_38.setTransform(378.7,552.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AgzAAIBnAA");
	this.shape_39.setTransform(468.7,606.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FC3065").s().p("AisERIAAohIAEAFIFVIcg");
	this.shape_40.setTransform(410.8,643.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("ACqAAIlTAA");
	this.shape_41.setTransform(468.7,637.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAipIAAFT");
	this.shape_42.setTransform(468.7,637.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("ACyCyIljAAIAAljIFjAAg");
	this.shape_43.setTransform(468.7,638);

	this.instance_5 = new lib.Path_3_2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(454.9,638,1,1,0,0,0,3.9,17.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#EBEBEB").s().p("AixCyIAAljIFjAAIAAFjg");
	this.shape_44.setTransform(468.7,638);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#1A1A1A").ss(1.2).p("AAAm1IkNFSIAAIcIIbAAIAAocg");
	this.shape_45.setTransform(468.7,626.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AkNG4IAAocIENlTIEOFTIAAIcg");
	this.shape_46.setTransform(468.7,626.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#EBEBEB").s().p("AjwEPIAAocIHhAAIAAIcg");
	this.shape_47.setTransform(417.6,643.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FC3065").s().p("Al3CpIAAlRILvAAIAAFRg");
	this.shape_48.setTransform(431.1,599.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAnPIAAOf");
	this.shape_49.setTransform(501.4,754.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AphAAITDAA");
	this.shape_50.setTransform(304.6,694.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AB3AAQAAAxgjAjQgjAjgxAAQgwAAgjgjQgjgjAAgxQAAgwAjgjQAjgjAwAAQAxAAAjAjQAjAjAAAwg");
	this.shape_51.setTransform(303.7,598.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#F2F2F2").s().p("AhTBUQgjgjAAgxQAAgwAjgjQAjgjAwAAQAxAAAjAjQAjAjAAAwQAAAxgjAjQgjAjgxAAQgwAAgjgjg");
	this.shape_52.setTransform(303.7,598.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#1A1A1A").ss(1.2).p("ANoBWI7PAAIAAirIbPAAg");
	this.shape_53.setTransform(551.6,809.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#999999").s().p("AtnBWIAAirIbPAAIAACrg");
	this.shape_54.setTransform(551.6,809.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FC3065").s().p("AjQHTIAAtGQAAgnAcgcQAcgcAoAAIDiAAQAnAAAcAcQAcAcAAAnIAANGg");
	this.shape_55.setTransform(542.6,771.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("ADzAAInmAA");
	this.shape_56.setTransform(459,747.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAi/IAAF/");
	this.shape_57.setTransform(459,747.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AD/DAIn9AAIAAl/IH9AAg");
	this.shape_58.setTransform(459,747.3);

	this.instance_6 = new lib.Path_3_1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(439,747.3,1,1,0,0,0,5.5,19.2);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#F2F2F2").s().p("Aj+DAIAAl/IH9AAIAAF/g");
	this.shape_59.setTransform(459,747.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#1A1A1A").ss(1.2).p("AN+HQI77AAIAAufIb7AAg");
	this.shape_60.setTransform(493.6,754.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#F2F2F2").s().p("At9HQIAAufIb7AAIAAOfg");
	this.shape_61.setTransform(493.6,754.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AnpAAIPTAA");
	this.shape_62.setTransform(341.1,780.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AD0AAInmAA");
	this.shape_63.setTransform(303.7,661.3);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAi/IAAF/");
	this.shape_64.setTransform(303.7,661.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AD/DAIn9AAIAAl/IH9AAg");
	this.shape_65.setTransform(303.7,661.3);

	this.instance_7 = new lib.Path_3_0();
	this.instance_7.parent = this;
	this.instance_7.setTransform(283.7,661.4,1,1,0,0,0,5.5,19.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#F2F2F2").s().p("Aj+DAIAAl/IH9AAIAAF/g");
	this.shape_66.setTransform(303.7,661.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("ADzAAInmAA");
	this.shape_67.setTransform(335.5,747.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AAAi/IAAF/");
	this.shape_68.setTransform(335.5,747.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#1A1A1A").ss(1.2,1,1).p("AD/DAIn9AAIAAl/IH9AAg");
	this.shape_69.setTransform(335.5,747.3);

	this.instance_8 = new lib.Path_3();
	this.instance_8.parent = this;
	this.instance_8.setTransform(315.6,747.3,1,1,0,0,0,5.6,19.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#F2F2F2").s().p("Aj+DAIAAl/IH9AAIAAF/g");
	this.shape_70.setTransform(335.5,747.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FC3065").s().p("AjIHTQgIAAAAgJIAArLQAAhXA9g9QA+g9BVAAQBWAAA+A9QA9A9AABXIAALLQAAAJgIAAg");
	this.shape_71.setTransform(253.8,771.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(1.2).p("AvsMeIPs42IPuY2g");
	this.shape_72.setTransform(303.7,628);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AvsMbIPs41IPtY1g");
	this.shape_73.setTransform(303.7,628.2);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#1A1A1A").ss(1.2).p("Am+BWIN9AAIAAirIt9AAg");
	this.shape_74.setTransform(419.7,809.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#E6E6E6").s().p("Am+BWIAAirIN9AAIAACrg");
	this.shape_75.setTransform(419.7,809.2);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(1.2).p("APuImI/aAAIAAxLIfaAAg");
	this.shape_76.setTransform(303.7,762.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AvsImIAAxLIfZAAIAARLg");
	this.shape_77.setTransform(303.7,762.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FC3065").s().p("A14MbIAA41IcDAAIPuY1g");
	this.shape_78.setTransform(443.8,628.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.instance_8},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.instance_7},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.instance_6},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.instance_5},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31}]}).wait(144));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-356.3,542.1,1683.3,794.4);
// library properties:
lib.properties = {
	id: 'BF3588748414496F81EECC4D19C9F1AD',
	width: 750,
	height: 920,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BF3588748414496F81EECC4D19C9F1AD'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;