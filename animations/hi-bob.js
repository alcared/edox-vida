(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Volo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AhZi2ICzFt");
	this.shape.setTransform(164.8,99.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AA2BBIhriB");
	this.shape_1.setTransform(179,123.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("Ah+B6IAUjuIDuAV");
	this.shape_2.setTransform(184.2,128.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3E3E").s().p("AhHBKQgegfAAgrQAAgqAegfQAegeApAAQAqAAAeAeQAeAfAAAqQAAArgeAfQgeAegqAAQgpAAgegeg");
	this.shape_3.setTransform(69,48.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AFVAAQAACRhkBmQhkBniNAAQiMAAhkhnQhkhmAAiRQAAiPBkhnQBkhmCMAAQCNAABkBmQBkBmAACQg");
	this.shape_4.setTransform(69,48.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AjwD3QhkhmAAiRQAAiPBkhnQBkhmCMgBQCNABBkBmQBkBmAACQQAACRhkBmQhkBniNAAQiMAAhkhng");
	this.shape_5.setTransform(69,48.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FC3065").s().p("AslHoQjKAAiPiPQiPiPAAjKQAAjJCPiPQCPiPDKAAMAgzAAAQAADGhMC2QhLCuiHCHQiHCHivBLQi1BMjHAAg");
	this.shape_6.setTransform(148.3,48.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("ACojGIlgDwIF2Cfg");
	this.shape_7.setTransform(18.5,59.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ai7ApIFgjwIAXGPg");
	this.shape_8.setTransform(18.8,59.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AhZi2ICzFt");
	this.shape_9.setTransform(215,99.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AA2BBIhriB");
	this.shape_10.setTransform(229.3,123.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("Ah+B6IAVjuIDtAV");
	this.shape_11.setTransform(234.4,128.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Volo, new cjs.Rectangle(-1,0,278.7,142.1), null);


(lib.sc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3F3E3E").s().p("Ag8E3IAAmuIB6AAIAAGugAgvjAQgTgTAAgeQAAgeATgUQATgTAcAAQAdAAATATQATAUAAAeQAAAegTAUQgTATgdAAQgcAAgTgUg");
	this.shape.setTransform(75.1,65.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F3E3E").s().p("AB/EYIAAjdIj9AAIAADdIh+AAIAAovIB+AAIAADqID9AAIAAjqIB+AAIAAIvg");
	this.shape_1.setTransform(31.6,68.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
    var userName = 'undefined';
    // Get username from the parent window
    if (typeof window.parent !== 'undefined') {
        if ('edoxData' in window.parent) {
            if ('userName' in window.parent.edoxData) {
                userName = window.parent.edoxData.userName;
            }
        }
    }
	this.text = new cjs.Text(userName, "normal 100 150px 'Montserrat'", "#3F3E3E");
	this.text.textAlign = "center";
	this.text.lineHeight = 150;
	this.text.lineWidth = 673;
	this.text.parent = this;
	this.text.setTransform(318.3,44.2,1,1,-2.5);
	if(!lib.properties.webfonts['Montserrat']) {
		lib.webFontTxtInst['Montserrat'] = lib.webFontTxtInst['Montserrat'] || [];
		lib.webFontTxtInst['Montserrat'].push(this.text);
	}

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = getMCSymbolPrototype(lib.sc, new cjs.Rectangle(-19.9,0,685.5,271.5), null);


(lib.Line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("Eg7cAAAMB25AAA");
	this.shape.setTransform(380.5,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Line, new cjs.Rectangle(-1,-1,763,2), null);


(lib.l1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ACXj3IksHv");
	this.shape.setTransform(28.9,24.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AgFBAIALh/");
	this.shape_1.setTransform(13.5,55.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AiAAyICLh1IB2CL");
	this.shape_2.setTransform(12.9,55.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.l1, new cjs.Rectangle(-1,-1,46,65.1), null);


(lib.DOTS = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("AhGBHQgegeAAgpQAAgoAegeQAdgeApAAQAqAAAdAeQAeAeAAAoQAAApgeAeQgdAegqAAQgpAAgdgeg");
	this.shape.setTransform(10.1,266.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F2CCC").s().p("AhGBHQgegdAAgqQAAgpAegdQAdgeApAAQAqAAAdAeQAeAdAAApQAAAqgeAdQgdAegqAAQgpAAgdgeg");
	this.shape_1.setTransform(485.4,10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.DOTS, new cjs.Rectangle(0,0,495.5,277), null);


(lib.cartell = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FC3065").ss(3).p("EA3+AR0MhtwAFyMgCLgpaMBtwgFyg");
	this.shape.setTransform(358.2,151.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AEhgVIpBAr");
	this.shape_1.setTransform(352.3,507.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("Ag4geIBxA9");
	this.shape_2.setTransform(318.1,506.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#222221").p("AAjhyIAzCvIivAz");
	this.shape_3.setTransform(315.2,503.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#EBEBEB").ss(11,1,1).p("AAA1KMAAAAqV");
	this.shape_4.setTransform(315,407.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cartell, new cjs.Rectangle(-1.5,-1.5,719.6,550.1), null);


(lib.Symbol = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FC3065").ss(5).p("AD2AAInrAA");
	this.shape.setTransform(24.6,24.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FC3065").ss(5).p("AAAj1IAAHr");
	this.shape_1.setTransform(24.7,24.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol, new cjs.Rectangle(-2.5,-2.5,54.3,54.3), null);


(lib.Alluccello = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3F3E3E").s().p("AhABBQgbgbAAgmQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQAAAmgbAbQgbAbgmAAQglAAgbgbg");
	this.shape.setTransform(70.5,46.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AE1AAQAACAhaBbQhbBaiAAAQh/AAhbhaQhahbAAiAQAAh/BahbQBbhaB/AAQCAAABbBaQBaBbAAB/g");
	this.shape_1.setTransform(70.5,46.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AjaDbQhahbAAiAQAAh/BahbQBbhaB/AAQCAAABbBaQBaBbAAB/QAACAhaBbQhbBaiAAAQh/AAhbhag");
	this.shape_2.setTransform(70.5,46.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#222221").p("ACXj3IksHv");
	this.shape_3.setTransform(86.6,188.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AgFBAIALh/");
	this.shape_4.setTransform(71.2,219.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#222221").p("AiAAyICLh1IB2CL");
	this.shape_5.setTransform(70.6,219.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FC3065").s().p("AnqNtQh4AAhkhEQhghCgthrQgehHAAhPIABgZIgBAAIAAs+QAAjUCXiVQCXiVDWAEQDPADCSCYQCPCWAADRIAAHHILvAAQAACfg+CSQg7CMhsBtQhtBsiMA7QiRA+igAAg");
	this.shape_6.setTransform(108,87.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("ACfi7IlNDiIFhCWg");
	this.shape_7.setTransform(17.5,56.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AiwAnIFMjjIAVF5g");
	this.shape_8.setTransform(17.7,56.1);

	this.instance = new lib.l1();
	this.instance.parent = this;
	this.instance.setTransform(49.8,159.7,1,1,0,0,0,43.1,3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Alluccello, new cjs.Rectangle(-1,0,197.2,227.8), null);


// stage content:
(lib._1hibob = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

    window.StopAnimation = function () {
        // console.log('executing StopAnimation()');
        this.stop();
    }.bind(this);
    window.StartAnimation = function () {
        // console.log('executing StartAnimation()');
        this.play();
    }.bind(this);

	// timeline functions:
	this.frame_121 = function() {
		this.gotoAndPlay(72
		);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(121).call(this.frame_121).wait(3));

	// terra
	this.instance = new lib.Line();
	this.instance.parent = this;
	this.instance.setTransform(-407,640.1,1,1,0,0,0,380.4,0);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).wait(1).to({regX:380.5,x:47.5},0).wait(1).to({x:148.4},0).wait(1).to({x:208},0).wait(1).to({x:248.7},0).wait(1).to({x:278.3},0).wait(1).to({x:300.7},0).wait(1).to({x:318},0).wait(1).to({x:331.5},0).wait(1).to({x:342},0).wait(1).to({x:350.1},0).wait(1).to({x:356.3},0).wait(1).to({x:361},0).wait(1).to({x:364.3},0).wait(1).to({x:366.5},0).wait(1).to({x:367.7},0).wait(1).to({regX:380.4,x:368},0).to({_off:true},90).wait(2));

	// volo
	this.instance_1 = new lib.Volo();
	this.instance_1.parent = this;
	this.instance_1.setTransform(906.3,414.9,1,1,0,0,0,138.3,70.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(8).to({_off:false},0).wait(1).to({regY:70.5,x:579.2,y:435.4},0).wait(1).to({x:507.5,y:440},0).wait(1).to({x:469.6,y:442.4},0).wait(1).to({x:446.7,y:443.9},0).wait(1).to({x:432.1,y:444.8},0).wait(1).to({x:422.6,y:445.4},0).wait(1).to({x:416.4,y:445.8},0).wait(1).to({x:412.5,y:446.1},0).wait(1).to({x:410,y:446.2},0).wait(1).to({x:408.5,y:446.3},0).wait(1).to({x:407.7,y:446.4},0).wait(1).to({x:407.3},0).wait(1).to({x:407.2},0).wait(1).to({regY:70.8,y:446.7},0).wait(1).to({regY:70.5},0).wait(1).to({x:407,y:447.9},0).wait(1).to({x:406.8,y:449.9},0).wait(1).to({x:406.5,y:452.9},0).wait(1).to({x:406,y:457.3},0).wait(1).to({x:405.3,y:463.4},0).wait(1).to({x:404.3,y:471.9},0).wait(1).to({x:402.9,y:484.1},0).wait(1).to({x:400.8,y:503.3},0).wait(1).to({regY:70.8,x:393.8,y:564.9},0).to({_off:true},1).wait(91));

	// Layer 15 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_48 = new cjs.Graphics().p("ADGYaMAAAgiTIBkAAMAAAAiTg");
	var mask_graphics_49 = new cjs.Graphics().p("AihYaMAAAgiTIMyAAMAAAAiTg");
	var mask_graphics_50 = new cjs.Graphics().p("AoIYaMAAAgiTIX/AAMAAAAiTg");
	var mask_graphics_51 = new cjs.Graphics().p("AtwYaMAAAgiTMAjOAAAMAAAAiTg");
	var mask_graphics_52 = new cjs.Graphics().p("AzYYaMAAAgiTMAucAAAMAAAAiTg");
	var mask_graphics_53 = new cjs.Graphics().p("A4/YaMAAAgiTMA5qAAAMAAAAiTg");
	var mask_graphics_54 = new cjs.Graphics().p("A+nYaMAAAgiTMBE4AAAMAAAAiTg");
	var mask_graphics_55 = new cjs.Graphics().p("EgkOAYaMAAAgiTMBQGAAAMAAAAiTg");
	var mask_graphics_56 = new cjs.Graphics().p("Egp2AYaMAAAgiTMBbUAAAMAAAAiTg");
	var mask_graphics_57 = new cjs.Graphics().p("EgvlAYaMAAAgiTMBmjAAAMAAAAiTg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(48).to({graphics:mask_graphics_48,x:29.8,y:156.2}).wait(1).to({graphics:mask_graphics_49,x:65.7,y:156.2}).wait(1).to({graphics:mask_graphics_50,x:101.5,y:156.2}).wait(1).to({graphics:mask_graphics_51,x:137.4,y:156.2}).wait(1).to({graphics:mask_graphics_52,x:173.2,y:156.2}).wait(1).to({graphics:mask_graphics_53,x:209.1,y:156.2}).wait(1).to({graphics:mask_graphics_54,x:244.9,y:156.2}).wait(1).to({graphics:mask_graphics_55,x:280.8,y:156.2}).wait(1).to({graphics:mask_graphics_56,x:316.6,y:156.2}).wait(1).to({graphics:mask_graphics_57,x:351.8,y:156.2}).wait(67));

	// sc
	this.instance_2 = new lib.sc();
	this.instance_2.parent = this;
	this.instance_2.setTransform(365.9,203.8,1,1,0,0,0,314.2,118.6);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(48).to({_off:false},0).to({_off:true},74).wait(2));

	// Layer 16 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_57 = new cjs.Graphics().p("ACrCxQgXAAgJgIQgHgGAAgLQAAgKAHgHQAIgIAZAAIAkAAQAPABAFACQANAGABAPQAAAQgMAGQgHAEgRAAgAAnglQgHgDgDgIQgEgHACgIQACgIAIgHIAPgMQAOgKAKgLIANgNQAHgGAIgBQAHgBAGADQAGADADAFQAIALgHANIgLAOIgKAPQgDAEgHAEIgKAHIgJAIQgFAFgEACQgHAFgIAAQgHAAgHgEgAjThHQgRgCgIgYQgFgRAAgZQAAgTAHgJQAIgKAMABQANACAGALQADAFABAJIABAQIAEAUQAEAQgGAMQgIAOgNAAIgCAAg");
	var mask_1_graphics_58 = new cjs.Graphics().p("AC6DZQgKgCgIgCIgPABIgjAAQgXAAgJgIQgHgHAAgKQAAgKAHgHQAIgIAZAAIAkAAIANABQAJgBAPAAIBKAGQATABAIAGQAIAHAAALQAAAMgIAGQgGAFgIABIgQABIgRABQglAAgXgEgAgNAAQgHgEgDgHQgEgIACgHQACgIAIgIIAPgLQANgKAKgMIANgMQAFgEAFgCQAEgJAIgJIAYgXIASgWQAKgMAOgCQAPgBAJAMQAJAMgHAOQgCAGgIAJIgtAuQgJAKgHAFQgGADgFABIgFAHIgKAOQgDAEgHAFIgKAHIgJAIQgFAFgEACQgHAEgIAAQgGAAgHgDgAkIgiQgRgCgIgYQgFgRAAgZIAAgEIgCgDQgEgIAAgOIAAgyQAAgXAHgIQAHgIALAAQALAAAGAIQAIAIAAAVQABAdgCAXIADAEQADAGABAJIABAQIAEAUQAEAPgGANQgIAOgNAAIgCAAg");
	var mask_1_graphics_59 = new cjs.Graphics().p("AD7EeQgOgFgIgJIAAAAIgQABQgxABgcgEQgLgCgHgCIgPABIgjAAQgXAAgJgIQgHgHAAgKQAAgKAHgHQAIgIAZAAIAkAAIANABQAJgBAPAAIBKAGQATABAIAGIAAABQAGgCAGABQAHACAPAGQANAEAVgEIAMgCQAGAAAFABQALAEAEAMQAEAJgDAKQgDAJgIAFQgHAGgTAAIgKAAQgiAAgSgGgAg/A0QgHgEgDgHQgEgIACgHQACgIAIgIIAPgKQAOgKAKgMIANgMQAEgEAGgCQADgJAIgJIAYgXIASgWQAIgKALgDIgCgHQgBgLAHgHQAEgEAKgEQALgEAEgDQAHgEAHgNQALgRAJgGQANgKAPADQAIACAFAGQAGAGAAAIQAAAHgFAIIgLANIgZAeQgOARgRAGIgJADQACAIgEAJQgCAGgIAJIgtAuQgJAKgHAFQgGADgFABIgFAGIgKAOQgDAEgGAFIgKAHIgJAIQgFAFgEACQgIAFgHAAQgHAAgHgEgAk6ASQgRgCgIgXQgFgRAAgZIAAgEIgCgDQgEgIAAgOIAAgyQAAgXAHgIIAGgFIAAgEIgFgWQgCgLAAgSIABgcIgBgSQABgKAFgGQAIgLARABQARACAFAMQADAGgCAIIgDANQgDARAFAeQAFAhgBAPQgCARgJAIIgCACQAAAcgBAWIADAEQADAGABAJIABAQIAEAUQAEAPgGAMQgIAOgNAAIgCAAg");
	var mask_1_graphics_60 = new cjs.Graphics().p("AEXFWQgMgDgEgLIgBgCIgJAAQgoABgWgHQgNgFgIgJIgBABIgPABQgxABgdgFQgKgBgHgDIgPABIgkAAQgVAAgJgIQgHgGAAgLQgBgKAHgHQAJgIAXAAIAlAAIANABQAIgBAPABIBKAFQATABAIAHIAAAAQAGgBAGABQAIABAOAHQAOADAVgDIALgCQAHgBAFACIACABIANgGQAagJAUgEQAYgFAMAEQAJADAFAIQAGAHgCAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFQgdAGgpABQgOAAgHgCgAh7BWQgGgDgEgIQgDgHABgIQACgIAIgHIAPgMQAOgKAKgLIANgMQAFgEAFgCQAEgIAIgJIAYgXIASgWQAIgLAKgCIgBgIQgBgLAGgGQAFgEAKgEQALgEAEgDQAGgEAHgOQALgQAJgGQAJgHAKgBIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgEAOgOACIgEAIIgLAOIgYAeQgPARgQAGIgKACQADAJgEAJQgDAFgIAJIgrAtQgJAKgIAFQgFADgFABIgGAHIgKAPQgDAEgGAEIgKAHIgJAIQgFAFgEACQgIAFgIAAQgHAAgHgEgAl2A0QgRgCgIgYQgFgRAAgYIAAgEIgBgDQgEgHAAgOIAAgyQAAgXAHgIIAFgFIAAgEIgFgXQgCgLABgRIAAgdIAAgSQABgIADgGQgDgGgBgIQgMgrAMgYQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANQAFAPgDAMIgBADQADADACAFQACAFgBAIIgDAOQgDAQAEAfQAGAhgCAOQgCASgIAHIgDACQABAcgBAXIADAEQADAFABAJIAAAQIAFATQADAQgGAMQgHAOgNAAIgDAAg");
	var mask_1_graphics_61 = new cjs.Graphics().p("AEXFWQgMgDgEgLIgBgCIgJAAQgoABgWgHQgNgFgIgJIgBABIgPABQgxABgdgFIgCAAQACgPgHgWIgEgPIARAAIBKAFQATABAIAHIAAAAQAGgBAGABQAIABAOAHQAOADAVgDIALgCQAHgBAFACIACABIANgGQAagJAUgEQAYgFAMAEQAJADAFAIQAGAHgCAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFQgdAGgpABQgOAAgHgCgAg0AgIghgWIAMgKQAFgEAFgCQAEgIAIgJIAYgXIASgWQAIgLAKgCIgBgIQgBgLAGgGQAFgEAKgEQALgEAEgDQAGgEAHgOQALgQAJgGQAJgHAKgBIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgEAOgOACIgEAIIgLAOIgYAeQgPARgQAGIgKACQADAJgEAJQgDAFgIAJIgrAtQgJAKgIAFQgFADgFABIgEAEIgIgFgAljgVQADgLgIgKQgIgKgLAAQgHAAgKAFIgNAGIAAgCIAAgyQAAgXAHgIIAFgFIAAgEIgFgXQgCgLABgRIAAgdIAAgSQABgIADgGQgDgGgBgIQgMgrAMgYQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANQAFAPgDAMIgBADQADADACAFQACAFgBAIIgDAOQgDAQAEAfQAGAhgCAOQgCASgIAHIgDACQABAcgBAXIADAEQADAFABAJIAAAFIgCgBg");
	var mask_1_graphics_62 = new cjs.Graphics().p("AEXFWQgMgDgEgLIgBgCIgJAAQgoABgWgHQgOgFgHgJIgBABIgPABIgEAAQgBgWgEgdIALAAQATABAIAHIAAAAQAGgBAGABQAIABAOAHQAOADAVgDIALgCQAHgBAFACIACABIANgGQAagJAUgEQAYgFAMAEQAJADAFAIQAFAHgBAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFQgdAGgpABQgOAAgHgCgAgNhAIAEgEQAIgLAKgCIgBgIQgBgLAGgGQAFgEAKgEQALgEAEgDQAGgEAHgOQALgQAJgGQAJgHAKgBIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgFAOgNACIgEAIIgLAOIgYAeQgPARgQAGIgKACQADAJgEAJQgDAFgIAJIgMANgAljgVQADgLgIgKIgBgBIABAAIADAEQADAFABAJIAAAFIgCgBgAmTh8IABAAIAFgFIAAgEIgFgXQgCgLABgRIAAgdIAAgSQAAgIAEgGQgDgGgBgIQgMgrAMgYQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANQAFAPgDAMIgBADQADADACAFQACAFgBAIIgDAOQgDAQAEAfQAFAdgBAPQgagCgeACg");
	var mask_1_graphics_63 = new cjs.Graphics().p("AEXFWQgMgDgEgLIgBgCIgJAAQghABgVgEIgBg6IATAIQAOADAVgDIALgCQAHgBAFACIACABIANgGQAagJAUgEQAYgFAMAEQAJADAFAIQAFAHgBAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFQgdAGgpABQgOAAgHgCgAljgVQADgLgIgKIgBgBIABAAIADAEQADAFABAJIAAAFIgCgBgAAehzQAJgEAEgCQAGgEAHgOQALgQAJgGQAJgHAKgBIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgFAOgNACIgEAIIgLAOIgYAeQgKALgKAGQgPgZgSgTgAmTixIAAgHIAAgdIAAgSQAAgIAEgGQgDgGgBgIQgMgrAMgYQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANQAFAPgDAMIgBADQADADACAFQACAFgBAIIgDAOQgDAPADAaQgcgEgXABg");
	var mask_1_graphics_64 = new cjs.Graphics().p("AEjFYIgGghQgEgQAAgNIAAgLIAJgEQAagJAUgEQAYgFAMAEQAJADAFAIQAFAHgBAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFQgdAGgpABIgJAAgAljgVQADgLgIgKIgBgBIABAAIADAEQADAFABAJIAAAFIgCgBgABdhtIgPgaIgNgQQAGgHAFgDQAJgHAKgBIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgFAOgNACIgEAIIgLAOIgLAOIgDgFgAmJjaIgKgBIAAgMQAAgIAEgGQgDgGgBgIQgMgrAMgYQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANQAFAPgDAMIgBADQADADACAFQACAFgBAIIgDANQgcAAgNgCg");
	var mask_1_graphics_65 = new cjs.Graphics().p("AFYE7QgHgfgLgaIAKgCQAYgFAMAEQAJADAFAIQAFAHgBAHQAFACAFADQAGAEADAIQAFAPgLAMQgJAKgYAFIgVADIgFgbgAljgTQADgLgIgKIgBgBIABAAIADAEQADAFABAJIAAAFIgCgBgABfinIABgKQADgMADgGQAEgJALgLQAQgQAOAAQAIABAGAEQAHAFADAGQAFARgLALIgKAHQgGAFgCADQgCADgCAGIgDAJQgEAMgKADQgOgOgRgOgAmGkXIgTgFQgCgYAIgQQAGgLAJgEQAGgDAIABQAHABAFAFQALALgEAPIgEAOQAAAEADANIACAHQgWgEgOgEg");
	var mask_1_graphics_66 = new cjs.Graphics().p("AACALQADgLgHgJIgBgBIABgBIACAEQADAGABAIIAAAFIgCgBg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(57).to({graphics:mask_1_graphics_57,x:478,y:408.8}).wait(1).to({graphics:mask_1_graphics_58,x:483.3,y:405.1}).wait(1).to({graphics:mask_1_graphics_59,x:488.3,y:399.8}).wait(1).to({graphics:mask_1_graphics_60,x:494.2,y:396.4}).wait(1).to({graphics:mask_1_graphics_61,x:494.2,y:396.4}).wait(1).to({graphics:mask_1_graphics_62,x:494.2,y:396.4}).wait(1).to({graphics:mask_1_graphics_63,x:494.2,y:396.4}).wait(1).to({graphics:mask_1_graphics_64,x:494.2,y:396.4}).wait(1).to({graphics:mask_1_graphics_65,x:494.2,y:396.2}).wait(1).to({graphics:mask_1_graphics_66,x:458.4,y:393.1}).wait(58));

	// sparks
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AAACiIAAlD");
	this.shape.setTransform(455.8,382.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1,1,1).p("AihAAIFDAA");
	this.shape_1.setTransform(510,425.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(1,1,1).p("Ah1CEIDskH");
	this.shape_2.setTransform(494.3,390.3);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},57).to({state:[]},65).wait(2));

	// Layer 32
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C4C4C4").s().p("AAHBTQgwgEgigUQgYgOgogqQgTgWgEgLQgEgNAEgOQAEgOAMgHQAUgMAjAOQAVAJAgAXQAlAaAOAIQA1AcA+gDQASAAAGADQANAGAAARQAAARgMAKQgQAOgpABQgcACgWAAQgWAAgRgCg");
	this.shape_3.setTransform(283.3,631.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C4C4C4").s().p("Al1DaIgkgCQgVgBgIgFQgFgDgGgHIgJgLQgOgRgmgRQgngTgOgOIgNgQQgIgLgHgEIgNgJQgJgEgEgEQgKgKgEgbQgHguADgbQADgpAWgYQAHgHAKgHQACgZAHgQQAPghAkgRQAjgRAnAEQAiADAkASQAcANAlAaIA/AwIB2BDQAbAPAMADIAWAEQANACAHADQAKAEAMAKIAIAGQAfAKAWAUQARAEATAEIA/AJQAPACAJAEQAmgZA3gBQAtAABDASQA0AOBdAeIAtAIQA6AJAXAKQAIAEAFAGQAFAHgCAHQgDAGgHACIgOADQgIABgNAEIgUAGQglAIg0gMQj9Aaj5gBIgiAHIg6ANQhBANhTABIg6gBIgDABIgWABQgRAAgNgDg");
	this.shape_4.setTransform(282.2,624.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C4C4C4").s().p("AjZDuQg8gIgtgRIgWgIQgPgQgigQQgSgIgNgHIgMgKQgMgKgJgKIgDgEIgJgKQgTgagEgaQgEgcAPgcQANgaAZgSQAmgcBJgKQBVgMA7AVQA3AUA+A7IAuAtQAbAZAYANQBJAkB8geIArgJQAYgEATABQAbACAsASQAzAWATAEQAPAEAcACQAeADANADQArAJATAYQj9Aaj6gBIghAHIg7ANQhAAMhSABIg7AAIgDAAIgHABIgFgBgApSBiQgDgDAAgJQAAgIADgFQACgHAHgBIADgBQAGgDAFAHQAEAFAAAHIgBAKQgBAIgEACIgGACIgDAAQgJAAgDgEgACHguQgIAAgFgEIgCgFIABgEIAHgNQAAgBABAAQABgBAAAAQABgBAAAAQAAAAABAAIACABIAAgCQAGAAADACQAEACABAFIABAGIAAAFIgEAHQgDADgEAAIgDAAgAnah0IAEABIgFADIABgEgAnVirQgFgCgJgJQgHgIgCgFQgCgEABgIQACgLAJgLQAFgGAGgCQADgBAJAAQAKAAACABQAHABAHAHQAOAPAAAQQAAAKgFAHQgFAIgIADQgEABgKAAQgNAAgFgCg");
	this.shape_5.setTransform(288.6,622.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C4C4C4").s().p("AB1DTIgDAAIgEABQhXgMhUhlQgdgjgDgbQgDgcATgbQAegqA+gNQApgIBFAIQAaACAPAGQAWAGALAPQAQAWgFAoQgIA5ABALQAHA+BWAvIAMAGQgzAIg8ACQgKgCgOABIgOACIgqgBgAAPikIgGgEIgEgFQgFgKAFgIIAEgGIAEgDQABAAABAAQABAAAAAAQABAAAAgBQAAAAAAAAQAIgBAEAFQAEAFgCAGIgDAHIgCAIQgBAEgDACQAAAAgBABQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAgAkriqIgEgGQgGgLACgMQAAgFACgDQADgDAEABIAAgCQAJAAAEACQAIADACAJQACAGgBAMQgDAHgCACQgDADgGgBQgIAAgDgCg");
	this.shape_6.setTransform(274.6,625);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_3}]},33).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},2).to({state:[]},1).wait(86));

	// Layer 28
	this.instance_3 = new lib.Alluccello();
	this.instance_3.parent = this;
	this.instance_3.setTransform(395.4,531.8,1,1,0,0,0,97.6,113.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(33).to({_off:false},0).wait(1).to({regY:113.5,x:445.3,y:531.7},0).wait(1).to({x:454.4},0).wait(1).to({x:458.1},0).wait(1).to({regY:113.6,x:459.9,y:531.8},0).to({_off:true},1).wait(86));

	// eye
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3F3E3E").s().p("AhABBQgbgbAAgmQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQAAAmgbAbQgbAbgmAAQglAAgbgbg");
	this.shape_7.setTransform(432.5,465.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#222221").p("AE1AAQAACAhaBbQhbBaiAAAQh/AAhbhaQhahbAAiAQAAh/BahbQBbhaB/AAQCAAABbBaQBaBbAAB/g");
	this.shape_8.setTransform(432.5,465.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AjaDbQhahbAAiAQAAh/BahbQBbhaB/AAQCAAABbBaQBaBbAAB/QAACAhaBbQhbBaiAAAQh/AAhbhag");
	this.shape_9.setTransform(432.5,465.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},38).to({state:[]},24).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},3).to({state:[]},26).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},3).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},1).to({state:[]},27).wait(2));

	// l2
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("ACXj3IksHv");
	this.shape_10.setTransform(448.7,607.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("AgFBAIALh/");
	this.shape_11.setTransform(433.2,638);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#222221").p("AiAAyICLh1IB2CL");
	this.shape_12.setTransform(432.7,638.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#222221").p("AA4j5IhvHz");
	this.shape_13.setTransform(448.2,607);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#222221").p("AgwjzIBhHn");
	this.shape_14.setTransform(448.5,607.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_12,p:{x:432.7,rotation:0}},{t:this.shape_11,p:{x:433.2,rotation:0,y:638}},{t:this.shape_10}]},38).to({state:[{t:this.shape_12,p:{x:432.7,rotation:0}},{t:this.shape_11,p:{x:433.2,rotation:0,y:638}},{t:this.shape_10}]},7).to({state:[{t:this.shape_12,p:{x:441.7,rotation:0}},{t:this.shape_11,p:{x:442.2,rotation:0,y:638}},{t:this.shape_13}]},1).to({state:[{t:this.shape_12,p:{x:453.4,rotation:-11.2}},{t:this.shape_11,p:{x:453.8,rotation:-11.2,y:637.9}},{t:this.shape_14}]},1).to({state:[]},75).wait(2));

	// body
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FC3065").s().p("AnqNtQh4AAhkhEQhghCgthrQgehHAAhPIABgZIgBAAIAAs+QAAjUCXiVQCXiVDWAEQDPADCSCYQCPCWAADRIAAHHILvAAQAACfg+CSQg7CMhsBtQhtBsiMA7QiRA+igAAg");
	this.shape_15.setTransform(470.1,506.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#222221").p("ACfi7IlNDiIFhCWg");
	this.shape_16.setTransform(379.5,474.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AiwAnIFMjjIAVF5g");
	this.shape_17.setTransform(379.8,474.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]},38).to({state:[]},84).wait(2));

	// l 1
	this.instance_4 = new lib.l1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(411.9,578.5,1,1,0,0,0,43.1,3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(38).to({_off:false},0).wait(5).to({_off:true},4).wait(77));

	// cartell
	this.instance_5 = new lib.cartell();
	this.instance_5.parent = this;
	this.instance_5.setTransform(384.8,573.5,0.422,0.422,-47,0,0,380.4,503.6);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(47).to({_off:false},0).wait(1).to({regX:358.2,regY:273.5,scaleX:0.95,scaleY:0.95,rotation:-4,x:360.1,y:333.5},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.3,x:373.7,y:319.5},0).wait(1).to({regX:380.6,regY:503.5,scaleX:1,scaleY:1,rotation:0,x:397.4,y:548.4},0).to({_off:true},72).wait(2));

	// dot
	this.instance_6 = new lib.DOTS();
	this.instance_6.parent = this;
	this.instance_6.setTransform(394.2,517.2,0.662,0.662,-48.7,0,0,247.7,138.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(50).to({_off:false},0).wait(1).to({regX:247.8,scaleX:0.89,scaleY:0.89,rotation:-16.3,y:517.1,alpha:0.665},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-9.1,alpha:0.813},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-5.4,y:517.2,alpha:0.889},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-3.2,y:517.1,alpha:0.934},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.9,alpha:0.962},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1,x:394.3,y:517.2,alpha:0.979},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.5,x:394.2,alpha:0.989},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,alpha:0.995},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1,x:394.3,alpha:0.998},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:394.2,y:517.1,alpha:0.999},0).wait(1).to({y:517.2,alpha:1},0).wait(22).to({y:509.7},19).to({y:517.2},19).to({_off:true},1).wait(2));

	// + copy
	this.instance_7 = new lib.Symbol();
	this.instance_7.parent = this;
	this.instance_7.setTransform(154.4,437.1,1,1,141.3,0,0,24.6,24.6);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(58).to({_off:false},0).wait(1).to({rotation:31.4,y:431.2,alpha:0.778},0).wait(1).to({rotation:16.6,y:430.5,alpha:0.882},0).wait(1).to({rotation:9.6,y:430.1,alpha:0.932},0).wait(1).to({rotation:5.7,y:429.9,alpha:0.959},0).wait(1).to({rotation:3.4,y:429.8,alpha:0.976},0).wait(1).to({rotation:2,y:429.7,alpha:0.986},0).wait(1).to({rotation:1.1,y:429.6,alpha:0.992},0).wait(1).to({rotation:0.6,alpha:0.996},0).wait(1).to({rotation:0.3,alpha:0.998},0).wait(1).to({rotation:0.1,alpha:0.999},0).wait(1).to({rotation:0,alpha:1},0).wait(3).to({y:428.7},0).wait(1).to({y:427.9},0).wait(1).to({y:427.1},0).wait(1).to({y:426.4},0).wait(1).to({y:425.7},0).wait(1).to({y:425.1},0).wait(1).to({y:424.5},0).wait(1).to({y:423.9},0).wait(1).to({y:423.4},0).wait(1).to({y:422.9},0).wait(1).to({y:422.4},0).wait(1).to({y:423.1},0).wait(1).to({y:423.9},0).wait(1).to({y:424.5},0).wait(1).to({y:425.2},0).wait(1).to({y:425.8},0).wait(1).to({y:426.4},0).wait(1).to({y:427},0).wait(1).to({y:427.6},0).wait(1).to({y:428.1},0).wait(1).to({y:428.6},0).wait(1).to({y:429.1},0).wait(1).to({y:429.6},0).wait(1).to({y:428.8},0).wait(1).to({y:428.1},0).wait(1).to({y:427.5},0).wait(1).to({y:426.8},0).wait(1).to({y:426.2},0).wait(1).to({y:425.6},0).wait(1).to({y:425.1},0).wait(1).to({y:424.6},0).wait(1).to({y:424.1},0).wait(1).to({y:423.6},0).wait(1).to({y:423.2},0).wait(1).to({y:422.8},0).wait(1).to({y:422.4},0).wait(1).to({y:423.1},0).wait(1).to({y:423.7},0).wait(1).to({y:424.3},0).wait(1).to({y:424.9},0).wait(1).to({y:425.5},0).wait(1).to({y:426},0).wait(1).to({y:426.5},0).wait(1).to({y:427},0).wait(1).to({y:427.5},0).wait(1).to({y:428},0).wait(1).to({y:428.4},0).wait(1).to({y:428.8},0).wait(1).to({y:429.2},0).wait(1).to({y:429.6},0).to({_off:true},1).wait(2));

	// +
	this.instance_8 = new lib.Symbol();
	this.instance_8.parent = this;
	this.instance_8.setTransform(631.8,655.1,1,1,141.3,0,0,24.6,24.6);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(50).to({_off:false},0).wait(1).to({rotation:42.7,y:660.3,alpha:0.698},0).wait(1).to({rotation:26.6,y:661.2,alpha:0.812},0).wait(1).to({rotation:18.1,y:661.7,alpha:0.872},0).wait(1).to({rotation:12.7,y:661.9,alpha:0.91},0).wait(1).to({rotation:9.2,y:662.1,alpha:0.935},0).wait(1).to({rotation:6.6,y:662.3,alpha:0.953},0).wait(1).to({rotation:4.8,alpha:0.966},0).wait(1).to({rotation:3.5,y:662.4,alpha:0.975},0).wait(1).to({rotation:2.5,y:662.5,alpha:0.982},0).wait(1).to({rotation:1.8,alpha:0.988},0).wait(1).to({rotation:1.2,alpha:0.991},0).wait(1).to({rotation:0.8,y:662.6,alpha:0.994},0).wait(1).to({rotation:0.5,alpha:0.996},0).wait(1).to({rotation:0.3,alpha:0.998},0).wait(1).to({rotation:0.2,alpha:0.999},0).wait(1).to({rotation:0.1},0).wait(1).to({alpha:1},0).wait(1).to({rotation:0},0).wait(4).to({y:663.5},0).wait(1).to({y:664.2},0).wait(1).to({y:664.9},0).wait(1).to({y:665.6},0).wait(1).to({y:666.2},0).wait(1).to({y:666.8},0).wait(1).to({y:667.3},0).wait(1).to({y:667.8},0).wait(1).to({y:668.2},0).wait(1).to({y:668.7},0).wait(1).to({y:669.1},0).wait(1).to({y:669.4},0).wait(1).to({y:669.8},0).wait(1).to({y:669.1},0).wait(1).to({y:668.4},0).wait(1).to({y:667.8},0).wait(1).to({y:667.2},0).wait(1).to({y:666.6},0).wait(1).to({y:666},0).wait(1).to({y:665.5},0).wait(1).to({y:664.9},0).wait(1).to({y:664.4},0).wait(1).to({y:663.9},0).wait(1).to({y:663.5},0).wait(1).to({y:663},0).wait(1).to({y:662.6},0).wait(1).to({y:663.4},0).wait(1).to({y:664.2},0).wait(1).to({y:664.9},0).wait(1).to({y:665.5},0).wait(1).to({y:666.2},0).wait(1).to({y:666.8},0).wait(1).to({y:667.4},0).wait(1).to({y:667.9},0).wait(1).to({y:668.4},0).wait(1).to({y:668.9},0).wait(1).to({y:669.4},0).wait(1).to({y:669.8},0).wait(1).to({y:669.1},0).wait(1).to({y:668.3},0).wait(1).to({y:667.7},0).wait(1).to({y:667},0).wait(1).to({y:666.4},0).wait(1).to({y:665.8},0).wait(1).to({y:665.2},0).wait(1).to({y:664.6},0).wait(1).to({y:664.1},0).wait(1).to({y:663.6},0).wait(1).to({y:663.1},0).wait(1).to({y:662.6},0).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	id: '703C36E064D54B04B0D2D979E362918E',
	width: 750,
	height: 750,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['703C36E064D54B04B0D2D979E362918E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;