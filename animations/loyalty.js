(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.z2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AD/hYIn+Cx");
	this.shape.setTransform(39.8,8.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("Ag6gOIB1Ad");
	this.shape_1.setTransform(8.8,16.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AgQh1IBXCRIiRBY");
	this.shape_2.setTransform(7.5,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.z2, new cjs.Rectangle(-1,-1,67.4,28.4), null);


(lib.z = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("Aj+haIH9C1");
	this.shape.setTransform(25.5,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AA7gNIh1Ab");
	this.shape_1.setTransform(56.5,16.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AASh2IhYCRICRBa");
	this.shape_2.setTransform(57.7,15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.z, new cjs.Rectangle(-1,-1,67.2,28.9), null);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkPEQQhxhwAAigQAAieBxhxQBxhxCeAAQCgAABwBxQBxBxAACeQAACghxBwQhwBxigAAQieAAhxhxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.4,-38.4,77,77);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkPEQQhxhwAAigQAAieBxhxQBxhxCeAAQCgAABwBxQBxBxAACeQAACghxBwQhwBxigAAQieAAhxhxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.4,-38.4,77,77);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AVUAAQAAEWhrD9QhnD0i9C+Qi9C8j1BoQj9BrkWAAQkVAAj9hrQj1hoi8i8Qi9i+hoj0Qhrj9AAkWQAAkVBrj9QBoj1C9i9QC9i9D0hnQD9hrEVAAQEWAAD9BrQD1BnC9C9QC9C9BnD1QBrD9AAEVg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F5F5F5").s().p("AoSTpQj1hoi8i8Qi+i9hnj1Qhrj+AAkVQAAkVBrj9QBnj1C+i9QC8i9D1hnQD+hrEUAAQEWAAD9BrQD1BnC8C9QC9C9BoD1QBrD9AAEVQAAEVhrD+QhoD1i9C9Qi8C8j1BoQj9BrkWAAQkUAAj+hrg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-137.4,-137.4,274.8,274.8);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AVUAAQAAEWhrD9QhnD0i9C+Qi9C8j1BoQj9BrkWAAQkVAAj9hrQj1hoi8i8Qi9i+hoj0Qhrj9AAkWQAAkVBrj9QBoj1C9i9QC9i9D0hnQD9hrEVAAQEWAAD9BrQD1BnC9C9QC9C9BnD1QBrD9AAEVg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F5F5F5").s().p("AoSTpQj1hoi8i8Qi+i9hnj1Qhrj+AAkVQAAkVBrj9QBnj1C+i9QC8i9D1hnQD+hrEUAAQEWAAD9BrQD1BnC8C9QC9C9BoD1QBrD9AAEVQAAEVhrD+QhoD1i9C9Qi8C8j1BoQj9BrkWAAQkUAAj+hrg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-137.4,-137.4,274.8,274.8);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ATnAAQAAEAhjDpQhfDhitCuQivCtjhBgQjpBij/AAQj/AAjphiQjhhgiuitQitiuhgjhQhijpAAkAQAAj+BijpQBgjiCtiuQCuitDhhfQDphjD/AAQD/AADpBjQDhBfCvCtQCtCuBfDiQBjDpAAD+g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AnoSEQjghfiuitQiuiuhgjiQhijpAAj/QAAj/BijoQBgjiCuiuQCuitDghgQDphiD/AAQEAAADpBiQDgBgCuCtQCuCuBfDiQBjDoAAD/QAAD/hjDpQhfDiiuCuQiuCtjgBfQjpBjkAAAQj/AAjphjg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-126.5,-126.5,253,253);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ATnAAQAAEAhjDpQhfDhitCuQivCtjhBgQjpBij/AAQj/AAjphiQjhhgiuitQitiuhgjhQhijpAAkAQAAj+BijpQBgjiCtiuQCuitDhhfQDphjD/AAQD/AADpBjQDhBfCvCtQCtCuBfDiQBjDpAAD+g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AnoSEQjghfiuitQiuiuhgjiQhijpAAj/QAAj/BijoQBgjiCuiuQCuitDghgQDphiD/AAQEAAADpBiQDgBgCuCtQCuCuBfDiQBjDoAAD/QAAD/hjDpQhfDiiuCuQiuCtjgBfQjpBjkAAAQj/AAjphjg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-126.5,-126.5,253,253);


(lib.stella = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("AAABnIh9BbIAwiUIh/hbICdAAIAviUIAwCUICdAAIh/BbIAwCUg");
	this.shape.setTransform(20.5,19.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.stella, new cjs.Rectangle(0,0,40.9,38.9), null);


(lib.piu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FC3065").ss(5).p("AD2AAInrAA");
	this.shape.setTransform(24.6,24.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FC3065").ss(5).p("AAAj1IAAHr");
	this.shape_1.setTransform(24.7,24.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.piu, new cjs.Rectangle(-2.5,-2.5,54.3,54.3), null);


(lib.Pallinirosa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape.setTransform(7.6,7.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Pallinirosa, new cjs.Rectangle(0,0,15.3,15.3), null);


(lib.Pbianchi = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ABlAAQAAAqgeAdQgdAegqAAQgpAAgdgeQgegdAAgqQAAgoAegeQAdgeApAAQAqAAAdAeQAeAeAAAog");
	this.shape.setTransform(473.4,10.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhGBHQgegdAAgqQAAgoAegeQAdgeApAAQAqAAAdAeQAeAeAAAoQAAAqgeAdQgdAegqAAQgpAAgdgeg");
	this.shape_1.setTransform(473.4,10.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("ACEAAQAAA3gmAnQgnAmg3AAQg1AAgngmQgngnAAg3QAAg2AngnQAngmA1AAQA3AAAnAmQAmAnAAA2g");
	this.shape_2.setTransform(13.2,233.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhcBdQgngmAAg3QAAg2AngnQAmgmA2AAQA3AAAmAmQAnAnAAA2QAAA3gnAmQgmAng3AAQg2AAgmgng");
	this.shape_3.setTransform(13.2,233.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Pbianchi, new cjs.Rectangle(-1,-1,485.5,248.5), null);


(lib.cuore2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ah4BkQh6iDAAgxQAAg6AqgpQApgqA6AAQA8AAApArQAqgrA8AAQA6AAApAqQAqApAAA6QAAAxh5CDQg9BCg9A4Qg7g4g9hCg");
	this.shape.setTransform(24.3,22.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cuore2, new cjs.Rectangle(0,0,48.6,44.4), null);


(lib.coppa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ABJAAQAAAegVAVQgWAWgeAAQgdAAgVgWQgWgVAAgeQAAgdAWgVQAVgWAdAAQAeAAAWAWQAVAVAAAdg");
	this.shape.setTransform(71.7,109.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("Am7gDIN4AH");
	this.shape_1.setTransform(71.7,0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AD7A8In1AAQAAgyAkgiQAjgjAxAAIEFAAQAyAAAjAjQAjAiAAAyg");
	this.shape_2.setTransform(71.7,164.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Aj6A8QAAgyAkgiQAjgjAxAAIEFAAQAyAAAjAjQAjAiAAAyg");
	this.shape_3.setTransform(71.7,164.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AAAikIAAFJ");
	this.shape_4.setTransform(71.7,143.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#222221").p("AAAJVQi3AAiCiCQiCiCAAi4IAArtIN4AAIAALtQAAC4iCCCQiDCCi4AAg");
	this.shape_5.setTransform(71.7,67.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E6E6E6").s().p("Ak6HTQiCiCABi4IAArtIN3AAIAALtQAAC4iBCCQiDCCi4AAQi3AAiDiCg");
	this.shape_6.setTransform(71.7,67.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("AC+AAQAABPg4A3Qg3A4hPAAQhNAAg4g4Qg3g3AAhPQAAhNA3g4QA4g3BNAAQBPAAA3A3QA4A4AABNg");
	this.shape_7.setTransform(116.1,53.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#222221").p("AC9AAQAABPg3A3Qg3A4hPAAQhNAAg4g4Qg3g3AAhPQAAhNA3g4QA4g3BNAAQBPAAA3A3QA3A4AABNg");
	this.shape_8.setTransform(27.6,53.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AEUAAQAAByhRBRQhRBRhyAAQhxAAhRhRQhRhRAAhyQAAhyBRhQQBRhRBxAAQByAABRBRQBRBQAAByg");
	this.shape_9.setTransform(27.6,53.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AEUAAQAAByhRBRQhRBRhyAAQhyAAhQhRQhRhRAAhyQAAhyBRhQQBQhRByAAQByAABRBRQBRBQAAByg");
	this.shape_10.setTransform(116.1,53.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.coppa, new cjs.Rectangle(-1,-1,145.7,172.6), null);


(lib.cerchio1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AFqAAQAACWhqBqQhqBqiWAAQiVAAhqhqQhqhqAAiWQAAiVBqhqQBqhqCVAAQCWAABqBqQBqBqAACVg");
	this.shape.setTransform(36.2,36.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Aj/EAQhqhqAAiWQAAiVBqhqQBqhqCVAAQCWAABqBqQBqBqAACVQAACWhqBqQhqBqiWAAQiVAAhqhqg");
	this.shape_1.setTransform(36.2,36.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cerchio1, new cjs.Rectangle(-1,-1,74.4,74.4), null);


(lib.alloro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("ABEAAQAAAcgUAUQgUAUgcAAQgbAAgUgUQgUgUAAgcQAAgbAUgUQAUgUAbAAQAcAAAUAUQAUAUAAAbg");
	this.shape.setTransform(98.5,136.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E6E6E6").s().p("AgvAwQgUgUAAgcQAAgbAUgUQAUgUAbAAQAcAAAUAUQAUAUAAAbQAAAcgUAUQgUAUgcAAQgbAAgUgUg");
	this.shape_1.setTransform(98.5,136.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AIarfQA0CgAACoQAAF+j1EkQjxEhlvBHIjXAAQguAAggAgQghAhAAAt");
	this.shape_2.setTransform(137.8,73.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#222221").p("AoZrfQg0CeAACqQAAF+D1EkQDxEhFvBHIDXAAQAuAAAgAgQAhAgAAAu");
	this.shape_3.setTransform(59,73.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.alloro, new cjs.Rectangle(-1,-1,198.8,149.3), null);


(lib.uccelliniall = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ah0E+QgrgHgjgYQg8gogoheQgwh2AIh9QAEg6AUggQAMgVAVgMQAVgNAXACQAOgvAugcQAugcAxAJQALACAiALQAcAJAQABQARAAAjgGQAsgCA2AZQBLAkApA6QAWAgAJAlQAJAmgGAkQgFAYACAJQABAHAFAJIAIAQQAKATACAeQACARAAAjIAAAfQgCASgGAMQgIAPgQAIQgPAHgRgCQACARgSALQgKAGgWADQgWADgJACQgRAEgLAHIgPAMIgOAMQgRANgcADIgxAEIhPAIQgVACgSAAQgXAAgRgDg");
	this.shape.setTransform(119.2,43.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E6E6E6").s().p("ACFFlQgOgHgKgQQgKgOgDgSIgBgHQgNANgRAJQgcAPggABQg5ABg4gpQgMgKgHgIQgkgIgfgVQg8gogoheQgwh1AIh+QAEg6AUggQAMgVAVgMQAVgNAXACQAOgvAugcQAugcAxAJQALACAiALQAcAJAQABQARAAAjgGQAsgCA2AZQBLAkApA6QAWAgAJAlQAJAmgGAkQgFAYACAJQABAHAFAKIAIAQQAKASACAeQACARAAAjIAAAfQgCASgGAMQgIAPgQAIQgPAHgRgCQACARgSALQgHAEgOADQAKAVACAQQACAQgHAQQgGAPgMALIgKAHIgIAIQgDAEgDAIIgGANQgJAOgSAEIgLABQgMAAgLgFg");
	this.shape_1.setTransform(320.2,44.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},54).to({state:[]},3).to({state:[{t:this.shape_1}]},25).to({state:[]},3).to({state:[{t:this.shape_1}]},51).to({state:[]},3).wait(14));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape_2.setTransform(59.9,111.5);

	this.instance = new lib.z2();
	this.instance.parent = this;
	this.instance.setTransform(290.7,136.8,1,1,0,0,0,62.1,3.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3E3E").s().p("Ag8A9QgZgaAAgjQAAgjAZgZQAagZAiAAQAjAAAaAZQAZAZAAAjQAAAjgZAaQgaAZgjAAQgiAAgagZg");
	this.shape_3.setTransform(319.3,43.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AEhAAQAAB4hVBUQhVBVh3AAQh3AAhUhVQhVhUAAh4QAAh2BVhVQBUhVB3AAQB3AABVBVQBVBVAAB2g");
	this.shape_4.setTransform(319.3,43.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AjLDMQhVhUAAh4QAAh2BVhVQBUhVB3AAQB3AABVBVQBVBVAAB2QAAB4hVBUQhVBVh3AAQh3AAhUhVg");
	this.shape_5.setTransform(319.3,43.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#222221").p("AM4BYIq9AAIAAmoQAAjEiHiMQiHiOjDgDQjGgEiNCMQiNCLAADGIAAMGIAAAAIAAAXQAABKAcBDQAqBkBaA+QBdA/BwAAIImAAQCVAACIg6QCDg3BlhlQBlhlA3iEQA6iHAAiVg");
	this.shape_6.setTransform(353.7,81.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E6E6E6").s().p("AnJMzQhwAAhdg/Qhag+gqhkQgchDAAhKIAAgXIAAAAIAAsGQAAjGCNiLQCNiMDGAEQDDADCHCOQCGCMAADEIAAGoIK+AAQgBCVg5CHQg3CEhmBlQhkBliEA3QiIA6iUAAg");
	this.shape_7.setTransform(353.7,81.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#222221").p("AAGkNIgLIc");
	this.shape_8.setTransform(334.5,173.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AAZA3Igxht");
	this.shape_9.setTransform(336.4,205.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AhrBTIA7igICfA7");
	this.shape_10.setTransform(338.7,207.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("ACUivIk2DUIFKCMg");
	this.shape_11.setTransform(269.8,52.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AikAkIE2jTIATFfg");
	this.shape_12.setTransform(270.1,52.4);

	this.instance_1 = new lib.z();
	this.instance_1.parent = this;
	this.instance_1.setTransform(137.8,136.8,1,1,0,0,0,3.1,3.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3F3E3E").s().p("Ag8A9QgZgaAAgjQAAgjAZgZQAZgZAjAAQAjAAAaAZQAZAZAAAjQAAAjgZAaQgaAZgjAAQgjAAgZgZg");
	this.shape_13.setTransform(116.7,43.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#222221").p("AkgAAQAAB4BUBUQBVBVB3AAQB3AABVhVQBVhUAAh4QAAh2hVhVQhVhVh3AAQh3AAhVBVQhUBVAAB2g");
	this.shape_14.setTransform(116.7,43.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AjMDMQhUhUAAh4QAAh2BUhVQBVhVB3AAQB3AABWBVQBUBVAAB2QAAB4hUBUQhWBVh3AAQh3AAhVhVg");
	this.shape_15.setTransform(116.7,43.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FC3065").s().p("AhcMzQkuAAjXjWQhkhmg4iDQg6iHAAiVIK9AAIAAmoQAAjECGiMQCJiODBgDQDHgECOCMQCMCLAADGIAAMGIgBAAIABAXQAABKgcBDQgpBkhbA+QhcA/hxAAg");
	this.shape_16.setTransform(82.4,81.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#222221").p("AgFkNIAMIc");
	this.shape_17.setTransform(101.5,173.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#222221").p("AgZA3IAzht");
	this.shape_18.setTransform(99.7,205.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#222221").p("ABsBTIg6igIigA7");
	this.shape_19.setTransform(97.3,207.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#222221").p("AiTivIE2DUIlKCMg");
	this.shape_20.setTransform(166.2,52.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AiRivIE2DTIlJCMg");
	this.shape_21.setTransform(165.9,52.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance_1},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.instance},{t:this.shape_2}]}).wait(153));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1,437,218.1);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Pallinirosa();
	this.instance.parent = this;
	this.instance.setTransform(237.1,-10.5,1,1,0,0,0,7.6,7.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape.setTransform(-151.5,37.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape_1.setTransform(-237.1,-37.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-244.7,-45.2,489.5,90.5);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Pallinirosa();
	this.instance.parent = this;
	this.instance.setTransform(237.1,-10.5,1,1,0,0,0,7.6,7.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape.setTransform(-151.5,37.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("Ag1A2QgWgWAAggQAAgfAWgWQAWgWAfAAQAgAAAWAWQAWAWAAAfQAAAggWAWQgWAWggAAQgfAAgWgWg");
	this.shape_1.setTransform(-237.1,-37.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-244.7,-45.2,489.5,90.5);


(lib.pb = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween8("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(38.5,38.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.pb, new cjs.Rectangle(0,0,77,77), null);


(lib.cuore = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cuore2();
	this.instance.parent = this;
	this.instance.setTransform(36.2,43.3,1,1,0,0,0,24.3,22.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.cuore, new cjs.Rectangle(11.9,21.1,48.6,44.4), null);


(lib.Symbol = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.piu();
	this.instance.parent = this;
	this.instance.setTransform(24.6,24.6,1,1,0,0,0,24.6,24.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:30.6},14).to({y:24.6},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.5,-2.5,54.3,54.3);


(lib.Uccellini = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.uccelliniall();
	this.instance.parent = this;
	this.instance.setTransform(251.6,108.1,1,1,0,0,0,218.3,108.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(30));

	// Layer 4
	this.instance_1 = new lib.pb();
	this.instance_1.parent = this;
	this.instance_1.setTransform(248.4,149.6,1,1,0,0,0,38.5,38.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:155},13).to({y:149.6},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(33.3,-0.5,436.5,217.1);


// stage content:
(lib._2loyalty = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

    window.StopAnimation = function () {
        // console.log('executing StopAnimation()');
        this.stop();
    }.bind(this);
    window.StartAnimation = function () {
        // console.log('executing StartAnimation()');
        this.play();
    }.bind(this);

	// timeline functions:
	this.frame_112 = function() {
		this.gotoAndPlay(60
		);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(112).call(this.frame_112).wait(2));

	// + copy 2
	this.instance = new lib.Symbol();
	this.instance.parent = this;
	this.instance.setTransform(586.6,364.1,0.666,0.666,-55.9,0,0,24.6,24.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(47).to({_off:false},0).wait(1).to({regY:27.6,scaleX:0.85,scaleY:0.85,rotation:-24.8,x:599.9,y:367.5},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-15.5,x:603.1,y:368.1},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-9.9,x:605.1,y:368.5},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-6.1,x:606.4,y:368.7},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-3.6,x:607.4,y:368.8},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.8,x:607.9,y:368.9},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.8,x:608.3,y:369},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,x:608.5},0).wait(1).to({regY:24.6,scaleX:1,scaleY:1,rotation:0,y:366.1},0).wait(58));

	// + copy
	this.instance_1 = new lib.Symbol();
	this.instance_1.parent = this;
	this.instance_1.setTransform(174.5,109.6,0.666,0.666,-55.9,0,0,24.6,24.7);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(42).to({_off:false},0).wait(1).to({regY:27.6,scaleX:0.85,scaleY:0.85,rotation:-24.8,x:171.1,y:105.2},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-15.5,x:169.4,y:103.5},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-9.9,x:168.3,y:102.4},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-6.1,x:167.6,y:101.7},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-3.6,x:167.2,y:101.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.8,x:166.8,y:100.9},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.8,x:166.6,y:100.7},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,x:166.5,y:100.6},0).wait(1).to({regY:24.6,scaleX:1,scaleY:1,rotation:0,x:166.4,y:97.6},0).wait(63));

	// stella
	this.instance_2 = new lib.stella();
	this.instance_2.parent = this;
	this.instance_2.setTransform(376.4,198.9,0.313,0.313,-90.3,0,0,20.4,19.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(31).to({_off:false},0).wait(1).to({regX:20.5,regY:19.4,scaleX:0.84,scaleY:0.84,rotation:-21.6,x:376.3,y:198.7},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:-10.8},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-5.9,x:376.4},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-3.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.8},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(2).to({regX:20.4,x:376.2,y:198.9},0).to({_off:true},71).wait(1));

	// Layer 4
	this.instance_3 = new lib.cerchio1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(376.3,200.9,0.555,0.555,0,0,0,36.1,36.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(35).to({_off:false},0).wait(1).to({regX:36.2,regY:36.2,scaleX:0.81,scaleY:0.81,y:201},0).wait(1).to({scaleX:0.89,scaleY:0.89,x:376.4},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:376.3},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:376.4},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1,x:376.3},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:36.1,regY:36.1,scaleX:1,scaleY:1,x:376.2,y:200.9},0).to({_off:true},69).wait(1));

	// coppa
	this.instance_4 = new lib.coppa();
	this.instance_4.parent = this;
	this.instance_4.setTransform(377.8,202.4,0.73,0.73,48,0,0,73.2,57.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(39).to({_off:false},0).wait(1).to({regX:71.9,regY:85.3,scaleX:0.9,scaleY:0.9,rotation:17.7,x:368.9,y:226.2},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:10.8,x:371.7,y:228.1},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:7,x:373.3,y:229.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:4.6,x:374.3,y:229.6},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:3.1,x:375,y:230},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:2,x:375.5,y:230.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:1.3,x:375.9,y:230.3},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.8,x:376.1,y:230.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.4,x:376.3,y:230.5},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.2,x:376.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.1,x:376.5},0).wait(1).to({rotation:0},0).wait(1).to({regX:73.2,regY:57.1,x:377.8,y:202.4},0).to({_off:true},61).wait(1));

	// foglie 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AgHg+IgPAVQgQAZADAVQADAWAVAVQAKAKAJAGQAIgIAHgNQAQgZgDgVQgDgWgVgVg");
	this.shape.setTransform(403.9,326.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgLAvQgVgVgDgWQgDgVAQgZIAPgVIATARQAVAVADAWQADAVgQAZQgHANgIAIQgJgGgKgLg");
	this.shape_1.setTransform(403.9,326.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AgxgmIAEAaQAHAbARAOQASANAcAAQAPAAALgCQAAgMgDgOQgIgbgRgOQgSgOgcAAg");
	this.shape_2.setTransform(409.7,337.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgVAcQgSgNgHgbIgEgaIAagDQAdAAARANQASAOAHAcQAEAOAAAMQgLACgPAAQgdAAgRgOg");
	this.shape_3.setTransform(409.7,337.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AgVg7IgKAXQgJAcAHAUQAIAVAZAQQALAIALAEQAGgKAEgOQAJgcgHgUQgIgVgYgQg");
	this.shape_4.setTransform(422.5,316.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAAAxQgZgQgIgVQgHgUAJgcIAKgYIAXAMQAYAQAIAVQAHATgJAcQgEAOgGALQgLgEgLgIg");
	this.shape_5.setTransform(422.5,316.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#222221").p("Ag5gaIAKAYQANAZAUAKQATAJAdgGQAOgDALgFQgDgLgGgOQgOgZgUgJQgTgKgdAGg");
	this.shape_6.setTransform(430.4,325.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgOAhQgUgKgOgZIgJgYIAZgJQAcgGAUAKQAUAJANAZQAHAOACALQgKAFgOADQgMACgKAAQgOAAgMgFg");
	this.shape_7.setTransform(430.5,325.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#222221").p("Agfg2IgGAZQgEAdALASQALAUAaALQANAGAMABQAEgKACgPQAEgdgLgSQgLgUgagLg");
	this.shape_8.setTransform(438.4,303.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAHAwQgagLgLgTQgLgSAEgdIAGgaIAZAIQAaALALATQALASgEAdQgCAPgEALQgMgCgNgGg");
	this.shape_9.setTransform(438.4,303.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("Ag9gQIAOAWQASAXAVAGQAUAGAbgMQAOgFAJgHQgEgLgJgKQgSgYgVgGQgVgFgbALg");
	this.shape_10.setTransform(447.8,310.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgIAjQgWgGgRgXIgOgWIAXgMQAbgLAUAFQAWAGARAYQAKAKAEALQgJAHgOAFQgSAIgPAAQgHAAgHgCg");
	this.shape_11.setTransform(447.8,310.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#222221").p("AgqgvIgBAaQACAcAPARQAOAQAcAGQAPADALgBQACgLgBgPQgCgcgOgRQgPgRgcgFg");
	this.shape_12.setTransform(448.6,290.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAQAuQgcgGgOgQQgPgRgCgcIABgaIAaACQAcAFAPARQAOARACAcQABAPgCALIgDAAQgKAAgNgCg");
	this.shape_13.setTransform(448.6,290.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#222221").p("AhAgDIARASQAXATAWACQAVACAYgRIAUgRQgGgJgLgJQgXgUgWgBQgVgCgYARQgMAIgIAJg");
	this.shape_14.setTransform(459.4,295.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgBAkQgWgCgXgTIgRgSQAIgJAMgIQAYgRAVACQAWACAXATQALAJAGAJIgUARQgWAPgVAAIgCAAg");
	this.shape_15.setTransform(459.3,295.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#222221").p("AgvgqIACAaQAGAcARAPQAQAOAdACQAOABAMgCQAAgMgDgOQgFgcgRgOQgRgPgcgCg");
	this.shape_16.setTransform(458.6,272.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAWArQgcgCgRgOQgRgPgFgcIgDgaIAagBQAdACAQAPQARAOAGAcQADAOgBAMQgJACgLAAIgGgBg");
	this.shape_17.setTransform(458.6,272.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#222221").p("AhAAEIAUARQAZAQAVgBQAWgBAWgUIARgSQgHgJgNgIQgYgQgVABQgWACgWATQgLAKgHAIg");
	this.shape_18.setTransform(470,276.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgrAVIgUgRQAHgIAKgKQAXgTAVgCQAWgBAYAQQANAIAHAJIgRASQgXAUgVABIgDAAQgVAAgWgPg");
	this.shape_19.setTransform(469.9,276.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#222221").p("Ag5gZIAKAYQANAZAVAJQATAJAdgHQAOgDAKgGQgDgLgHgNQgNgZgVgJQgTgJgcAHg");
	this.shape_20.setTransform(463.6,254.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgOAhQgUgJgOgZIgKgYIAZgJQAdgHATAJQAUAJAOAZQAHANADALQgKAGgPADQgMADgLAAQgNAAgMgFg");
	this.shape_21.setTransform(463.7,254.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#222221").p("Ag8AXIAYAKQAcAJAUgIQAUgIAQgZIALgXQgKgGgOgEQgcgJgUAIQgVAIgPAZQgIAMgDALg");
	this.shape_22.setTransform(475.7,255);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgjAhIgYgKQADgLAIgMQAPgZAVgIQAUgIAcAJQAOAEAKAGIgLAXQgQAZgUAIQgKAEgLAAQgMAAgPgFg");
	this.shape_23.setTransform(475.6,255);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#222221").p("Ag9gQIAOAWQASAXAVAGQAVAGAbgMQANgFAKgHQgFgLgJgKQgSgYgVgGQgUgGgcAMg");
	this.shape_24.setTransform(465.3,236.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgIAjQgWgGgSgXIgNgWIAXgMQAbgMAUAGQAWAGASAYQAJAKAEALQgJAHgOAFQgRAIgQAAQgHAAgHgCg");
	this.shape_25.setTransform(465.3,236.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#222221").p("Ag3AgIAZAHQAdAEASgMQAUgLALgaIAHgZQgKgEgPgCQgdgEgTALQgTALgLAaQgFAOgCALg");
	this.shape_26.setTransform(477.2,234.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgdAnIgagHQACgLAGgOQALgaATgLQASgLAdAEQAPACALAEIgIAZQgLAagTALQgOAJgTAAIgOgBg");
	this.shape_27.setTransform(477.2,234.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#222221").p("Ag9gOIAOAVQATAXAVAFQAVAFAagMQAOgGAJgHQgFgLgJgKQgTgXgVgFQgVgFgbAMg");
	this.shape_28.setTransform(464.6,218.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgHAjQgVgFgTgXIgOgVIAWgNQAbgMAVAFQAVAFASAXQAJAKAGALQgJAHgOAGQgTAJgQAAQgGAAgGgCg");
	this.shape_29.setTransform(464.6,218.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#222221").p("Ag2AiIAaAGQAcADATgMQATgMAKgaIAGgaQgLgEgOgBQgdgEgSANQgTALgKAbQgFAOgCALg");
	this.shape_30.setTransform(476.4,216.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgcAoIgZgGQABgMAFgOQALgaASgLQASgMAdACQAPACALAEIgHAZQgKAbgTAMQgPAJgVAAIgLAAg");
	this.shape_31.setTransform(476.4,216.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#222221").p("AhAABIATASQAYASAVgBQAWAAAXgSIASgSQgHgJgMgJQgYgRgVAAQgWABgXASQgLAJgHAIg");
	this.shape_32.setTransform(460.6,203.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgsATIgTgSQAHgIALgKQAXgRAWgBQAVgBAYASQAMAJAHAJIgSASQgXARgWABIgBAAQgVAAgXgRg");
	this.shape_33.setTransform(460.5,203.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#222221").p("AgrAvIAagBQAcgFAQgQQAPgQADgcIAAgaQgLgBgPACQgcAEgPAQQgQAQgDAdQgCAOACAMg");
	this.shape_34.setTransform(471.4,198.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgrAVQADgdAQgQQAPgQAcgEQAPgCALABIAAAaQgDAcgPAQQgQAQgcAFIgaABQgCgMACgOg");
	this.shape_35.setTransform(471.4,198.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},51).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[]},58).wait(1));

	// foglie 1
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#222221").p("AAIg+IAQAVQAPAZgDAVQgDAWgUAVQgLAKgJAGQgIgIgHgNQgPgZACgVQADgWAVgVg");
	this.shape_36.setTransform(343.2,326.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgWArQgQgZADgVQADgWAVgVIATgRIAQAVQAPAZgDAVQgDAWgUAVQgLALgJAGQgIgIgHgNg");
	this.shape_37.setTransform(343.2,326.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#222221").p("AAzgmIgEAaQgIAbgRAOQgSANgbAAIgbgCQAAgMAEgOQAHgbARgOQASgOAcAAQAPAAAMADg");
	this.shape_38.setTransform(337.3,337.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgxAoQAAgMADgOQAHgcASgOQARgNAdAAQAOAAAMADIgEAaQgIAbgRANQgRAOgdAAg");
	this.shape_39.setTransform(337.3,337.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#222221").p("AAWg7IALAXQAJAcgIAUQgHAVgZAQQgMAIgLAEQgGgKgFgOQgIgcAHgUQAIgVAZgQg");
	this.shape_40.setTransform(324.6,316.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgfAkQgJgcAHgTQAHgVAZgQIAXgMIAKAYQAJAcgHAUQgHAVgZAQQgMAIgLAEQgGgLgEgOg");
	this.shape_41.setTransform(324.6,316.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#222221").p("AA7gaIgKAYQgNAZgUAKQgTAJgdgGIgZgIQADgLAHgOQANgZAUgJQATgKAdAGQAOAEALAFg");
	this.shape_42.setTransform(316.5,325.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AggAkIgZgIQADgLAGgOQAOgZAUgJQATgKAdAGQAOAEAKAFIgJAYQgNAZgVAKQgMAFgNAAQgLAAgLgCg");
	this.shape_43.setTransform(316.5,325.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#222221").p("AAgg3IAGAZQAEAdgLASQgLAUgaALIgZAHQgEgKgCgPQgEgdALgSQALgUAagLQANgFAMgCg");
	this.shape_44.setTransform(308.7,303.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AglAeQgEgdALgSQALgTAagLQANgGAMgCIAGAaQAEAdgLASQgLATgaALIgZAIQgEgLgCgPg");
	this.shape_45.setTransform(308.7,303.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#222221").p("AA/gQIgOAWQgSAXgVAGQgUAGgcgMIgXgMQAFgLAJgKQASgYAVgGQAVgFAbALQANAGAKAGg");
	this.shape_46.setTransform(299.2,310.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgmAdIgXgMQAEgLAJgKQASgYAWgGQAUgFAbALQAOAGAJAGIgNAWQgSAXgWAGQgHACgHAAQgPAAgSgIg");
	this.shape_47.setTransform(299.2,310.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#222221").p("AArgvIABAaQgCAcgPARQgOAQgcAGIgaACQgCgLABgPQACgcAOgRQAPgRAcgFQAOgDAMABg");
	this.shape_48.setTransform(298.4,290.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgqAWQACgcAOgRQAPgRAcgFQAOgDAMABIABAaQgCAcgPARQgOAQgcAGIgaACQgCgLABgPg");
	this.shape_49.setTransform(298.4,290.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#222221").p("ABBgDIgSASQgWATgWACQgVACgYgRIgUgRQAGgJALgJQAXgUAWgBQAVgCAYARQAMAIAIAJg");
	this.shape_50.setTransform(287.6,295.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgrAVIgUgRQAHgJALgJQAWgTAWgCQAVgCAYARQAMAIAIAJIgSASQgVATgXACIgCAAQgVAAgWgPg");
	this.shape_51.setTransform(287.7,295.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#222221").p("AAwgpIgCAaQgGAcgQAOQgRAPgcACIgagCQgBgLADgPQAGgbAQgPQARgOAcgCQAPgBALACg");
	this.shape_52.setTransform(288.4,272.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgvAqQgBgLADgPQAGgbAQgPQARgOAcgCQAPgBALACIgCAaQgGAcgQAOQgRAPgcACg");
	this.shape_53.setTransform(288.4,272.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#222221").p("ABBAEIgUARQgZAQgVgBQgWgBgWgUIgRgSQAIgJAMgIQAYgQAVABQAWACAXATQALAKAGAIg");
	this.shape_54.setTransform(277,276.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgCAkQgWgBgWgUIgRgSQAIgJAMgIQAYgQAVABQAWACAXATQALAKAGAIIgUARQgXAPgUAAIgDAAg");
	this.shape_55.setTransform(277.1,276.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#222221").p("AA7gZIgJAYQgOAZgUAJQgUAJgcgHIgZgJQADgLAHgNQANgZAVgJQATgJAdAHQAOAEAKAFg");
	this.shape_56.setTransform(283.3,254.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AghAjIgZgJQADgLAHgNQANgZAVgJQATgJAdAHQAOAEAKAFIgJAYQgOAZgUAJQgMAFgNAAQgLAAgMgDg");
	this.shape_57.setTransform(283.4,254.6);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#222221").p("AA9AXQgKAGgOAEQgcAJgUgIQgVgIgPgZIgLgXIAYgKQAcgJAUAIQAUAIAQAZQAIAMADALg");
	this.shape_58.setTransform(271.3,255);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgMAiQgVgIgPgZIgLgXIAYgKQAcgJAUAIQAUAIAQAZQAIAMADALQgKAGgOAEQgPAFgNAAQgKAAgKgEg");
	this.shape_59.setTransform(271.4,255);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#222221").p("AA/gQIgOAWQgRAXgWAGQgUAGgbgMIgXgMQAEgLAJgKQASgYAVgGQAVgGAbAMQAOAFAJAHg");
	this.shape_60.setTransform(281.6,236.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgmAdIgXgMQAEgLAJgKQASgYAVgGQAVgGAbAMQAOAFAJAHIgNAWQgSAXgWAGQgHACgHAAQgPAAgSgIg");
	this.shape_61.setTransform(281.7,236.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#222221").p("AA4AgQgKAFgPACQgdAEgTgMQgTgLgLgaIgHgZIAZgGQAdgEASALQAUALALAaQAGAOABALg");
	this.shape_62.setTransform(269.8,234.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgRAfQgTgLgLgaIgIgZIAZgGQAdgEATALQATALALAaQAGAOACALQgLAFgPACIgOABQgTAAgOgJg");
	this.shape_63.setTransform(269.9,234.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#222221").p("AA/gOIgOAVQgSAXgWAFQgUAFgbgMIgXgNQAFgLAKgKQASgXAWgFQAUgFAbAMQANAGAJAHg");
	this.shape_64.setTransform(282.3,218.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgnAcIgXgNQAGgLAJgKQATgXAVgFQAUgFAbAMQANAGAJAHIgNAVQgTAXgWAFQgGACgGAAQgQAAgTgJg");
	this.shape_65.setTransform(282.4,218.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#222221").p("AA3AiQgLAEgOACQgdADgSgMQgTgMgLgaIgGgaIAZgFQAdgEASANQATALALAbQAFAOABALg");
	this.shape_66.setTransform(270.6,216.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgSAfQgTgMgKgbIgHgZIAagGQAcgCATAMQATALAKAaQAFAOACAMQgLAEgPACIgLAAQgVAAgPgJg");
	this.shape_67.setTransform(270.6,216.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#222221").p("ABBABIgTASQgYASgWgBQgVAAgXgSIgTgSQAIgJALgJQAYgRAVAAQAWABAYASQALAJAHAIg");
	this.shape_68.setTransform(286.4,203.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AAAAkQgVgBgYgRIgSgSQAHgJAMgJQAXgSAWABQAWABAXARQALAKAIAIIgTASQgYARgVAAIgBAAg");
	this.shape_69.setTransform(286.5,203.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#222221").p("AAsAuQgLABgPgCQgcgEgPgQQgQgQgDgdIAAgaIAaABQAcAFAQAQQAPAQADAcQACAPgCALg");
	this.shape_70.setTransform(275.6,198.6);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AASAtQgcgEgPgQQgQgQgDgdIAAgaIAaABQAcAFAQAQQAPAQADAcQACAPgCALIgIABIgSgCg");
	this.shape_71.setTransform(275.6,198.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},51).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},1).to({state:[]},58).wait(1));

	// rami
	this.instance_5 = new lib.alloro();
	this.instance_5.parent = this;
	this.instance_5.setTransform(373.6,276,0.85,1,0,0,0,98.4,73.7);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(45).to({_off:false},0).wait(1).to({regY:73.6,scaleX:0.95,y:275.9},0).wait(1).to({scaleX:0.98},0).wait(1).to({scaleX:0.99,x:373.5},0).wait(1).to({scaleX:1},0).wait(1).to({scaleX:1,x:373.6},0).wait(1).to({regY:73.7,y:276},0).to({_off:true},62).wait(1));

	// c
	this.instance_6 = new lib.Tween3("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(376.3,232.2,0.387,0.387,0,0,0,0,0.1);
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(376.3,230.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},32).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(76));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(32).to({_off:false},0).wait(1).to({regY:0,scaleX:0.69,scaleY:0.69,y:231.4},0).wait(1).to({scaleX:0.81,scaleY:0.81,y:231.1},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:230.9},0).wait(1).to({scaleX:0.95,scaleY:0.95,y:230.8},0).wait(1).to({scaleX:0.99,scaleY:0.99,y:230.7},0).to({_off:true},1).wait(76));

	// c2
	this.instance_8 = new lib.Tween5("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(376.3,233.1,0.387,0.387,0,0,0,0.1,0.1);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween6("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(376.3,233);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},33).to({state:[{t:this.instance_9}]},6).to({state:[]},74).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(33).to({_off:false},0).to({_off:true,regX:0,regY:0,scaleX:1,scaleY:1,y:233},6,cjs.Ease.get(1)).wait(75));

	// uccelini all
	this.instance_10 = new lib.Uccellini();
	this.instance_10.parent = this;
	this.instance_10.setTransform(359.2,289.5,1,1,0,0,0,234.8,108.1);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(20).to({_off:false},0).wait(1).to({regX:251.5,regY:107.9,x:375.9,y:290},0).wait(1).to({y:292.2},0).wait(1).to({y:296.5},0).wait(1).to({y:303.5},0).wait(1).to({y:314.5},0).wait(1).to({y:331.4},0).wait(1).to({y:357.4},0).wait(1).to({y:395.4},0).wait(1).to({y:436.3},0).wait(1).to({y:466.2},0).wait(1).to({y:485.4},0).wait(1).to({y:497.7},0).wait(1).to({y:505.5},0).wait(1).to({y:510.2},0).wait(1).to({y:512.6},0).wait(1).to({regX:234.8,regY:108.1,x:359.2,y:513.5},0).to({_off:true},77).wait(1));

	// pallini bianchi
	this.instance_11 = new lib.Pbianchi();
	this.instance_11.parent = this;
	this.instance_11.setTransform(330.4,228.4,0.548,0.548,-25.7,0,0,242,123);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(31).to({_off:false},0).wait(1).to({regX:241.8,regY:123.2,scaleX:0.82,scaleY:0.82,rotation:-10.3,x:330.2,y:228.5},0).wait(1).to({scaleX:0.89,scaleY:0.89,rotation:-6.3},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-3.9},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-2.5},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-1.5,y:228.4},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-0.9},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.5,x:330.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,x:330.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:0,y:228.5},0).wait(1).to({regY:123,x:330.4,y:228.3},0).to({y:233.7},8).to({y:228.3},9).to({y:222.9},14).to({y:228.3},14).to({y:222.9},13).to({y:228.3},12).to({_off:true},1).wait(1));

	// pallini rosa
	this.instance_12 = new lib.Tween9("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(369.3,255.6,0.643,0.643,27.2);
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween10("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(369.2,255.5);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(34).to({_off:false},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:13,x:369.2,y:255.5},0).wait(1).to({scaleX:0.89,scaleY:0.89,rotation:8.7},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:6.1},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:4.2},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:2.9},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:1.9},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:1.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.7},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).to({_off:true},1).wait(68));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(46).to({_off:false},0).to({y:250.1},13).to({y:255.5},14).to({y:250.1},14).to({y:255.5},13).to({y:250.1},12).to({_off:true},1).wait(1));

	// cuore
	this.instance_14 = new lib.cuore();
	this.instance_14.parent = this;
	this.instance_14.setTransform(374.1,228.8,0.304,0.304,0,0,0,36.4,63.2);
	this.instance_14._off = true;

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FC3065").s().p("AgYGRQgbgLgJgiQgFgRACgqQAEhwgDhdQgGh5gBg+QgDhXADiuQAAgeAKgLQAJgKAQgBQAPAAANAHQAnASASAxQAMAiADA4QAGBTAAC6QAACXgDBkQgBAdgDAQQgEAZgKASQgMAVgVAKQgNAGgLAAQgKAAgIgEg");
	this.shape_72.setTransform(371.9,243.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FC3065").s().p("AADGnQgRgHgNgQQgMgPgFgTQgCgMgBgUIgBggIgHgwQgJgxgBg+QgBgiABhAIgIhHQgEg6AChIIACgzIgEgaIgGglQgFgqAJgmQAIgjASgNQAGgKAIgHQAMgJANgBQAbAAAWAkQAUAgAKAtQAHAgAEA0QAIBgACB4QABBNgCCLQgBBlgDAzQgDAtgVAOIgGADIgLAFQgKADgJAAQgJAAgJgDg");
	this.shape_73.setTransform(372.3,268.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FC3065").s().p("Ag1CRQgVgRgWgsQgeg8gMgfQgUg1gCgsQgBggANgPQAMgNAWgBQASAAAUAIQAbAMAXAQQAKAKAIABQAIACAUgKQAbgNAagCQAegBAWAOQAaATAHAmQAHAggIAnQgQBcg0ArQgfAagoAEIgMABQgiAAgZgVg");
	this.shape_74.setTransform(375.3,400.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},6).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.shape_72}]},2).to({state:[{t:this.shape_73}]},2).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.instance_14}]},1).to({state:[]},81).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(6).to({_off:false},0).wait(1).to({regX:36.2,regY:43.3,scaleX:0.4,scaleY:0.4,y:221.1},0).wait(1).to({scaleX:0.49,scaleY:0.49,y:219.8},0).wait(1).to({scaleX:0.56,scaleY:0.56,x:374.2,y:218.6},0).wait(1).to({scaleX:0.62,scaleY:0.62,y:217.6},0).wait(1).to({scaleX:0.67,scaleY:0.67,y:216.8},0).wait(1).to({scaleX:0.72,scaleY:0.72,y:216},0).wait(1).to({scaleX:0.76,scaleY:0.76,y:215.3},0).wait(1).to({scaleX:0.8,scaleY:0.8,y:214.8},0).wait(1).to({scaleX:0.83,scaleY:0.83,x:374.3,y:214.3},0).wait(1).to({scaleX:0.86,scaleY:0.86,y:213.8},0).wait(1).to({scaleX:0.88,scaleY:0.88,y:213.4},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:213.1},0).wait(1).to({scaleX:0.92,scaleY:0.92,y:212.7},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:374.4,y:212.4},0).wait(1).to({scaleX:0.96,scaleY:0.96,y:212.2},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:374.3,y:211.9},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:374.4,y:211.8},0).wait(1).to({scaleX:1,scaleY:1,y:211.6},0).wait(1).to({scaleX:1,scaleY:1,y:211.4},0).wait(1).to({regX:36.4,regY:63.4,scaleX:1.01,scaleY:1.01,x:374.6,y:231.5},0).to({_off:true},2).wait(4).to({_off:false,y:447.5},0).to({_off:true},81).wait(1));

	// zampa sx
	this.instance_15 = new lib.z2();
	this.instance_15.parent = this;
	this.instance_15.setTransform(448.5,318.4,1,1,-74.7,0,0,62.1,3.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(1).to({regX:32.6,regY:13.2,rotation:-28.7,x:427.4,y:341.5},0).wait(1).to({rotation:-16.4,x:423,y:336.4},0).wait(1).to({rotation:-9.5,x:421,y:333.2},0).wait(1).to({rotation:-5.2,x:420,y:331.1},0).wait(1).to({rotation:-2.6,x:419.4,y:329.8},0).wait(1).to({rotation:-1,x:419.2,y:329},0).wait(1).to({rotation:-0.2,x:419,y:328.6},0).wait(1).to({regX:62.1,regY:3.1,rotation:0,x:448.4,y:318.4},0).to({_off:true},12).wait(94));

	// grigio
	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#3F3E3E").s().p("Ag8A9QgZgaAAgjQAAgjAZgZQAagZAiAAQAjAAAaAZQAZAZAAAjQAAAjgZAaQgaAZgjAAQgiAAgagZg");
	this.shape_75.setTransform(477.1,225.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#222221").p("AEhAAQAAB4hVBUQhVBVh3AAQh3AAhUhVQhVhUAAh4QAAh2BVhVQBUhVB3AAQB3AABVBVQBVBVAAB2g");
	this.shape_76.setTransform(477.1,225.5);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AjLDMQhVhUAAh4QAAh2BVhVQBUhVB3AAQB3AABVBVQBVBVAAB2QAAB4hVBUQhVBVh3AAQh3AAhUhVg");
	this.shape_77.setTransform(477.1,225.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#222221").p("AM4BYIq9AAIAAmoQAAjEiHiMQiHiOjDgDQjGgEiNCMQiNCLAADGIAAMGIAAAAIAAAXQAABKAcBDQAqBkBaA+QBdA/BwAAIImAAQCVAACIg6QCDg3BlhlQBlhlA3iEQA6iHAAiVg");
	this.shape_78.setTransform(511.4,263.5);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#E6E6E6").s().p("AnJMzQhwAAhdg/Qhag+grhkQgchDAAhKIABgXIgBAAIAAsGQABjGCNiLQCNiMDHAEQDBADCICOQCGCMAADEIAAGoIK9AAQAACVg5CHQg3CEhmBlQhlBliCA3QiJA6iUAAg");
	this.shape_79.setTransform(511.4,263.5);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#222221").p("AAGkNIgLIc");
	this.shape_80.setTransform(492.3,355.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#222221").p("AAZA3Igxht");
	this.shape_81.setTransform(494.1,387.2);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#222221").p("AhrBTIA7igICfA7");
	this.shape_82.setTransform(496.5,389.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#222221").p("ACUivIk2DUIFKCMg");
	this.shape_83.setTransform(427.6,234);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AikAkIE2jTIATFfg");
	this.shape_84.setTransform(427.8,234.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75}]}).to({state:[]},20).to({state:[]},93).wait(1));

	// zampa dx
	this.instance_16 = new lib.z();
	this.instance_16.parent = this;
	this.instance_16.setTransform(295.4,318.5,1,1,54.7,0,0,3.1,3.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({rotation:0,x:295.5},8).to({_off:true},12).wait(94));

	// fuxia
	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#3F3E3E").s().p("Ag8A9QgZgaAAgjQAAgjAZgZQAZgZAjAAQAjAAAaAZQAZAZAAAjQAAAjgZAaQgaAZgjAAQgjAAgZgZg");
	this.shape_85.setTransform(274.5,225.5);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#222221").p("AkgAAQAAB4BUBUQBVBVB3AAQB3AABVhVQBVhUAAh4QAAh2hVhVQhVhVh3AAQh3AAhVBVQhUBVAAB2g");
	this.shape_86.setTransform(274.5,225.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AjMDMQhUhUAAh4QAAh2BUhVQBVhVB3AAQB3AABWBVQBUBVAAB2QAAB4hUBUQhWBVh3AAQh3AAhVhVg");
	this.shape_87.setTransform(274.5,225.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FC3065").s().p("AhcMzQkuAAjXjWQhkhmg4iDQg6iHABiVIK9AAIAAmoQAAjECGiMQCIiODBgDQDHgECOCMQCMCLABDGIAAMGIgBAAIABAXQAABKgcBDQgrBkhaA+QhdA/hwAAg");
	this.shape_88.setTransform(240.1,263.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#222221").p("AgFkNIAMIc");
	this.shape_89.setTransform(259.3,355.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#222221").p("AgZA3IAzht");
	this.shape_90.setTransform(257.4,387.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#222221").p("ABsBTIg6igIigA7");
	this.shape_91.setTransform(255,389.5);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#222221").p("AiTivIE2DUIlKCMg");
	this.shape_92.setTransform(323.9,234);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AiRivIE2DTIlJCMg");
	this.shape_93.setTransform(323.7,234.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85}]}).to({state:[]},20).to({state:[]},93).wait(1));

	// focus
	this.instance_17 = new lib.Tween7("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(375,330.2,0.495,0.495);
	this.instance_17._off = true;

	this.instance_18 = new lib.Tween8("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(375,330.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[]},12).to({state:[]},89).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(2).to({_off:false},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.85,scaleY:0.85},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).to({_off:true},1).wait(102));

	// Layer 28 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_14 = new cjs.Graphics().p("AEuA3QgMgMACgMQABgIAIgFQAIgFAJACIAHACQAFABADAAQAFgBAGgEQALgIAPgDQAVgDAKALQAJAKgHASQgEAJgLARQgHALgDABQgEADgKABIgDAAQgiAAgZgZgAmNBFQgFgEgCgNQgCgPADgHQADgKAMgLQAKgIAEgBQAIgEAQAAIAnAAQAVAAAHAHQAGAFAAAJQgBAIgGAFQgGAGgRABIgWABQgHAAgCADQgCACgBAHQgBALgMAGQgGAEgPACIgIAAQgKAAgEgEgAgGgBQgLgBgEgEQgGgGgBgRIAAgTQgBgPAFgIQAGgIAKABQAHAAAGAFQAGAFADAHQAFAKgBAUQAAAJgCAHQgDAIgHAEQgFACgEAAIgDAAg");
	var mask_graphics_15 = new cjs.Graphics().p("AEuBAQgMgMACgLQABgJAIgFQAIgFAJACIAHACQAFACADgBQAEAAAFgEIgBgIQAAgLAHgFQADgDAKgEIAcgIQAOgEAFAAQAIABAGAFQAHAFABAHQACANgQALIgLAGQACAIgEALQgEAJgLARQgHALgDACQgEACgKABIgDAAQgiAAgZgZgAmNBPQgFgFgCgMIAAgFIgFgBQgIgCgDgGIgHgVQgDgGABgEQAAgHALgKQAGgGAFgDQALgGAOADQANADAJAKIACADIAIAAIAnABQAVAAAHAHQAGAGAAAIQgBAJgGAFQgGAGgRABIgWABQgHAAgCADQgCABgBAHQgBAMgMAGQgGADgPACIgIABQgKAAgEgEgAgGAIQgLgBgEgFQgGgEgBgSIAAgFQgEgBgEgFQgHgJADgLQACgJANgNQANgOAJAAIACgBQAHAAAGAIQAFAHAAAJQABAGgEAGIAAAAQAFALgBATQAAAKgCAGQgDAIgHAEQgFACgEAAIgDAAg");
	var mask_graphics_16 = new cjs.Graphics().p("AEqBdQgMgMACgMQABgIAIgFQAIgFAJACIAHACQAFABADAAQAEgBAFgDIgBgJQAAgLAHgGQADgDAKgDIAYgIQABgIACgEQAEgHAIgFQALgFAMABQAJAAAGADQAPAIgCAQQgBAPgNALQgGADgHADIgIAHIgLAGQACAIgEAMQgEAJgLARQgHALgDABQgEADgKABIgDAAQgiAAgZgZgAmRBrQgFgEgCgNIAAgEIgFgBQgIgDgDgGIgHgUQgDgGABgEQAAgHAJgJQgJgCgEgCQgJgFAAgKQAAgKAIgGQAGgFASAAIARAAQAMAAAFADQAHAEADAHQADAIgCAIIgBAEIAGAGIACADIAIAAIAnAAQAVAAAHAIQAGAFAAAJQgBAIgGAFQgGAGgRABIgWABQgHAAgCADQgCACgBAHQgBALgMAGQgGAEgPACIgIAAQgKAAgEgEgAgKAkQgLgBgEgEQgGgGgBgRIAAgFQgEgCgEgDQgHgJADgLIABgDQgHgKgBgXQgBgaAGgNQAEgKAIgFQAJgGAJACQALADAIARQAEAIgBAGQAAAEgDAEIgEAJIgCAFQAGACAFAHQAFAHAAAIQABAGgEAGIAAABQAFAKgBATQAAAJgCAHQgDAIgHAEQgEACgFAAIgDAAg");
	var mask_graphics_17 = new cjs.Graphics().p("AEmByQgMgMACgLQABgJAJgFQAIgFAIACIAIACQAEACADgBQAFAAAFgDIgCgJQAAgLAHgGQAEgDAKgEIAYgHQAAgIADgEQADgIAJgFIAIgDQgEgGAAgJQABgLAEgFQAJgKAPAEQAFACAMAJQAOAOAEAGQAJAMgCAMQgBANgKAFQgJAFgHgDQgEAIgIAGQgFAEgHADIgIAHIgLAGQACAIgEALQgEAJgMARQgHALgCACQgFACgKABIgCAAQgjAAgZgZgAmVCBQgEgFgCgMIgBgFIgFgBQgHgCgDgGIgIgVQgCgGAAgEQABgGAJgKQgJgBgFgDQgIgFAAgJIgDgBQgGgBgEgEQgFgEgBgNQAAgIADgEQADgGALgGQAJgEADAAQABAAAAAAQABAAABgBQAAAAAAAAQABAAAAgBQAHACAFAEQAGAFAAAHQABAFgDAIIABAAIARAAQAMAAAGADQAGADADAJQADAHgBAIIgBAFIAFAFIADAEIAHgBIAoABQAVAAAHAHQAFAGAAAIQAAAJgGAFQgHAGgRABIgWABQgGAAgDADQgBABgBAHQgCAMgLAGQgHADgPACIgIABQgJAAgFgEgAgOA6QgKgBgFgFQgGgFgBgSIAAgEQgEgCgDgFQgHgJADgKIABgCQgHgKgBgYQgBgaAGgNQADgIAIgGIgBgCQgDgGABgEIAEgNIADgLQADgGAFgDQAHgEAHABQAHAAAFADQAIAFACANQACAMgHAXIgEAKQADAHgBAEQgBAEgCAFIgFAIIgBAGQAGABAEAHQAFAHAAAJQABAGgDAGIAAAAQAEAKgBATQAAAKgCAGQgDAJgGAEQgFACgEAAIgEAAg");
	var mask_graphics_18 = new cjs.Graphics().p("AE8CLQgMgMACgLQABgJAIgFQAIgFAJACIAHACQAFACADgBQAEAAAFgDIgBgJQAAgLAHgGQADgDAKgEIAYgHQABgIACgEQAEgIAIgFIAIgDQgDgGgBgKQABgLAFgEQAJgKAOAEIADABIgBgHQABgMAEgGQAGgIAJAAQAEABAHAAIAKgBQAMACAFANQAFAMgEAKIgNAPQgIAJgDACIAAAAQAIANgCALQgBANgLAFQgIAFgHgDQgEAIgIAGQgFAEgHADIgJAHIgKAGQACAIgEALQgFAJgLARQgHALgDACQgEACgKABIgDABQgiAAgZgagAl/CaQgFgFgCgMIAAgFIgFgBQgIgCgDgGIgHgVQgDgGABgEQAAgGAJgKQgIgBgFgDIgBAAIgCAAQgJgBgGgGIgNgPQgDgDgPgJQgRgKgOgPQgNgNAEgMQABgDAEgEIAFgGIAFgJQADgGAEgCQAIgFATALQAfASAjAMQAZAHAEAMQACAFgCAGQgBAFgEAEIgEADQAIABADACQAHADADAJQADAHgCAIIgBAFIAGAFIACAEIAIgBIAnABQAWAAAGAHQAGAGAAAIQgBAJgGAFQgGAGgRABIgWABQgHAAgCADQgCABgBAHQgBAMgMAGQgGADgPACIgIABQgKAAgEgEgAAHBTQgKgBgEgFQgGgFgBgSIAAgEQgEgCgEgFQgHgJADgLIABgCQgHgKgBgXQgBgaAGgNQAEgIAHgGIgBgCQgCgGAAgEIABgBIgDgJQgFgOACgJQACgPAMgCQgHgWAKgJQAFgEAMgBIAPgBQAPAAAGADQAKAFABAMQAAAMgKAFQgJAEgDADQgDAEABAMQABASgCAZQgBAPgGAGIAAAEQAAAEgDAFIgFAIIgCAGQAHACAFAGQAFAGAAAJQABAGgEAGIAAAAQAFALgBATQAAAKgCAGQgDAJgHAEQgFACgFAAIgDAAg");
	var mask_graphics_19 = new cjs.Graphics().p("AE8CLQgMgMACgLQABgJAIgFQAIgFAJACIAHACQAFACADgBQAEAAAFgDIgBgJQAAgLAHgGQADgDAKgEIAYgHQABgIACgEQAEgIAIgFIAIgDQgDgGgBgKQABgLAFgEQAJgKAOAEIADABIgBgHQABgMAEgGQAGgIAJAAQAEABAHAAIAKgBQAMACAFANQAFAMgEAKIgNAPQgIAJgDACIAAAAQAIANgCALQgBANgLAFQgIAFgHgDQgEAIgIAGQgFAEgHADIgJAHIgKAGQACAIgEALQgFAJgLARQgHALgDACQgEACgKABIgDABQgiAAgZgagAl/CaQgFgFgCgMIAAgFIgFgBQgIgCgDgGIgHgVQgDgGABgEQAAgGAJgKQgIgBgFgDIgBAAIgCAAQgJgBgGgGIgNgPQgDgDgPgJQgRgKgOgPQgNgNAEgMQABgDAEgEIAFgGIAFgJQADgGAEgCQAIgFATALQAfASAjAMQAZAHAEAMQACAFgCAGQgBAFgEAEIgEADQAIABADACQAHADADAJQADAHgCAIIgBAFIAGAFIACAEIAIgBIAnABQAWAAAGAHQAGAGAAAIQgBAJgGAFQgGAGgRABIgWABQgHAAgCADQgCABgBAHQgBAMgMAGQgGADgPACIgIABQgKAAgEgEgAAHBTQgKgBgEgFQgGgFgBgSIAAgEQgEgCgEgFQgHgJADgLIABgCQgHgKgBgXQgBgaAGgNQAEgIAHgGIgBgCQgCgGAAgEIABgBIgDgJQgFgOACgJQACgPAMgCQgHgWAKgJQAFgEAMgBIAPgBQAPAAAGADQAKAFABAMQAAAMgKAFQgJAEgDADQgDAEABAMQABASgCAZQgBAPgGAGIAAAEQAAAEgDAFIgFAIIgCAGQAHACAFAGQAFAGAAAJQABAGgEAGIAAAAQAFALgBATQAAAKgCAGQgDAJgHAEQgFACgFAAIgDAAg");
	var mask_graphics_20 = new cjs.Graphics().p("AGVCLIgFgXQgFgNgFgIQgHgJgKgFIACAAIAYgHQABgJACgEQAEgIAIgFIAIgDQgDgGgBgKQABgLAFgEQAJgLAOAEIADABIgBgGQABgMAEgGQAGgHAJAAQAEABAHgBIAKAAQAMABAFAOQAFALgEAKIgNAQQgIAIgDACIAAAAQAIANgCAMQgBAMgLAGQgIAFgHgDQgEAHgIAHQgFADgHADIgJAHIgKAGQACAIgEAMIgIANIgFgLgAmWCJIgHgUQgDgGABgEQAAgHAJgJQgIgBgFgDIgBgBIgCAAQgJgBgGgFIgNgPQgDgDgPgJQgRgKgOgPQgNgPAEgLQABgCAEgEIAFgGIAFgKQADgFAEgDQAIgFATALQAfATAjAKQAZAJAEAMQACAFgCAFQgBAGgEADIgEAEQAIAAADACQAHAEADAIQADAIgCAIIgBAEIAGAGIACADIAIAAIAnAAQAWAAAGAIQAEAEABAFIgYAEIgUAFQgFgLgLgHQgKgFgIgBQgRAAgOAKQgNAKgFAQIgDAJIgGACQgEgCgCgEgAgWA6QgHgJADgLIABgDQgHgKgBgXQgBgZAGgNQAEgJAHgFIgBgCQgCgGAAgEIABgCIgDgJQgFgNACgKQACgOAMgDQgHgWAKgIQAFgEAMgBIAPgBQAPAAAGACQAKAFABAMQAAAMgKAGQgJADgDAEQgDAEABALQABASgCAZQgBAPgGAGIAAAFQAAAEgDAEIgFAJIgCAEQAHACAFAHQAFAHAAAIQABAGgDAFIgLgDIgBACIgCAAQgQAAgMANQgEAFgDAHIgCgBg");
	var mask_graphics_21 = new cjs.Graphics().p("AmdCAQgDgGABgEQAAgHAJgJQgIgBgFgDIgBgBIgCAAQgJgBgGgFIgNgPQgDgDgPgJQgRgKgOgPQgNgPAEgLQABgDAEgEIAFgGIAFgJQADgFAEgDQAIgFATALQAfASAjALQAZAJAEAMQACAFgCAFQgBAGgEADIgEADQAIABADACQAHAEADAIQADAIgCAIIgBAEIAGAGIACADIgHADQgLABgLAIIgBABQgVAKgOALgAGJBvIgDgFQgHgJgKgFIACAAIAYgIQABgIACgEQAEgIAIgFIAIgDQgDgGgBgKQABgLAFgEQAJgLAOAEIADABIgBgGQABgNAEgFQAGgHAJAAQAEABAHgBIAKAAQAMABAFANQAFAMgEAKIgNAQQgIAIgDACIAAAAQAIANgCAMQgBAMgLAGQgIAFgHgEQgEAIgIAHQgFADgHADIgJAHIgKAGIgbgRgAgPASIgRAAIgBgFQgBgZAGgNQAEgJAHgFIgBgCQgCgGAAgEIABgCIgDgJQgFgNACgKQACgOAMgDQgHgWAKgIQAFgEAMgBIAPgBQAPAAAGACQAKAFABAMQAAAMgKAGQgJADgDAEQgDAEABALQABASgCAZQgBAPgGAGIAAAFQAAAEgDAEIgFAIIgCAFQAHACAFAHIABACQgYgDgSgBg");
	var mask_graphics_22 = new cjs.Graphics().p("AF/BqQgEgEgGgCIACgBIAYgHQABgIACgEQAEgIAIgFIAIgEQgDgFgBgKQABgLAFgFQAJgKAOAEIADABIgBgHQABgMAEgGQAGgHAJAAQAEABAHAAIAKgBQAMACAFAMQAFAMgEAKIgNAQQgIAJgDACIAAAAQAIANgCALQgBANgLAFQgIAFgHgDQgEAIgIAGQgFAEgHADIgJAHIgEACIgrgagAmjBpIgBAAIgCAAQgJgBgGgGIgNgPQgDgDgPgJQgRgKgOgPQgNgOAEgMQABgDAEgEIAFgGIAFgIQADgGAEgCQAIgFATALQAfARAjAMQAZAIAEAMQACAFgCAGQgBAFgEAEIgEADQAIABADACQAEABACADQgjAOgZARIgDgCgAgcgRIAAgBQAEgJAHgFIgBgCQgCgGAAgEIABgBIgDgJQgFgOACgJQACgPAMgCQgHgWAKgJQAFgEAMgBIAPgBQAPAAAGADQAKAFABAMQAAAMgKAFQgJAEgDADQgDAEABAMQABASgCAZIgCALQgbgDgeADg");
	var mask_graphics_23 = new cjs.Graphics().p("AG7BJQgLgKgLgHIADgDQAJgLAOAEIADABIgBgHQABgMAEgGQAGgIAJAAQAEACAHgBIAKgBQAMACAFANQAFANgEAKIgNAPQgIAJgDACIAAAAQAIANgCALQAAAGgDADQgVgUgXgSgAnUBbQgRgKgOgPQgNgOAEgMQABgCAEgEIAFgGIAFgKQADgFAEgDQAIgFATALQAZAPAcAKQgbASgUAVIgLANIgEgCgAgZg0IAAgFQACgPAMgCQgHgWAKgJQAFgDAMgBIAPgCQAPAAAGADQAKAFABAMQAAAMgKAGQgJADgDADQgDAFABALIAAAGQgegFgbgCg");
	var mask_graphics_24 = new cjs.Graphics().p("AG/BFIgGgCIAGABIADABIgBgHQABgMAEgGQAGgIAJAAQAEABAHAAIAKgBQAMACAFANQAFAMgEAKIgNAQIgBABQgUgKgbgLgAnzBYQgNgOAEgMQABgDAEgEIAFgGIAFgJQADgGAEgCQAIgFATALIAZANIgxAcIgPAKIgBgBgAgPgsIgGgBQADgGAHgBQgHgWAKgJQAFgEAMgBIAPgBQAPAAAGADQAKAFABAMQAAAMgKAFQgJAEgDADQgBACgBADIgvgEg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_graphics_14,x:374.2,y:152.2}).wait(1).to({graphics:mask_graphics_15,x:374.2,y:151.3}).wait(1).to({graphics:mask_graphics_16,x:374.6,y:148.4}).wait(1).to({graphics:mask_graphics_17,x:374.9,y:146.3}).wait(1).to({graphics:mask_graphics_18,x:372.8,y:143.8}).wait(1).to({graphics:mask_graphics_19,x:372.8,y:143.8}).wait(1).to({graphics:mask_graphics_20,x:372.8,y:142.3}).wait(1).to({graphics:mask_graphics_21,x:372.8,y:141.2}).wait(1).to({graphics:mask_graphics_22,x:372.8,y:140.5}).wait(1).to({graphics:mask_graphics_23,x:372.8,y:138.5}).wait(1).to({graphics:mask_graphics_24,x:372.8,y:136.3}).wait(1).to({graphics:null,x:0,y:0}).wait(89));

	// Layer 27
	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#FFFFFF").p("AAABpIAAjR");
	this.shape_94.setTransform(372.8,139.5);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#FFFFFF").p("AhABBICBiB");
	this.shape_95.setTransform(413.8,150);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#FFFFFF").p("ABBBBIiBiB");
	this.shape_96.setTransform(331.8,150);

	var maskedShapeInstanceList = [this.shape_94,this.shape_95,this.shape_96];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_96},{t:this.shape_95},{t:this.shape_94}]},14).to({state:[]},11).to({state:[]},88).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(532.8,518.2,437,218.1);
// library properties:
lib.properties = {
	id: 'A5B3FD0621774932A92D1670E4228B46',
	width: 750,
	height: 675,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A5B3FD0621774932A92D1670E4228B46'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;