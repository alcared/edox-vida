(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.viola = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#222221").p("AhGiPICNEe");
	this.shape.setTransform(129.1,77.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#222221").p("AArAzIhVhl");
	this.shape_1.setTransform(140.2,96.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AhiBgIAQi6IC6AQ");
	this.shape_2.setTransform(144.2,100.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3E3E").s().p("Ag3A6QgYgYAAgiQAAghAYgXQAXgZAgAAQAhAAAXAZQAXAXABAhQgBAhgXAZQgXAYghgBQggABgXgYg");
	this.shape_3.setTransform(54.1,38.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AELAAQAABxhOBRQhOBQhvAAQhtAAhPhQQhOhQAAhyQAAhwBOhRQBPhQBtAAQBvAABOBQQBOBRAABwg");
	this.shape_4.setTransform(54,38.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ai8DCQhOhQAAhyQAAhwBOhRQBPhQBtAAQBvAABOBQQBOBRAABwQAABxhOBRQhOBQhvAAQhtAAhPhQg");
	this.shape_5.setTransform(54,38.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5F2CCC").s().p("Ap3F+QidAAhxhwQhvhvAAifQAAidBvhwQBxhwCdAAIZsAAQABCbg9COQg5CJhrBpQhpBqiKA6QiNA8icAAg");
	this.shape_6.setTransform(116.1,38.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#222221").p("ACEibIkTC8IEkB8g");
	this.shape_7.setTransform(14.4,46.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AiSAgIEUi8IARE5g");
	this.shape_8.setTransform(14.7,46.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AhFiPICLEe");
	this.shape_9.setTransform(168.4,77.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AArAzIhVhl");
	this.shape_10.setTransform(179.5,96.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("AhiBgIAQi6IC6AQ");
	this.shape_11.setTransform(183.5,100.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.viola, new cjs.Rectangle(-1,0,218.5,111.5), null);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape.setTransform(180.7,301.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F2CCC").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_1.setTransform(180.7,301.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F2CCC").s().p("AgTA2QgXgJgJgVQgKgWAIgVQAJgWAVgKQAWgKAVAIQAWAJAKAVQAKAWgIAVQgJAWgVAKQgMAGgMAAQgJAAgKgEg");
	this.shape_2.setTransform(-176.9,-323.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FC3065").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape_3.setTransform(-203.2,310);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FC3065").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_4.setTransform(-203.2,310);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F2CCC").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape_5.setTransform(203.2,-234.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5F2CCC").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_6.setTransform(203.2,-235);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-222.3,-329.1,444.6,658.3);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape.setTransform(180.7,301.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5F2CCC").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_1.setTransform(180.7,301.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F2CCC").s().p("AgTA2QgXgJgJgVQgKgWAIgVQAJgWAVgKQAWgKAVAIQAWAJAKAVQAKAWgIAVQgJAWgVAKQgMAGgMAAQgJAAgKgEg");
	this.shape_2.setTransform(-176.9,-323.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FC3065").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape_3.setTransform(-203.2,310);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FC3065").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_4.setTransform(-203.2,310);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5F2CCC").s().p("Ai+AZIAAgxIF9AAIAAAxg");
	this.shape_5.setTransform(203.2,-234.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5F2CCC").s().p("AgYC/IAAl9IAxAAIAAF9g");
	this.shape_6.setTransform(203.2,-235);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-222.3,-329.1,444.6,658.3);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("AgTA2QgWgJgKgVQgKgWAJgVQAHgWAWgKQAWgKAVAIQAWAJAKAVQAKAWgIAVQgJAWgWAKQgLAGgMAAQgJAAgKgEg");
	this.shape.setTransform(223.4,27.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AhkBlQgpgqAAg7QAAg6ApgpQAqgqA6gBQA7ABAqAqQAqApgBA6QABA7gqAqQgqAqg7AAQg6AAgqgqg");
	this.shape_1.setTransform(-214.8,-19.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-229.1,-33.3,458.3,66.7);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("AgTA2QgWgJgKgVQgKgWAJgVQAHgWAWgKQAWgKAVAIQAWAJAKAVQAKAWgIAVQgJAWgWAKQgLAGgMAAQgJAAgKgEg");
	this.shape.setTransform(223.4,27.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AhkBlQgpgqAAg7QAAg6ApgpQAqgqA6gBQA7ABAqAqQAqApgBA6QABA7gqAqQgqAqg7AAQg6AAgqgqg");
	this.shape_1.setTransform(-214.8,-19.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-229.1,-33.3,458.3,66.7);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("AgTA2QgWgJgKgVQgKgWAJgVQAHgWAWgKQAWgKAVAIQAWAJAKAVQAKAWgIAVQgJAWgWAKQgLAGgMAAQgJAAgKgEg");
	this.shape.setTransform(223.4,27.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FC3065").s().p("AhkBlQgpgqAAg7QAAg6ApgpQAqgqA6gBQA7ABAqAqQAqApgBA6QABA7gqAqQgqAqg7AAQg6AAgqgqg");
	this.shape_1.setTransform(-214.8,-19.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-229.1,-33.3,458.3,66.7);


(lib.Rosa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3065").s().p("Ag0GEQgGgEgSgOQgPgMgKgEQgMgFgRgCIgdgBQh/gChBhAQg4g3gIhhQgEglAFguQACgcAJg3QAKg9AJgfQAOgzAYghQASgZAigdQAmgfATgRIAngkQAYgSAWgEQANgCAbADQAbACAMgCQAUgDAlgSQA6gXBFAXQA/AUAzAxQArAqAuBKQAlA9ALAoQAFAUADAbIAEAwQAFA5AJAzQAFAaAFAMIAKATQAFALADAIQAJAjgfAnQgKAMgTARIgdAbIgVAXQgMAOgKAHQgOALgVAIQgNAEgZAHQg+APgdAGQgzAKgpABIgFAAQg2AAgegTg");
	this.shape.setTransform(71.7,99.2);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(23).to({_off:false},0).to({_off:true},5).wait(48));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F3E3E").s().p("AhHBIQgegeAAgqQAAgpAegeQAegeApAAQAqAAAeAeQAeAeAAApQAAAqgeAeQgeAegqAAQgpAAgegeg");
	this.shape_1.setTransform(73,97.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AFVAAQAACNhkBkQhkBkiNAAQiMAAhkhkQhkhkAAiNQAAiMBkhkQBkhkCMAAQCNAABkBkQBkBkAACMg");
	this.shape_2.setTransform(73,97.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AjwDxQhkhkAAiNQAAiMBkhkQBkhkCMAAQCOAABjBkQBkBkAACMQAACNhkBkQhjBkiOAAQiMAAhkhkg");
	this.shape_3.setTransform(73,97.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AjHj4IGPHx");
	this.shape_4.setTransform(153.3,164.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#222221").p("ABBAeIiBg7");
	this.shape_5.setTransform(179.4,192.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#222221").p("AgWCDIhFi9IC8hF");
	this.shape_6.setTransform(182.1,195.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Aheg7IC9hGIh3ECg");
	this.shape_7.setTransform(182.4,195.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FC3065").s().p("ApDMYQjeAAididQieidAAjfQAAjeCeidQCdidDeAAII9AAQBzAABShSQBShSAAhzIAAhgQAAg5ApgoQApgoA5ACQA2ADAmAoQAlAoAAA3IAAF0IG6AAQA3AAAoAoQAnAnAAA3QAAA4gnAnQgnAng3ABQA3AAAnAmQAnAnAAA4QAAA3gnAoQgoAng3AAIhAAAQA0ACAmAmQAnAnAAA3QAAA4gnAnQgnAog4AAIhBAAQA2ABAlAmQAoAnAAA4QAAA4goAnQgnAng4AAg");
	this.shape_8.setTransform(131,79.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#222221").p("AjHj4IGPHx");
	this.shape_9.setTransform(194.5,164.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("ABBAeIiBg7");
	this.shape_10.setTransform(220.6,192.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("AgWCDIhFi9IC8hF");
	this.shape_11.setTransform(223.3,195.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("Aheg7IC9hGIh3ECg");
	this.shape_12.setTransform(223.6,195.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#222221").p("ACujOIluD6IGFClg");
	this.shape_13.setTransform(19.3,111);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AjCArIFuj6IAXGfg");
	this.shape_14.setTransform(19.5,111.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]},75).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,0,243.7,209.2);


(lib.nuvola = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").p("AIBkKQhTAAhPAZQhMAYhBAuQgoAbgwAEQgxADgrgXQhjg2hxgFQh1gFhsAyQhSAng+BAQgUAUgcAIQgbAIgdgHQgkgIgkAAQiEAAhdBcQhdBdAACEMAgrAAAQAAjdicicQicicjdAAg");
	this.shape.setTransform(104.6,26.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AwVELQAAiEBdhdQBdhcCEAAQAkAAAkAIQAdAHAbgIQAcgIAUgUQA+hABSgnQBsgyB1AFQBxAFBjA2QArAXAxgDQAwgEAogbQBBguBMgYQBPgZBTAAQDdAACcCcQCcCcAADdg");
	this.shape_1.setTransform(104.6,26.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.nuvola, new cjs.Rectangle(-1,-1,211.3,55.4), null);


(lib.Grigio = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EBEBEB").s().p("AhEF+QglgHgRgFQgdgJgWgMQgZgPgogoIg8g9QgYgYgIgPQgKgQgIgfQgKglgDgTIgDgSIgDgCQgkgTgWgYQglgpACg7QABg9AngnQAlgmBAgNIALgCIACgHQAIghAQgWQAZgiAxgWQBGggBtgLQBrgNA8AcQAiAPAgAhQAUAVAgArIByCWQAXAiAKAXQAGAPAKAgIAWAvQANAeAEAUQAHAfgIAgQgIAfgUAZQgUAYgbAOQgdARgjAFQgjAEgggKQgkA5g9AjQg9AihEABIgGAAQgiAAg1gKg");
	this.shape.setTransform(73.9,98.5);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},5).wait(42));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F3E3E").s().p("AhHBIQgdgeAAgqQAAgpAdgdQAegfApAAQAqAAAeAfQAdAdAAApQAAAqgdAeQgeAdgqABQgpgBgegdg");
	this.shape_1.setTransform(73,97.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#222221").p("AFVAAQAACNhkBkQhkBkiNAAQiMAAhkhkQhkhkAAiNQAAiMBkhkQBkhkCMAAQCNAABkBkQBkBkAACMg");
	this.shape_2.setTransform(73,97.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AjwDxQhkhkAAiNQAAiMBkhkQBkhkCMAAQCNAABkBkQBkBkAACMQAACNhkBkQhkBkiNAAQiMAAhkhkg");
	this.shape_3.setTransform(73,97.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#222221").p("AjHj4IGPHx");
	this.shape_4.setTransform(153.3,164.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#222221").p("ABBAeIiBg7");
	this.shape_5.setTransform(179.4,192.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#222221").p("AgWCDIhFi9IC8hF");
	this.shape_6.setTransform(182.2,195);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Aheg7IC9hFIh3ECg");
	this.shape_7.setTransform(182.4,195.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").p("AOWD/IBBAAQA3AAAognQAngoAAg3QAAg4gngnQgngmg3gBQA3AAAngnQAngnAAg4QAAg4gngnQgogng3AAIm6AAIAAl0QAAg3glgoQgmgpg2gCQg5gCgpAoQgpAnAAA5IAABgQAAB0hSBSQhSBShzAAIo9AAQjeAAidCdQieCcAADfQAADeCeCdQCdCeDeAAIWQAAQA4AAAngnQAogoAAg3QAAg4gognQglgmg2gCIBBAAQA4AAAngnQAngnAAg4QAAg4gngnQgmgmg1gBIgEAAANRILIgEAA");
	this.shape_8.setTransform(131.1,79.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EBEBEB").s().p("ApDMYQjeAAidieQieidAAjeQAAjfCeicQCdidDeAAII9AAQBzAABShSQBShSAAh0IAAhgQAAg5ApgnQApgoA5ACQA2ACAmApQAlAoAAA3IAAF0IG6AAQA3AAAoAnQAnAnAAA4QAAA4gnAnQgnAng3AAQA3ABAnAmQAnAnAAA4QAAA3gnAoQgoAng3AAIhBAAIgEAAIAEAAQA1ABAmAmQAnAnAAA4QAAA4gnAnQgnAng4AAIhBAAQA2ACAlAmQAoAnAAA4QAAA3goAoQgnAng4AAgANRILIgEAAg");
	this.shape_9.setTransform(131.1,79.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#222221").p("AjHj4IGPHx");
	this.shape_10.setTransform(194.5,164.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#222221").p("ABBAeIiBg7");
	this.shape_11.setTransform(220.6,192.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#222221").p("AgWCDIhFi9IC8hF");
	this.shape_12.setTransform(223.4,195);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Aheg7IC9hFIh3ECg");
	this.shape_13.setTransform(223.6,195.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#222221").p("ACvjPIlvD6IGGCmg");
	this.shape_14.setTransform(19.3,111);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AjCArIFuj6IAXGfg");
	this.shape_15.setTransform(19.5,111.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,244.8,210.1);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Grigio();
	this.instance.parent = this;
	this.instance.setTransform(50.6,282.5,1,1,0,0,0,121.1,104.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("AALA1IgjgGQgIAAgEgCQgGgEgGgKIgKgQQAZgiAHgHQAXgYAUgCQAHgEAHAEIAGAGQAPAWAFASQACAEAAAHQAAAGgGAGQgGAKgQAUQgGAGgGACQgEAAgEgCg");
	this.shape.setTransform(-166.3,-380.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-172.2,-386.4,345,773.3);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Grigio();
	this.instance.parent = this;
	this.instance.setTransform(50.6,282.5,1,1,0,0,0,121.1,104.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("AALA1IgjgGQgIAAgEgCQgGgEgFgKIgLgQQAZgiAHgHQAXgYAUgCQAHgEAHAEIAGAGQAPAWAFASQACAEAAAHQAAAGgGAGQgGAKgQAUQgGAGgGACQgEAAgEgCg");
	this.shape.setTransform(-166.3,-380.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-172.2,-386.4,345,773.3);


(lib.sparksv = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween4("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(222.3,329.1);

	this.instance_1 = new lib.Tween5("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(222.3,345.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,y:345.1},21).to({_off:false,y:329.1},26).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},21).to({_off:true,y:329.1},26).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,444.6,658.3);


(lib.prosa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(229.1,33.4);

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(229.1,17.4);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(229.1,33.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},18).to({state:[{t:this.instance_2}]},20).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,y:17.4},18).wait(21));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},18).to({_off:true,y:33.4},20).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,458.3,66.7);


// stage content:
(lib._5Theend1140 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

    window.StopAnimation = function () {
        // console.log('executing StopAnimation()');
        this.stop();
    }.bind(this);
    window.StartAnimation = function () {
        // console.log('executing StartAnimation()');
        this.play();
    }.bind(this);

	// timeline functions:
	this.frame_139 = function() {
		this.gotoAndPlay(79
		);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(139).call(this.frame_139).wait(14));

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5F2CCC").s().p("AA2FDQhEgCgngNQgSgGgagMIgqgVIgwgVQgcgMgSgMQgtgegbg1QgagygCg6QgEhCAbg/QAPgkAWgYQASgUApgbQArgdAXgNQAlgUAigHIAYgFQAOgDAJgFQAMgFASgQQAjgWAlAIQAPADAVAMIAiATIAYAJQANAGAHAHQAFAEAIAKQAHAKAGAEQAFAEAMAGQAMAFAFAFQALAIAIATIAMAgQAGALANARQAQAUAFAIQAWAjgFAsQgFArgdAeIgMALQgHAGgDAGQgHAKgFAUIgIAtQgFAZgKAQQgJAPgTASIghAeIgdAiQgSATgQAIQgTAJgmAAIgIAAg");
	this.shape.setTransform(514.5,187.6);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(21).to({_off:false},0).to({_off:true},4).wait(128));

	// viola
	this.instance = new lib.viola();
	this.instance.parent = this;
	this.instance.setTransform(1269.9,206.8,1,1,0,0,0,108.2,55.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:55.3,x:1003.5,y:206.6},0).wait(1).to({x:910.5},0).wait(1).to({x:846.1},0).wait(1).to({x:796.7},0).wait(1).to({x:756.8},0).wait(1).to({x:723.8},0).wait(1).to({x:696.1},0).wait(1).to({x:672.5},0).wait(1).to({x:652.3},0).wait(1).to({x:635},0).wait(1).to({x:620.3},0).wait(1).to({x:607.7},0).wait(1).to({x:597.2},0).wait(1).to({x:588.3},0).wait(1).to({x:581.1},0).wait(1).to({x:575.4},0).wait(1).to({x:571.1},0).wait(1).to({x:568.1},0).wait(1).to({x:566.3},0).wait(1).to({regY:55.5,x:565.7,y:206.8},0).wait(8).to({regY:55.3,x:565.3,y:206.6},0).wait(1).to({x:564.2},0).wait(1).to({x:562.2},0).wait(1).to({x:559.5},0).wait(1).to({x:555.9},0).wait(1).to({x:551.6},0).wait(1).to({x:546.3},0).wait(1).to({x:540.2},0).wait(1).to({x:533.2},0).wait(1).to({x:525.1},0).wait(1).to({x:516},0).wait(1).to({x:505.8},0).wait(1).to({x:494.4},0).wait(1).to({x:481.8},0).wait(1).to({x:467.7},0).wait(1).to({x:452.1},0).wait(1).to({x:434.8},0).wait(1).to({x:415.5},0).wait(1).to({x:394.1},0).wait(1).to({x:370.1},0).wait(1).to({x:343.2},0).wait(1).to({x:312.6},0).wait(1).to({x:277.5},0).wait(1).to({x:236.4},0).wait(1).to({x:187},0).wait(1).to({x:124.8},0).wait(1).to({x:37.9},0).wait(1).to({regY:55.5,x:-198.3,y:206.8},0).to({_off:true},85).wait(13));

	// Grigio copy
	this.instance_1 = new lib.Tween6("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1010.4,-65.6);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween7("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(483.3,-65.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},42).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[]},76).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(42).to({_off:false},0).wait(1).to({regX:0.2,regY:0.1,x:800.1,y:-65.5},0).wait(1).to({x:732.7},0).wait(1).to({x:688},0).wait(1).to({x:654.7},0).wait(1).to({x:628.5},0).wait(1).to({x:607.1},0).wait(1).to({x:589.3},0).wait(1).to({x:574.3},0).wait(1).to({x:561.5},0).wait(1).to({x:550.4},0).wait(1).to({x:540.9},0).wait(1).to({x:532.6},0).wait(1).to({x:525.4},0).wait(1).to({x:519.1},0).wait(1).to({x:513.6},0).wait(1).to({x:508.8},0).wait(1).to({x:504.6},0).wait(1).to({x:500.9},0).wait(1).to({x:497.7},0).wait(1).to({x:495},0).wait(1).to({x:492.6},0).wait(1).to({x:490.6},0).wait(1).to({x:488.9},0).wait(1).to({x:487.5},0).wait(1).to({x:486.3},0).wait(1).to({x:485.4},0).wait(1).to({x:484.6},0).wait(1).to({x:484.1},0).wait(1).to({x:483.7},0).wait(1).to({x:483.5},0).to({_off:true},1).wait(80));

	// Rosa copy
	this.instance_3 = new lib.Rosa();
	this.instance_3.parent = this;
	this.instance_3.setTransform(1312.3,415,1,1,0,0,0,120.9,104.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42).to({_off:false},0).wait(1).to({regX:120.8,regY:104.2,x:899.7,y:414.9},0).wait(1).to({x:765.4},0).wait(1).to({x:675.3},0).wait(1).to({x:607.6},0).wait(1).to({x:553.7},0).wait(1).to({x:509.4},0).wait(1).to({x:472.2},0).wait(1).to({x:440.6},0).wait(1).to({x:413.3},0).wait(1).to({x:389.5},0).wait(1).to({x:368.8},0).wait(1).to({x:350.6},0).wait(1).to({x:334.5},0).wait(1).to({x:320.3},0).wait(1).to({x:307.7},0).wait(1).to({x:296.5},0).wait(1).to({x:286.6},0).wait(1).to({x:277.8},0).wait(1).to({x:270},0).wait(1).to({x:263.1},0).wait(1).to({x:257.1},0).wait(1).to({x:251.7},0).wait(1).to({x:247},0).wait(1).to({x:242.9},0).wait(1).to({x:239.3},0).wait(1).to({x:236.2},0).wait(1).to({x:233.6},0).wait(1).to({x:231.4},0).wait(1).to({x:229.6},0).wait(1).to({x:228.1},0).wait(1).to({x:227},0).wait(1).to({x:226.1},0).wait(1).to({x:225.5},0).wait(1).to({x:225.2},0).wait(1).to({regX:120.9,regY:104.3,y:415},0).wait(76));

	// Grigio
	this.instance_4 = new lib.Tween6("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(1410.4,54.4);
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween7("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(883.3,54.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},48).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[]},63).wait(14));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(48).to({_off:false},0).wait(1).to({regX:0.2,regY:0.1,x:1191,y:54.5},0).wait(1).to({x:1121.8},0).wait(1).to({x:1076.4},0).wait(1).to({x:1042.8},0).wait(1).to({x:1016.5},0).wait(1).to({x:995.3},0).wait(1).to({x:977.8},0).wait(1).to({x:963.2},0).wait(1).to({x:950.8},0).wait(1).to({x:940.3},0).wait(1).to({x:931.3},0).wait(1).to({x:923.5},0).wait(1).to({x:916.8},0).wait(1).to({x:911.1},0).wait(1).to({x:906.1},0).wait(1).to({x:901.9},0).wait(1).to({x:898.3},0).wait(1).to({x:895.2},0).wait(1).to({x:892.5},0).wait(1).to({x:890.3},0).wait(1).to({x:888.5},0).wait(1).to({x:887},0).wait(1).to({x:885.8},0).wait(1).to({x:884.9},0).wait(1).to({x:884.2},0).wait(1).to({x:883.8},0).wait(1).to({x:883.5},0).to({_off:true},1).wait(77));

	// Rosa
	this.instance_6 = new lib.Rosa();
	this.instance_6.parent = this;
	this.instance_6.setTransform(1192.3,535,1,1,0,0,0,120.9,104.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(44).to({_off:false},0).wait(1).to({regX:120.8,regY:104.2,x:959.4,y:534.9},0).wait(1).to({x:885.7},0).wait(1).to({x:837},0).wait(1).to({x:800.9},0).wait(1).to({x:772.7},0).wait(1).to({x:749.8},0).wait(1).to({x:730.9},0).wait(1).to({x:715},0).wait(1).to({x:701.5},0).wait(1).to({x:690},0).wait(1).to({x:680},0).wait(1).to({x:671.5},0).wait(1).to({x:664.1},0).wait(1).to({x:657.7},0).wait(1).to({x:652.2},0).wait(1).to({x:647.4},0).wait(1).to({x:643.3},0).wait(1).to({x:639.7},0).wait(1).to({x:636.7},0).wait(1).to({x:634.1},0).wait(1).to({x:631.9},0).wait(1).to({x:630.1},0).wait(1).to({x:628.7},0).wait(1).to({x:627.5},0).wait(1).to({x:626.6},0).wait(1).to({x:625.9},0).wait(1).to({x:625.4},0).wait(1).to({x:625.2},0).wait(1).to({regX:120.9,regY:104.3,y:535},0).to({_off:true},76).wait(4));

	// p rosa
	this.instance_7 = new lib.prosa();
	this.instance_7.parent = this;
	this.instance_7.setTransform(751.7,580.8,0.52,0.52,-70.7,0,0,229.2,33.4);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(50).to({_off:false},0).wait(1).to({regX:229.1,regY:25.4,scaleX:0.76,scaleY:0.76,rotation:-34.9,x:701.5,y:545.5},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:-24.4,x:688.5,y:535.4},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-17.8,x:680.6,y:529.3},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-13.2,x:675,y:524.9},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-9.8,x:671,y:521.7},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-7.2,x:667.9,y:519.4},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-5.2,x:665.6,y:517.6},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-3.7,x:663.8,y:516.1},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-2.5,x:662.5,y:515.1},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.6,x:661.4,y:514.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1,x:660.7,y:513.7},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.5,x:660.1,y:513.3},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2,x:659.7,y:513},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1,x:659.5,y:512.9},0).wait(1).to({regY:33.4,rotation:0,x:659.6,y:520.9},0).to({_off:true},75).wait(13));

	// sparks v
	this.instance_8 = new lib.sparksv();
	this.instance_8.parent = this;
	this.instance_8.setTransform(757,374.1,1,1,0,0,0,222.3,329.1);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(58).to({_off:false},0).wait(1).to({regY:337.1,x:666.5,y:382.1,alpha:0.565},0).wait(1).to({x:642.2,alpha:0.717},0).wait(1).to({x:628,alpha:0.806},0).wait(1).to({x:618.5,alpha:0.865},0).wait(1).to({x:612,alpha:0.906},0).wait(1).to({x:607.3,alpha:0.935},0).wait(1).to({x:604,alpha:0.956},0).wait(1).to({x:601.6,alpha:0.971},0).wait(1).to({x:599.9,alpha:0.982},0).wait(1).to({x:598.7,alpha:0.989},0).wait(1).to({x:597.9,alpha:0.994},0).wait(1).to({x:597.4,alpha:0.997},0).wait(1).to({x:597.1,alpha:0.999},0).wait(1).to({x:597,alpha:1},0).wait(1).to({regY:329.1,y:374.1},0).to({_off:true},67).wait(13));

	// Layer 1 copy
	this.instance_9 = new lib.nuvola();
	this.instance_9.parent = this;
	this.instance_9.setTransform(266.6,182.3,1,1,0,0,0,104.6,26.7);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(55).to({_off:false},0).wait(1).to({x:260,alpha:0.274},0).wait(1).to({x:256.1,alpha:0.435},0).wait(1).to({x:253.4,alpha:0.549},0).wait(1).to({x:251.3,alpha:0.636},0).wait(1).to({x:249.7,alpha:0.704},0).wait(1).to({x:248.4,alpha:0.759},0).wait(1).to({x:247.3,alpha:0.805},0).wait(1).to({x:246.4,alpha:0.842},0).wait(1).to({x:245.6,alpha:0.873},0).wait(1).to({x:245,alpha:0.899},0).wait(1).to({x:244.5,alpha:0.92},0).wait(1).to({x:244.1,alpha:0.938},0).wait(1).to({x:243.7,alpha:0.952},0).wait(1).to({x:243.5,alpha:0.964},0).wait(1).to({x:243.2,alpha:0.974},0).wait(1).to({x:243,alpha:0.981},0).wait(1).to({x:242.9,alpha:0.987},0).wait(1).to({x:242.8,alpha:0.992},0).wait(1).to({x:242.7,alpha:0.995},0).wait(1).to({alpha:0.998},0).wait(1).to({x:242.6,alpha:0.999},0).wait(1).to({alpha:1},0).wait(1).to({_off:true},62).wait(13));

	// Layer 1 copy 2
	this.instance_10 = new lib.nuvola();
	this.instance_10.parent = this;
	this.instance_10.setTransform(162.7,658.7,1,1,0,0,0,104.6,26.7);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(9).to({_off:false},0).wait(1).to({x:173.1,alpha:0.164},0).wait(1).to({x:180.5,alpha:0.278},0).wait(1).to({x:186.2,alpha:0.368},0).wait(1).to({x:190.9,alpha:0.441},0).wait(1).to({x:194.8,alpha:0.502},0).wait(1).to({x:198.2,alpha:0.555},0).wait(1).to({x:201.2,alpha:0.602},0).wait(1).to({x:203.8,alpha:0.642},0).wait(1).to({x:206.1,alpha:0.678},0).wait(1).to({x:208.2,alpha:0.711},0).wait(1).to({x:210,alpha:0.74},0).wait(1).to({x:211.7,alpha:0.766},0).wait(1).to({x:213.2,alpha:0.79},0).wait(1).to({x:214.6,alpha:0.811},0).wait(1).to({x:215.8,alpha:0.83},0).wait(1).to({x:217,alpha:0.848},0).wait(1).to({x:218,alpha:0.864},0).wait(1).to({x:218.9,alpha:0.879},0).wait(1).to({x:219.8,alpha:0.892},0).wait(1).to({x:220.5,alpha:0.904},0).wait(1).to({x:221.2,alpha:0.915},0).wait(1).to({x:221.9,alpha:0.925},0).wait(1).to({x:222.5,alpha:0.934},0).wait(1).to({x:223,alpha:0.942},0).wait(1).to({x:223.5,alpha:0.949},0).wait(1).to({x:223.9,alpha:0.956},0).wait(1).to({x:224.3,alpha:0.962},0).wait(1).to({x:224.6,alpha:0.967},0).wait(1).to({x:224.9,alpha:0.972},0).wait(1).to({x:225.2,alpha:0.977},0).wait(1).to({x:225.4,alpha:0.98},0).wait(1).to({x:225.7,alpha:0.984},0).wait(1).to({x:225.9,alpha:0.987},0).wait(1).to({x:226,alpha:0.989},0).wait(1).to({x:226.2,alpha:0.992},0).wait(1).to({x:226.3,alpha:0.993},0).wait(1).to({x:226.4,alpha:0.995},0).wait(1).to({x:226.5,alpha:0.996},0).wait(1).to({alpha:0.997},0).wait(1).to({x:226.6,alpha:0.998},0).wait(1).to({alpha:0.999},0).wait(1).to({x:226.7},0).wait(1).to({alpha:1},0).wait(2).to({_off:true},86).wait(13));

	// Layer 1
	this.instance_11 = new lib.nuvola();
	this.instance_11.parent = this;
	this.instance_11.setTransform(870.7,598.7,1,1,0,0,0,104.6,26.7);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(17).to({_off:false},0).wait(1).to({x:881.1,alpha:0.164},0).wait(1).to({x:888.5,alpha:0.278},0).wait(1).to({x:894.2,alpha:0.368},0).wait(1).to({x:898.9,alpha:0.441},0).wait(1).to({x:902.8,alpha:0.502},0).wait(1).to({x:906.2,alpha:0.555},0).wait(1).to({x:909.2,alpha:0.602},0).wait(1).to({x:911.8,alpha:0.642},0).wait(1).to({x:914.1,alpha:0.678},0).wait(1).to({x:916.2,alpha:0.711},0).wait(1).to({x:918,alpha:0.74},0).wait(1).to({x:919.7,alpha:0.766},0).wait(1).to({x:921.2,alpha:0.79},0).wait(1).to({x:922.6,alpha:0.811},0).wait(1).to({x:923.8,alpha:0.83},0).wait(1).to({x:925,alpha:0.848},0).wait(1).to({x:926,alpha:0.864},0).wait(1).to({x:926.9,alpha:0.879},0).wait(1).to({x:927.8,alpha:0.892},0).wait(1).to({x:928.5,alpha:0.904},0).wait(1).to({x:929.2,alpha:0.915},0).wait(1).to({x:929.9,alpha:0.925},0).wait(1).to({x:930.5,alpha:0.934},0).wait(1).to({x:931,alpha:0.942},0).wait(1).to({x:931.5,alpha:0.949},0).wait(1).to({x:931.9,alpha:0.956},0).wait(1).to({x:932.3,alpha:0.962},0).wait(1).to({x:932.6,alpha:0.967},0).wait(1).to({x:932.9,alpha:0.972},0).wait(1).to({x:933.2,alpha:0.977},0).wait(1).to({x:933.4,alpha:0.98},0).wait(1).to({x:933.7,alpha:0.984},0).wait(1).to({x:933.9,alpha:0.987},0).wait(1).to({x:934,alpha:0.989},0).wait(1).to({x:934.2,alpha:0.992},0).wait(1).to({x:934.3,alpha:0.993},0).wait(1).to({x:934.4,alpha:0.995},0).wait(1).to({x:934.5,alpha:0.996},0).wait(1).to({alpha:0.997},0).wait(1).to({x:934.6,alpha:0.998},0).wait(1).to({alpha:0.999},0).wait(1).to({x:934.7},0).wait(1).to({alpha:1},0).wait(2).to({_off:true},78).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1730.7,526.3,218.5,111);
// library properties:
lib.properties = {
	id: 'AE626E680F074C8A87F8191DE326967E',
	width: 1140,
	height: 750,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['AE626E680F074C8A87F8191DE326967E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;